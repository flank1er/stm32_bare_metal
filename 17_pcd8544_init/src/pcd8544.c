#include "main.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_spi.h"
#include "pcd8544.h"
#include "uart.h"

// Port B
#define RST  GPIO_Pin_4
#define BL   GPIO_Pin_5
#define DC   GPIO_Pin_6
// Port A
#define CS   GPIO_Pin_4
#define CLK  GPIO_Pin_5
#define DIN  GPIO_Pin_7

#define LCD_C     0x00
#define LCD_D     0x01

#define LCD_X     84
#define LCD_Y     48
#define LCD_LEN   (uint16_t)((LCD_X * LCD_Y) / 8)

#define chip_select_enable() gpio_reset(GPIOA,CS)
#define chip_select_disable() gpio_set(GPIOA,CS)

#define FONT_SIZE_1 (uint8_t)0x01
#define FONT_SIZE_2 (uint8_t)0x02
#define FONT_SIZE_3 (uint8_t)0x03

static uint8_t fb[LCD_LEN];          // screen buffer

bool isPower=false;

void pcd8544_send(uint8_t dc, uint8_t data);

void pcd8544_init() {
    // hardware reset
    gpio_reset(GPIOB,RST);
    gpio_set(GPIOB,RST);
    // init routine
    chip_select_enable();
    pcd8544_send(LCD_C, 0x21);  // LCD Extended Commands.
    pcd8544_send(LCD_C, 0x14);  // LCD bias mode 1:48. //0x13
    pcd8544_send(LCD_C, 0xB6);  // Set LCD Vop (Contrast).
    pcd8544_send(LCD_C, 0x04);  // Set Temp coefficent. //0x04
    pcd8544_send(LCD_C, 0x20);  // LCD Basic Commands
    pcd8544_send(LCD_C, 0x0C);  // LCD in normal mode.
    //pcd8544_send(LCD_C, 0x0d);  // inverse mode
    chip_select_disable();
    gpio_reset(GPIOB,BL);           // enable blacklight
    isPower=true;
    pcd8544_fill_fb(0x0);
    pcd8544_display_fb();
}

void pcd8544_send(uint8_t dc, uint8_t data)
{

   if (dc == LCD_D)
      gpio_set(GPIOB,DC);
   else
      gpio_reset(GPIOB,DC);

#ifdef HW_SPI
   SPI1->DR=data;
   while (!(SPI1->SR & SPI_I2S_FLAG_TXE) || (SPI1->SR & SPI_I2S_FLAG_BSY));
#else

   for (uint8_t i=0; i<8; i++)
   {
      if (data & 0x80)
          gpio_set(GPIOA,DIN);
      else
          gpio_reset(GPIOA,DIN);

      data=(data<<1);
      // Set Clock Signal
      gpio_set(GPIOA,CLK);
      gpio_reset(GPIOA,CLK);
   }
#endif
}

//  work with buffer
void pcd8544_fill_fb(uint8_t value)
{
    for(int i=0;i<LCD_LEN; i++)
        fb[i]=value;
}

void pcd8544_display_fb() {
    chip_select_enable();
    for(uint16_t i=0;i<LCD_LEN; i++)
        pcd8544_send(LCD_D,fb[i]);
    chip_select_disable();
}


void pcd8544_fill(uint8_t value)
{
   chip_select_enable();
   for (int i=0; i < LCD_LEN; i++)
   {
      pcd8544_send(LCD_D, value);
   }
   chip_select_disable();
}

void pcd8544_off() {
    pcd8544_fill(0);
    // hardware reset
    gpio_reset(GPIOB,RST);
    delay_ms(10);
    gpio_set(GPIOB,RST);
    delay_ms(10);
    // blacklight off
    gpio_set(GPIOB,BL);
    isPower=false;
}
