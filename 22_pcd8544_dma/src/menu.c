#include "pcd8544.h"
#include "menu.h"
#include "uart.h"

#define LCD_X 12
#define LCD_Y 6
#define LCD_LEN (uint8_t)(LCD_X * LCD_Y)        // =72
#define MAIN_MENU_MAX_DEPTH 8
#define MAIN_MENU_LEN   (uint8_t)(MAIN_MENU_MAX_DEPTH * LCD_X)
//------------------------------------|000011112222000011112222000011112222000011112222|
//const uint8_t  menu[MAIN_MENU_LEN] = {"  LINE 01     LINE 02     LINE 03     LINE 04   "};
const uint8_t  menu[MAIN_MENU_LEN] = {"  LINE 01     LINE 02     LINE 03     LINE 04     LINE 05     LINE 06     LINE 07     LINE 08   "};

extern bool isMirror;
static uint8_t select_line=0;

void main_menu(uint8_t select) {
    if (select >= MAIN_MENU_MAX_DEPTH)
        return;

    uint8_t start;
    if (select < LCD_Y)
        start=0;
    else
        start=(select-LCD_Y+1);

    pcd8544_clear();
#if MAIN_MENU_MAX_DEPTH > LCD_Y
    for (uint8_t i=(start*LCD_X); i<((start*LCD_X)+LCD_LEN); i++) {
        if (i<(MAIN_MENU_MAX_DEPTH*LCD_X))
            pcd8544_send_char(menu[i],false);
    }
#else
    for (uint8_t i=0; i<MAIN_MENU_LEN; i++) {
        pcd8544_send_char(menu[i],false);
    }
#endif

    menu_inverse_line(select);

    if (isMirror)
        pcd8544_mirror();

#ifndef  USE_DMA
    pcd8544_display_fb();
#else
    pcd8544_DMA_UPD();
#endif

    select_line=select;
}

void main_menu_enc(int offset) {
    int nl=(int)(select_line + offset);
    if (nl < 0) {
        nl=0;
    } else if (nl >= MAIN_MENU_MAX_DEPTH) {
        nl = (MAIN_MENU_MAX_DEPTH -1);
    }

    main_menu((uint8_t)nl);
    return;
}

void menu_inverse_line(uint8_t line) {
    if (line < LCD_Y) {
        //pcd8544_clear_line(line);
        pcd8544_set_pos(0,line);
    } else {
        //pcd8544_clear_line(LCD_Y-1);
        pcd8544_set_pos(0,LCD_Y-1);
    }


    line = line *LCD_X;
    for (uint8_t i=line; i<(line+LCD_X); i++) {
        uint8_t c=menu[i];
        pcd8544_send_char(c,true);
    }

}
