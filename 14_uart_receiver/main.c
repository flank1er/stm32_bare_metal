#include "main.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_usart.h"
#include "stm32f10x_rcc.h"
#include "uart.h"

extern uint8_t uart_ready;
extern uint8_t uart_index;
extern char uart_buf[UART_BUFFER_LEN];              // UART_BUFFER_LEN=10
void clear_uart_buf();

int main()
{
    // enable GPIOC port
    RCC->APB2ENR |= RCC_APB2Periph_GPIOC;           // enable PORT_C
    RCC->APB2ENR |= RCC_APB2Periph_GPIOA;           // enable PORT_A
    RCC->APB2ENR |= RCC_APB2Periph_USART1;          // enable UART1
    // --- GPIO setup ----
    GPIOC->CRH &= ~(uint32_t)(0xf<<20);             // Reset for PC13 (LED)
    GPIOC->CRH |=  (uint32_t)(0x2<<20);             // Push-Pull 2-MHz fo PC13 (LED)
	gpio_set(GPIOC,LED);
    GPIOA->CRH &= ~(uint32_t)(0xf<<4);              // enable Alterentive mode
    GPIOA->CRH |=  (uint32_t)(0xa<<4);              // for PA9 = USART1_TX
    GPIOA->CRH &= ~(uint32_t)(0xf<<8);              // enable Alterentive mode
    GPIOA->CRH |=  (uint32_t)(0xa<<8);              // for PA10 = USART1_RX
    // ------- SysTick CONFIG --------------
    if (SysTick_Config(72000)) // set 1ms
    {
        while(1); // error
    }
    // --- UART setup ----
    //USART1->BRR  = 0x1d4c;                        // 9600 Baud, when APB2=72MHz
    USART1->BRR = 0x271;                            // 115200 Baud, when APB2=72MHz
    USART1->CR1 |= (USART_CR1_UE_Set | USART_Mode_Tx | USART_Mode_Rx | USART_CR1_RXNEIE);  // enable USART1, enable  TX/RX mode, enable RX interrupt
    NVIC_SetPriority(USART1_IRQn,1);                // set priority for USART1 IRQ
    NVIC_EnableIRQ(USART1_IRQn);                    // enable USART1 IRQ via NVIC
    //////////////////////////////////////
    /// let's go.....
    /////////////////////////////////////
    uint8_t led_toggle=0;
    bool tick_enable=1;
    uint8_t count=0;
	println("ready...");
    for(;;){
        if (uart_ready) {
            if (uart_buf[0] == '?' && uart_buf[1] == 0x0) {
                print("help:\n");
                print("t-  disable print tick\n");
            } else if (uart_buf[0] == 't' && uart_buf[1] == '-') {
                print("turn off tick\n");
                tick_enable=false;
            } else {
                print("invalid command\n");
            }
            clear_uart_buf();
            uart_ready=0;
            uart_index=0;
        }

        if ((++count % 20)  == 0) {
            if (led_toggle)
                gpio_set(GPIOC,LED);
            else
                gpio_reset(GPIOC,LED);
            led_toggle=!led_toggle;
            if (tick_enable)
                print("tick..\n");
            count=0;
        }
        delay_ms(50);
    }
}

void clear_uart_buf() {
    for (uint8_t i=0; i<UART_BUFFER_LEN;i++)
        uart_buf[i]=0;
}
