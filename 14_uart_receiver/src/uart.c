#include "main.h"
#include "uart.h"

#define  len 10

const uint8_t hex[]="0123456789ABCDEF";

void uart_print_hex(uint32_t num){
    uint8_t n[len];
    uint8_t *s=n+(len-1);
    *s=0;           // EOL
    do {
		*(--s)=hex[num%16];
		num=num/16;
    } while (num>0);
    *(--s)='x'; *(--s)='0';
    uart_print_str((char*)s);
}

void  uart_print_num(uint32_t num){
    uint8_t n[len];
    uint8_t *s=n+(len-1);
    *s=0;           // EOL
    do {
		*(--s)=('0' + num%10);
		num = num/10;
    } while (num>0);
    uart_print_str((char*)s);
}


void uart_print_str(char *str) {
    while (*str)
    {
        uart_send_char(*str++);
    }
}

void uart_send_char(char ch) {
    while(!(USART1->SR & USART_FLAG_TXE)) {
        __asm__ volatile ("nop\n");
    }
    USART1->DR=ch;
} 


