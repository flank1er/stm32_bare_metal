import qbs

Product {
	type: ["application","flash"]
    Depends { name: "cpp" }
    name: "firmware"

    property string Home: path
    property string Inc: Home + "/inc"
    property string Src: Home + "/src"
    property string Asm: Home + "/asm"

    cpp.cLanguageVersion: "c99"
    cpp.positionIndependentCode: false
	cpp.executableSuffix: ".elf"

	cpp.includePaths:[
		"CMSIS/device",
		"CMSIS/core",
		"SPL/inc",
		"inc",
	]

	cpp.driverFlags: [
        "-specs=nosys.specs",
		"-specs=nano.specs",
	]

	cpp.linkerFlags:
   [
		"--gc-sections",
		"-T" + path + "/script.ld",
	]


    cpp.cFlags: [
		"-mthumb",
	    "-mcpu=cortex-m3",
    ]

	cpp.defines: [
		"STM32F10X_MD",
		"SYSCLK_FREQ_72MHz",
	]

    Properties
    {
        condition: qbs.buildVariant === "debug"
        cpp.defines: outer.concat(["DEBUG=1"])
        cpp.debugInformation: true
        cpp.optimization: "none"
	}

    Properties
    {
		condition: qbs.buildVariant === "release"
	    cpp.debugInformation: false
	    cpp.optimization: "fast"
    }


    Group {
        name: "Assembly"
        fileTags: ["asm"]
        files: [
            Asm  + "/*.s",
        ]
    }

    Group {
        name: "Inc"
        files: [
            Inc + "/*.h",
        ]
    }

    Group {
        name: "Src"
        files: [
            Src + "/*.c",
        ]
    }


   files: [
        "main.c",
    ]


    Rule
    {
        inputs: ["application"]
        Artifact
        {
            filePath: product.name
            fileTags: "flash"
        }
        prepare:
        {
            var size_name=input.filePath
            var argsObjcopy = ["-O", "binary", input.filePath, output.filePath+".bin"]
            var cmd_bin=new Command("arm-none-eabi-objcopy",argsObjcopy)
            var cmd_size=new Command("arm-none-eabi-size",size_name)
            cmd_bin.description = "convert to binary format"
            cmd_size.description = "print size of the firmware"
            return [cmd_bin,cmd_size]
        }
    }
}
