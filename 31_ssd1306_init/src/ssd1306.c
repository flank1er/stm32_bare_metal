#include "main.h"
#include "ssd1306.h"

#define ssd1306_buf_size 512
#define ssd1306_init_len 25

const uint8_t  ssd1306_init_str[ssd1306_init_len] =
        {0xAE,0xD5,0x80,0xA8,0x1F,0xD3,0x00,0x00,0x8D,0x14,0x20,0x00,
         0xA1,0xC8,0xDA,0x02,0x81,0x8F,0xD9,0xF1,0xDB,0x40,0xA4,0xA6,0xAF};

uint8_t init_i2c(uint8_t adr, uint8_t value, uint8_t last);
void i2c_send_cmd(uint8_t cmd);


void ssd1306_turn_off(void)
{
    i2c_send_cmd(0xAE); // Set display OFF
}


uint8_t ssd1306_init() {
    uint8_t ret=SSD1306_OK;
    //enable_i2c;
    for(uint8_t i=0; i<ssd1306_init_len; i++) {
        i2c_send_cmd(ssd1306_init_str[i]);
    }

    delay_ms(20);
    //disable_i2c;
    return ret;
}


void i2c_send_cmd(uint8_t cmd) {
    enable_i2c;
    if (init_i2c((SSD1306_I2C_ADDRESS<<1), 0x0, NOLAST) == 0) {
        I2C1->DR=cmd;
        while(!(I2C1->SR1 & I2C_FLAG_BTF));     // wait BFT
        I2C1->CR1 |= I2C_CR1_STOP_Set;          // Program the STOP bit
        while (I2C1->CR1 & I2C_CR1_STOP_Set);   // Wait until STOP bit is cleared by hardware
    }  else
        stop_i2c;
    delay_ms(20);
    disable_i2c;
}


uint8_t init_i2c(uint8_t adr, uint8_t value, uint8_t last) {
    I2C1->CR1 |= I2C_CR1_START_Set;             //=0x0100, START
    while (!(I2C1->SR1 & I2C_FLAG_SB));         // wait SB
    (void) I2C1->SR1;
    I2C1->DR=adr;                               // send ADDR
    while (!(I2C1->SR1 & I2C_FLAG_ADDR))        // wait ADDR
    {
        if(I2C1->SR1 & I2C_IT_AF)               // if NACK
            return 1;
    }
    (void) I2C1->SR1;                           // clear ADDR
    (void) I2C1->SR2;                           // clear ADDR
    I2C1->DR=value;
    if (last == LAST) {
        while(!(I2C1->SR1 & I2C_FLAG_BTF));     // wait BFT
        I2C1->CR1 |= I2C_CR1_STOP_Set;          // Program the STOP bit
        while (I2C1->CR1 & I2C_CR1_STOP_Set);   // Wait until STOP bit is cleared by hardware
    } else {
        while(!(I2C1->SR1 & I2C_FLAG_TXE));     // wait TXE bit
    }
    return 0;
}
