#include "font5x7.h"
#include "ste2007.h"

extern uint8_t fb[];
extern uint16_t pos;


void nums_send(uint8_t ch) {
    uint16_t p=pos;
    ch-=0x30;
    if (ch <10) {
        for(uint8_t i = 0; i<12;i++) {
            fb[p+i]=NUMS[ch*24+i];
        }
    }
    p+=96;
    for(uint8_t i = 12; i<24;i++) {
        fb[p+i-12]=NUMS[ch*24+i];
    }
}

void font_ascii_5x7_print_str_invert(char *str, uint8_t x, uint8_t y)
{
    pos=y*LCD_X + x;
    while (*str)
    {
        font_ascii_5x7_send(*str++,true);
    }
}

// y = column, x = pixel
void font_ascii_5x7_print_str(char *str, uint8_t x, uint8_t y)
{
    pos=y*LCD_X + x;
    while (*str)
    {
        font_ascii_5x7_send(*str++,false);
    }
}

void font_ascii_5x7_send(uint8_t ch,bool inverse)
{
    if (ch >= 0x20 && ch <= 0xf0 && pos <= (LCD_LEN-6))
    {
        for (uint8_t i=0; i < 5; i++)
        {
            uint8_t c=(ch<0xe0) ? ch - 0x20 : ch - 0x50;
            c=ASCII_5x7[c][i];
            if (inverse)
                fb[pos+i] =~c;
            else
                fb[pos+i] = c;

        }
        if (inverse) {
            fb[pos+6]=0xff;
            fb[pos+5]=0xff;
        } else {
            fb[pos+6]=0x0;
            fb[pos+5]=0x0;
        }
        pos+=7;
    }
}

void font_ascii_5x7_send_char_size2(uint8_t ch, uint8_t x, uint8_t y) {
    uint8_t s[5]; // source
    uint8_t r[20]; // result
    uint8_t i,j;
   // get littera
    if (ch >= 0x20 && ch <= 0xf0)
    {
        for (i=0; i < 5; i++)
        {
              uint8_t c=(ch<0xe0) ? ch - 0x20 : ch - 0x50;
              s[i]=ASCII_5x7[c][i];
        }
    }
    // scale
    for(i=0;i<5;i++)
    {
        uint8_t a=0;
        for(j=0;j<4;j++)
        {
            uint8_t b=(s[i]>>j) & 0x01;
            a|=(b<<(j<<1)) | (b<<((j<<1)+1));
        }
        r[(i<<1)]=a;
        r[(i<<1)+1]=a;
    }

    for(i=0;i<5;i++)
    {
        uint8_t a=0;
        for(j=0;j<4;j++)
        {
            uint8_t b=(s[i]>>(j+4)) & 0x01;
            a|=(b<<(j<<1)) | (b<<((j<<1)+1));
        }
        r[(i<<1)+10]=a;
        r[(i<<1)+11]=a;
    }
    // print
    pos=y*LCD_X+x;
    if (pos<(LCD_LEN-14))
    {
        fb[pos++]=0x00; fb[pos++]=0x00;
        for(i=0;i<10;i++)
            fb[pos++]=r[i];

        fb[pos++]=0x00; fb[pos++]=0x00;
    };

    pos=(y+1)*LCD_X+x;
    if(pos<(LCD_LEN-14))
    {
        fb[pos++]=0x00; fb[pos++]=0x00;
        for(i=10;i<20;i++)
            fb[pos++]=r[i];
        fb[pos++]=0x00; fb[pos++]=0x00;
    }
}

void font_ascii_5x7_send_char_size3(uint8_t ch, uint8_t x, uint8_t y) {
    uint8_t s[5]; // source
    uint8_t r[45]; // result
    uint8_t i;
    // get littera
    if (ch >= 0x20 && ch <= 0xf0)
    {
        for (i=0; i < 5; i++)
        {
              uint8_t c=(ch<0xe0) ? ch - 0x20 : ch - 0x50;
              s[i]=ASCII_5x7[c][i];
        }
    }
    // scale
    for(i=0;i<5;i++)
    {
        uint8_t b,a;
        b=(s[i] & 0x01);
        a=(b) ? 0x7 : 0;
        b=(s[i]>>1) & 0x01;
        if (b) a|=0x38;
        b=(s[i]>>2) & 0x01;
        a|=(b<<6)|(b<<7);

        r[i*3]=a;
        r[i*3+1]=a;
        r[i*3+2]=a;

        r[i*3+15]=b;
        r[i*3+16]=b;
        r[i*3+17]=b;
    }

    for(i=0;i<5;i++)
    {
        uint8_t b,a;
        b=(s[i]>>3) & 0x01;
        a=(b) ? 0x0e : 0;
        b=(s[i]>>4) & 0x01;
        if (b) a|=0x70;
        b=(s[i]>>5) & 0x01;
        a|=(b<<7);

        r[i*3+15]|=a;
        r[i*3+16]|=a;
        r[i*3+17]|=a;
     }

    for(i=0;i<5;i++)
    {
        uint8_t b,a;
        b=(s[i]>>5) & 0x01;
        a=(b) ? 0x3 : 0;
        b=(s[i]>>6) & 0x01;
        if (b) a|=0x1c;
        b=(s[i]>>7) & 0x01;
        if (b) a|=0xe0;

        r[i*3+30]=a;
        r[i*3+31]=a;
        r[i*3+32]=a;
     }

    // print
    pos=y*LCD_X+x;
    fb[pos++]=0;fb[pos++]=0; fb[pos++]=0;
    for(i=0;i<15;i++) fb[pos++]=r[i];
    fb[pos++]=0;fb[pos++]=0; fb[pos++]=0;

    pos=(y+1)*LCD_X+x;
    fb[pos++]=0;fb[pos++]=0; fb[pos++]=0;
    for(i=15;i<30;i++) fb[pos++]=r[i];
    fb[pos++]=0;fb[pos++]=0; fb[pos++]=0;

    pos=(y+2)*LCD_X+x;
    fb[pos++]=0;fb[pos++]=0; fb[pos++]=0;
    for(i=30;i<45;i++) fb[pos++]=r[i];
    fb[pos++]=0;fb[pos++]=0; fb[pos++]=0;
}

void font_ascii_5x7_print_uint8(uint8_t num, uint8_t size, uint8_t x, uint8_t y){
    uint8_t sym[3];
    int8_t i=2;
    do  {
      if (num == 0 && i<2)
        sym[i]=0x20; // space
      else
        sym[i]=0x30+num%10;

      num=num/10;
      i--;

    } while (i>=0);

    uint8_t j=0;
    for (i=0;i<3;i++)
    {
        if (!(i<2 && sym[i] == 0x20))
        {
            switch(size) {
            case 3:
                font_ascii_5x7_send_char_size3(sym[i],x+j*6*size,y);
                break;
            case 2:
                font_ascii_5x7_send_char_size2(sym[i],x+j*6*size,y);
                break;
            default:
                pos=(y*LCD_X) + x +j*7;
                font_ascii_5x7_send(sym[i],false);
                break;
            }
            j++;
        }
    }
}
void font_ascii_5x7_print(char *str, uint8_t size,  uint8_t x, uint8_t y)
{
    uint8_t i=0;
    pos=y*LCD_X+x;
    switch (size) {
      case 3:
          while (*str)
          {
             font_ascii_5x7_send_char_size3(*str++,x+(i*6*size),y);
             ++i;
          }
          break;
      case 2:
          while (*str)
          {
            font_ascii_5x7_send_char_size2(*str++,x+(i*6*size),y);
            ++i;
          }
          break;
      default:
          while (*str)
          {
            font_ascii_5x7_send(*str++,false);
          }
          break;
   }
}


void cristal_print_uint8(uint8_t num, uint8_t x, uint8_t y){
    uint8_t sym[3];
    int8_t i=2;
    do  {
      if (num == 0 && i<2)
        sym[i]=0x20; // space
      else
        sym[i]=0x30+num%10;

      num=num/10;
      i--;

    } while (i>=0);

    uint8_t j=0;
    for (i=0;i<3;i++)
    {
        if (!(i<2 && sym[i] == 0x20))
        {
            ste2007_set_graph_pos(x+j*14,y);
            nums_send(sym[i]);
            j++;
        }
    }
}

