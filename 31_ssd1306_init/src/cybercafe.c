#include "cybercafe.h"
#include "ste2007.h"

extern uint8_t fb[];
extern uint16_t pos;

void cybercafe_send(uint8_t ch)
{
    if (ch >= 0x20 && ch <= 0x80 && pos <= (LCD_LEN-104))
    {
        for (uint8_t i=0; i < 8; i++)
        {
            uint8_t c=ch - 0x20;
            c=CYBERCAFE[c*16+i];
            fb[pos+i] = c;

        }


        for (uint8_t i=8; i < 16; i++)
        {
            uint8_t c=ch - 0x20;
            c=CYBERCAFE[c*16+i];
            fb[pos+88+i] = c;

        }
        pos+=8;
    }
}

// y = column, x = row
void cybercafe_print_str(char *str, uint8_t x, uint8_t y)
{
    pos=y*LCD_X + x;
    while (*str)
    {
        cybercafe_send(*str++);
        if ((pos >= 96 && pos <192) || (pos >=288 && pos < 384) || (pos >=480 && pos < 576) || (pos >=672 && pos < 768))
            pos+=96;
    }
}


void cybercafe_print_uint8(uint8_t num, uint8_t x, uint8_t y){
    uint8_t sym[3];
    int8_t i=2;
    do  {
      if (num == 0 && i<2)
        sym[i]=0x20; // space
      else
        sym[i]=0x30+num%10;

      num=num/10;
      i--;

    } while (i>=0);

    for (i=0;i<3;i++)
    {
        if (!(i<2 && sym[i] == 0x20))
        {

            pos=(y*LCD_X) + x +i*7;
            cybercafe_send(sym[i]);
        }
    }
}

// ------------- HUGE DIGITS ------------------

void cybercafe_digits_send(uint8_t ch)
{
    if (ch > 14 || pos > (LCD_LEN - 208))
        return;

    for (uint8_t i=0,j=0; i < 48; i+=3,j++)
    {
        uint8_t c=CYBERCAFE_HUGE_DIGITS[ch*48+i];
        fb[pos+j] = c;

    }
    pos+=96;
    for (uint8_t i=1,j=0; i < 48; i+=3,j++)
    {
        uint8_t c=CYBERCAFE_HUGE_DIGITS[ch*48+i];
        fb[pos+j] = c;

    }

    pos+=96;
    for (uint8_t i=2,j=0; i < 48; i+=3,j++)
    {
        uint8_t c=CYBERCAFE_HUGE_DIGITS[ch*48+i];
        fb[pos+j] = c;

    }

}

void cybercafe_print_huge_uint8(uint8_t num, uint8_t x, uint8_t y){
    uint8_t sym[3];
    int8_t i=2;
    do  {
      if (num == 0 && i<2)
        sym[i]=10; // space
      else
        sym[i]=num%10;

      num=num/10;
      i--;

    } while (i>=0);

    uint8_t j=0;
    for (i=0;i<3;i++)
    {
        if (!(i<2 && sym[i] == 10))
        {

            pos=(y*LCD_X) + x +j*16;
            cybercafe_digits_send(sym[i]);
            j++;
        }
    }
}


void cybercafe_print_large_uint8(uint8_t num, uint8_t x, uint8_t y){
    uint8_t sym[3];
    int8_t i=2;
    do  {
      if (num == 0 && i<2)
        sym[i]=10; // space
      else
        sym[i]=num%10;

      num=num/10;
      i--;

    } while (i>=0);

    for (i=0;i<3;i++)
    {
        if (!(i<2 && sym[i] == 10))
        {

            pos=(y*LCD_X) + x +i*16;
            cybercafe_digits_send(sym[i]);
        }
    }
}

