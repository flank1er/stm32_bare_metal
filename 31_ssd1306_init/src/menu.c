#include "ste2007.h"
#include "cp866.h"
#include "menu.h"
#include "uart.h"

#define LCD_HEIGHT 12
#define LCD_WIDTH 8
//#define LCD_LEN (uint8_t)(LCD_X * LCD_Y)        // =72
#define LCD_DEPTH 8
#define MAIN_MENU_MAX_DEPTH 10
#define MAIN_MENU_LEN   (uint8_t)(MAIN_MENU_MAX_DEPTH * 12)
//------------------------------------|000011112222000011112222000011112222000011112222|
//const uint8_t  menu[MAIN_MENU_LEN] = {"  LINE 01     LINE 02     LINE 03     LINE 04   "};
static const uint8_t  menu[] = {"  LINE 01     LINE 02     LINE 03     LINE 04     LINE 05     LINE 06     LINE 07     LINE 08     LINE 09     LINE 10    "};

extern bool isMirror;
static uint8_t select_line=0;

void main_menu(uint8_t select) {
    if (select >= MAIN_MENU_MAX_DEPTH)
        return;

    ste2007_clear();
#if MAIN_MENU_MAX_DEPTH > LCD_DEPTH
    uint8_t start;
    if (select < LCD_DEPTH)
        start=0;
    else
        start=(select-LCD_DEPTH+1);

    for (uint16_t i=(start*LCD_HEIGHT); i<((start*LCD_HEIGHT)+96); i++) {
        if (i<(MAIN_MENU_MAX_DEPTH*LCD_X))
            cp866_send_char_8x8(menu[i],false);
    }
#else
    for (uint8_t i=0; i<MAIN_MENU_LEN; i++) {
        cp866_send_char_8x8(menu[i],false);
    }
#endif

    menu_inverse_line(select);
#ifdef DEBUG
    uart_print_str("select: ");
    uart_print_num(select);
    uart_send_char('\n');
#endif
    if (isMirror)
        ste2007_mirror();
    ste2007_display_fb();

    select_line=select;
}

void main_menu_enc(int offset) {
    int nl=(int)(select_line + offset);
    if (nl < 0) {
        nl=0;
    } else if (nl >= MAIN_MENU_MAX_DEPTH) {
        nl = (MAIN_MENU_MAX_DEPTH -1);
    }

    main_menu((uint8_t)nl);
    return;
}

void menu_inverse_line(uint8_t line) {
    if (line < LCD_DEPTH) {
        //pcd8544_clear_line(line);
        cp866_set_pos(0,line);
    } else {
        //pcd8544_clear_line(LCD_Y-1);
        cp866_set_pos(0,LCD_DEPTH-1);
    }


    line = line *12;
    for (uint8_t i=line; i<(line+12); i++) {
        uint8_t c=menu[i];
        cp866_send_char_8x8(c,true);
    }

}
