#ifndef __SSD1306_H__
#define __SSD1306_H__
#include <stdint.h>
#include "i2c.h"

#define SSD1306_I2C_ADDRESS (uint8_t)0x3C
#define ssd1306_clean() ssd1306_fill(0x0)

#define SSD1306_OK (uint8_t)0x01
#define SSD1306_ERROR (uint8_t)0x0

#define ssd1306_tunr_off i2c_send_cmd(0xAE)

uint8_t ssd1306_init();

#endif
