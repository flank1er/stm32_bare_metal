#ifndef __CP866_H__
#define __CP866_H__
#include "stdbool.h"
#include "cp866_font.h"

// alias
#define  lcd_print_8x8(x,y,z)   cp866_print_str_8x8(x,y,z)
#define  lcd_print_invert_8x8(s,x,y) cp866_print_str_inverse_8x8(s,x,y)
#define  lcd_print_big(s,x,y) cp866_send_char_size2_fb(s,x,y)
#define  lcd_print_hugo(s,x,y) cp866_send_char_size3_fb(s,x,y)

void cp866_send_char_8x8(uint8_t ch,bool inverse);
void cp866_print_str_8x8(char *str, uint8_t x, uint8_t y);
void cp866_print_str_inverse_8x8(char *str, uint8_t x, uint8_t y);
// scale font
void cp866_send_char_size2_fb(uint8_t ch, uint8_t x, uint8_t y);
void cp866_send_char_size3_fb(uint8_t ch, uint8_t x, uint8_t y);
void cp866_print_at_fb(char *str, uint8_t size,  uint8_t x, uint8_t y);
void cp866_print_uint8_at(uint8_t num, uint8_t size, uint8_t x, uint8_t y);
void cp866_set_pos(uint8_t x, uint8_t y);
void cp866_print_num(uint8_t num, uint8_t x, uint8_t y);
void cp866_print_split_num(uint8_t num, uint8_t x, uint8_t y);
// radio
void fm_radio_interface(uint16_t freq);
void fm_radio_interface2(uint16_t freq);
void fm_radio_interface3(uint16_t freq);

#endif // __CP866_H__
