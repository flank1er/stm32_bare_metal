#ifndef __CYBERCAFE_H__
#define __CYBERCAFE_H__
#include "cybercafe_font.h"

void cybercafe_print_str(char *str, uint8_t x, uint8_t y);
void cybercafe_send(uint8_t ch);
void cybercafe_print_uint8(uint8_t num, uint8_t x, uint8_t y);
/// -------------
void cybercafe_digits_send(uint8_t ch);
void cybercafe_print_huge_uint8(uint8_t num, uint8_t x, uint8_t y);
void cybercafe_print_large_uint8(uint8_t num, uint8_t x, uint8_t y);

#endif 	// __CYBERCAFE_FONT_H__
