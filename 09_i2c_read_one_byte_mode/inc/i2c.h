#ifndef __I2C_H__
#define __I2C_H__

#include "stm32f10x.h"
#include "stm32f10x_i2c.h"

/* I2C SPE mask */
#define I2C_CR1_PE_Set              ((uint16_t)0x0001)
#define I2C_CR1_PE_Reset            ((uint16_t)0xFFFE)

/* I2C START mask */
#define I2C_CR1_START_Set           ((uint16_t)0x0100)
#define I2C_CR1_START_Reset         ((uint16_t)0xFEFF)

/* I2C STOP mask */
#define I2C_CR1_STOP_Set            ((uint16_t)0x0200)
#define I2C_CR1_STOP_Reset          ((uint16_t)0xFDFF)

/* I2C ACK mask */
#define I2C_CR1_ACK_Set             ((uint16_t)0x0400)
#define I2C_CR1_ACK_Reset           ((uint16_t)0xFBFF)

/* I2C ENGC mask */
#define I2C_CR1_ENGC_Set            ((uint16_t)0x0040)
#define I2C_CR1_ENGC_Reset          ((uint16_t)0xFFBF)

/* I2C SWRST mask */
#define I2C_CR1_SWRST_Set           ((uint16_t)0x8000)
#define I2C_CR1_SWRST_Reset         ((uint16_t)0x7FFF)

/* I2C PEC mask */
#define I2C_CR1_PEC_Set             ((uint16_t)0x1000)
#define I2C_CR1_PEC_Reset           ((uint16_t)0xEFFF)

/* I2C ENPEC mask */
#define I2C_CR1_ENPEC_Set           ((uint16_t)0x0020)
#define I2C_CR1_ENPEC_Reset         ((uint16_t)0xFFDF)

/* I2C ENARP mask */
#define I2C_CR1_ENARP_Set           ((uint16_t)0x0010)
#define I2C_CR1_ENARP_Reset         ((uint16_t)0xFFEF)

/* I2C NOSTRETCH mask */
#define I2C_CR1_NOSTRETCH_Set       ((uint16_t)0x0080)
#define I2C_CR1_NOSTRETCH_Reset     ((uint16_t)0xFF7F)

/* I2C registers Masks */
#define I2C_CR1_CLEAR_Mask          ((uint16_t)0xFBF5)

/* I2C DMAEN mask */
#define I2C_CR2_DMAEN_Set           ((uint16_t)0x0800)
#define I2C_CR2_DMAEN_Reset         ((uint16_t)0xF7FF)

/* I2C LAST mask */
#define I2C_CR2_LAST_Set            ((uint16_t)0x1000)
#define I2C_CR2_LAST_Reset          ((uint16_t)0xEFFF)

/* I2C FREQ mask */
#define I2C_CR2_FREQ_Reset          ((uint16_t)0xFFC0)

/* I2C ADD0 mask */
#define I2C_OAR1_ADD0_Set           ((uint16_t)0x0001)
#define I2C_OAR1_ADD0_Reset         ((uint16_t)0xFFFE)

/* I2C ENDUAL mask */
#define I2C_OAR2_ENDUAL_Set         ((uint16_t)0x0001)
#define I2C_OAR2_ENDUAL_Reset       ((uint16_t)0xFFFE)

/* I2C ADD2 mask */
#define I2C_OAR2_ADD2_Reset         ((uint16_t)0xFF01)

/* I2C F/S mask */
#define I2C_CCR_FS_Set              ((uint16_t)0x8000)

/* I2C CCR mask */
#define I2C_CCR_CCR_Set             ((uint16_t)0x0FFF)

/* I2C FLAG mask */
#define I2C_FLAG_Mask               ((uint32_t)0x00FFFFFF)

/* I2C Interrupt Enable mask */
#define I2C_ITEN_Mask               ((uint32_t)0x07000000)

#define DS3231_I2C_ADDR (0x68<<1)
#define DS3231_CONTROL_REG ((uint8_t)0x0E)
#define DS3231_STATUS_REG ((uint8_t)0x0F)

#define LAST    ((uint8_t)0x01)
#define NOLAST  ((uint8_t)0x00)

#define enable_i2c      I2C1->CR1 |= I2C_CR1_PE_Set;    // =1, disable I2C
#define disable_i2c     I2C1->CR1 &= I2C_CR1_PE_Reset;  // disable I2C
#define stop_i2c        I2C1->CR1 |= I2C_CR1_STOP_Set;  //=0x0200, send STOP

uint8_t init_i2c(uint8_t adr, uint8_t value, uint8_t last);
uint8_t read_byte(uint8_t adr);

#endif
