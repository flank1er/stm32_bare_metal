#include "led.h"
#include "spi.h"

const uint16_t  digit[10] = {
      0xC000, // 0
      0xF900, // 1
      0xA400, // 2
      0xB000, // 3
      0x9900, // 4
      0x9200, // 5
      0x8200, // 6
      0xF800, // 7
      0x8000, // 8
      0x9000, // 9
};

void to_led(uint8_t value, uint16_t reg) {
	GPIOA->BSRR = RCLK;
	SPI1->DR=(digit[value]|reg);
    while (!(SPI1->SR & SPI_I2S_FLAG_TXE) || (SPI1->SR & SPI_I2S_FLAG_BSY));
	GPIOA->BRR  = RCLK;
}

void show_led() {
    switch (reg) {
    case 0:
        to_led((uint8_t)(led%10),1);
        break;
    case 1:
        if (led>=10)
            to_led((uint8_t)((led%100)/10),2);
        break;
    case 2:
        if (led>=100)
            to_led((uint8_t)((led%1000)/100),4);
        break;
    case 3:
        if (led>=1000)
            to_led((uint8_t)(led/1000),8);
        break;
     }
    reg = (reg ==  3) ? 0 : reg+1;
}
