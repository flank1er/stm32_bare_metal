#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"

extern void delay(uint32_t ms);
static __IO uint32_t s_timer;

void SysTick_Handler(void)
{
		if (s_timer)
			s_timer--;
}

void delay_ms(__IO uint32_t val) {
    // ------- SysTick CONFIG --------------
    if (SysTick_Config(72000)) // set 1ms
    {
        while(1); // error
    }

	s_timer=val;

    while(s_timer) {
		asm("wfi");
	};

	SysTick->LOAD &= ~(SysTick_CTRL_ENABLE_Msk);	// disable SysTick
}

int main()
{
    // enable GPIOC port
    RCC->APB2ENR |= RCC_APB2Periph_GPIOC;
    // --- GPIO setup ----
    GPIOC->CRH &= ~(uint32_t)(0xf<<20);
    GPIOC->CRH |=  (uint32_t)(0x2<<20);
	// Let's go..
	__enable_irq();
    for(;;){
		GPIOC->ODR ^= GPIO_Pin_13;
		delay_ms(1000);
    }

}
