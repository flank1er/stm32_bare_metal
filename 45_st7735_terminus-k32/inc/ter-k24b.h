#ifndef __TERMINUS_K12x24B_H__
#define __TERMINUS_K12x24B_H__
#include <stdint.h>
// Terminus  Console Font
// https://terminus-font.sourceforge.net/
// License: OFL
// size 12x24 px, bold
// codepage KOI8-R
//
// converted by flanker from PSF2 file format.
// compressed size: 2007 max lenght: 24
// compression algorithm: RLE
// www.count-zero.ru
// date 15 May 2023
//
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!! Horizontal endianness !!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#define TERMINUS_K12x24B_LENGTH           256      // full ASCII+KOI8-R code page
#define TERMINUS_K12x24N_START_CHAR       0
#define TERMINUS_K12x24B_CHAR_WIDTH       12
#define TERMINUS_K12x24B_CHAR_HEIGHT      2
#define TERMINUS_K12x24B_ARRAY_LENGTH     2007     // compressed

static const uint16_t TERMINUS_K12x24B[TERMINUS_K12x24B_ARRAY_LENGTH]= {
// первые 32 символа
0x4000, 0x17fe, 0xd606, 0x17fe, 0x5000,                                                                                                                   // Символ: 0 0x0 lenght: 5 offset: 0
0x4000, 0x13fc, 0x1606, 0x2c03, 0x2d9b, 0x3c03, 0x1dfb, 0x1cf3, 0x2c03, 0x1606, 0x13fc, 0x5000,                                                           // Символ: 1 0x1 lenght: 12 offset: 5
0x4000, 0x13fc, 0x17fe, 0x2fff, 0x2e67, 0x3fff, 0x1e07, 0x1f0f, 0x2fff, 0x17fe, 0x13fc, 0x5000,                                                           // Символ: 2 0x2 lenght: 12 offset: 17
0x6000, 0x139c, 0x179e, 0x5fff, 0x17fe, 0x13fc, 0x11f8, 0x10f0, 0x1060, 0x6000,                                                                           // Символ: 3 0x3 lenght: 10 offset: 29
0x6000, 0x1060, 0x10f0, 0x11f8, 0x13fc, 0x17fe, 0x2fff, 0x17fe, 0x13fc, 0x11f8, 0x10f0, 0x1060, 0x6000,                                                   // Символ: 4 0x4 lenght: 13 offset: 39
0x4000, 0x10f0, 0x21f8, 0x10f0, 0x2060, 0x176e, 0x3fff, 0x176e, 0x3060, 0x11f8, 0x5000,                                                                   // Символ: 5 0x5 lenght: 11 offset: 52
0x4000, 0x2060, 0x10f0, 0x11f8, 0x13fc, 0x57fe, 0x136c, 0x3060, 0x11f8, 0x5000,                                                                           // Символ: 6 0x6 lenght: 10 offset: 63
0x9000, 0x10f0, 0x41f8, 0x10f0, 0x9000,                                                                                                                   // Символ: 7 0x7 lenght: 5 offset: 73
0x9fff, 0x1f0f, 0x4e07, 0x1f0f, 0x9fff,                                                                                                                   // Символ: 8 0x8 lenght: 5 offset: 78
0x9000, 0x10f0, 0x1198, 0x2108, 0x1198, 0x10f0, 0x9000,                                                                                                   // Символ: 9 0x9 lenght: 7 offset: 83
0x9fff, 0x1f0f, 0x1e67, 0x2ef7, 0x1e67, 0x1f0f, 0x9fff,                                                                                                   // Символ: 10 0xa lenght: 7 offset: 90
0x4000, 0x10fe, 0x100e, 0x101a, 0x1032, 0x1062, 0x10c2, 0x13f0, 0x1618, 0x5c0c, 0x1618, 0x13f0, 0x5000,                                                   // Символ: 11 0xb lenght: 13 offset: 97
0x4000, 0x11f8, 0x130c, 0x5606, 0x130c, 0x11f8, 0x2060, 0x17fe, 0x3060, 0x5000,                                                                           // Символ: 12 0xc lenght: 10 offset: 110
0x4000, 0x13fe, 0x3306, 0x13fe, 0x8300, 0x1f00, 0x1e00, 0x5000,                                                                                           // Символ: 13 0xd lenght: 8 offset: 120
0x4000, 0x17fe, 0x3606, 0x17fe, 0x8606, 0x160c, 0x1c00, 0x5000,                                                                                           // Символ: 14 0xe lenght: 8 offset: 128
0x4000, 0x2060, 0x1c63, 0x1666, 0x136c, 0x11f8, 0x10f0, 0x1f9f, 0x10f0, 0x11f8, 0x136c, 0x1666, 0x1c63, 0x2060, 0x5000,                                   // Символ: 15 0xf lenght: 15 offset: 136
0x6000, 0x1c00, 0x1f00, 0x1fc0, 0x1ff0, 0x1ffc, 0x2fff, 0x1ffc, 0x1ff0, 0x1fc0, 0x1f00, 0x1c00, 0x6000,                                                   // Символ: 16 0x10 lenght: 13 offset: 151
0x6000, 0x1003, 0x100f, 0x103f, 0x10ff, 0x13ff, 0x2fff, 0x13ff, 0x10ff, 0x103f, 0x100f, 0x1003, 0x6000,                                                   // Символ: 17 0x11 lenght: 13 offset: 164
0x4000, 0x1060, 0x10f0, 0x11f8, 0x136c, 0x1666, 0x5060, 0x1666, 0x136c, 0x11f8, 0x10f0, 0x1060, 0x5000,                                                   // Символ: 18 0x12 lenght: 13 offset: 177
0x4000, 0xa198, 0x2000, 0x3198, 0x5000,                                                                                                                   // Символ: 19 0x13 lenght: 5 offset: 190
0x4000, 0x13fe, 0x6666, 0x13e6, 0x7066, 0x5000,                                                                                                           // Символ: 20 0x14 lenght: 6 offset: 195
0x3000, 0x11f0, 0x1318, 0x2300, 0x11e0, 0x1330, 0x4318, 0x1198, 0x10f0, 0x2018, 0x1318, 0x11f0, 0x5000,                                                   // Символ: 21 0x15 lenght: 13 offset: 201
0xd000, 0x6ffe, 0x5000,                                                                                                                                   // Символ: 22 0x16 lenght: 3 offset: 214
0x4000, 0x1060, 0x10f0, 0x11f8, 0x136c, 0x1666, 0x4060, 0x1666, 0x136c, 0x11f8, 0x10f0, 0x1060, 0x17fe, 0x5000,                                           // Символ: 23 0x17 lenght: 14 offset: 217
0x4000, 0x1060, 0x10f0, 0x11f8, 0x136c, 0x1666, 0xa060, 0x5000,                                                                                           // Символ: 24 0x18 lenght: 8 offset: 231
0x4000, 0xa060, 0x1666, 0x136c, 0x11f8, 0x10f0, 0x1060, 0x5000,                                                                                           // Символ: 25 0x19 lenght: 8 offset: 239
0x7000, 0x1020, 0x1030, 0x1018, 0x100c, 0x2ffe, 0x100c, 0x1018, 0x1030, 0x1020, 0x7000,                                                                   // Символ: 26 0x1a lenght: 11 offset: 247
0x7000, 0x1080, 0x1180, 0x1300, 0x1600, 0x2ffe, 0x1600, 0x1300, 0x1180, 0x1080, 0x7000,                                                                   // Символ: 27 0x1b lenght: 11 offset: 258
0x7000, 0x8600, 0x17fe, 0x8000,                                                                                                                           // Символ: 28 0x1c lenght: 4 offset: 269
0x7000, 0x1090, 0x1198, 0x130c, 0x1606, 0x2fff, 0x1606, 0x130c, 0x1198, 0x1090, 0x7000,                                                                   // Символ: 29 0x1d lenght: 11 offset: 273
0x6000, 0x2060, 0x20f0, 0x21f8, 0x23fc, 0x27fe, 0x2fff, 0x6000,                                                                                           // Символ: 30 0x1e lenght: 8 offset: 284
0x6000, 0x2fff, 0x27fe, 0x23fc, 0x21f8, 0x20f0, 0x2060, 0x6000,                                                                                           // Символ: 31 0x1f lenght: 8 offset: 292
// Знаки
0x0, 0x8000,                                                                                                                                              // Символ: 32 0x20 lenght: 2 offset: 300
0x4000, 0xa060, 0x2000, 0x3060, 0x5000,                                                                                                                   // Символ: 33 0x21 lenght: 5 offset: 302
0x2000, 0x5198, 0x0, 0x1000,                                                                                                                              // Символ: 34 0x22 lenght: 4 offset: 307
0x4000, 0x4198, 0x17fe, 0x5198, 0x17fe, 0x4198, 0x5000,                                                                                                   // Символ: 35 0x23 lenght: 7 offset: 311
0x3000, 0x2060, 0x11f8, 0x136c, 0x1666, 0x2660, 0x1360, 0x11f8, 0x106c, 0x2066, 0x1666, 0x136c, 0x11f8, 0x2060, 0x4000,                                   // Символ: 36 0x24 lenght: 15 offset: 318
0x5000, 0x138c, 0x16cc, 0x16d8, 0x1398, 0x2030, 0x2060, 0x20c0, 0x119c, 0x11b6, 0x1336, 0x131c, 0x5000,                                                   // Символ: 37 0x25 lenght: 13 offset: 333
0x4000, 0x10e0, 0x11b0, 0x3318, 0x11b0, 0x10e0, 0x11e6, 0x1336, 0x161c, 0x260c, 0x161c, 0x1336, 0x11e6, 0x5000,                                           // Символ: 38 0x26 lenght: 14 offset: 346
0x2000, 0x5060, 0x0, 0x1000,                                                                                                                              // Символ: 39 0x27 lenght: 4 offset: 360
0x4000, 0x1030, 0x1060, 0x20c0, 0x7180, 0x20c0, 0x1060, 0x1030, 0x5000,                                                                                   // Символ: 40 0x28 lenght: 9 offset: 364
0x4000, 0x1180, 0x10c0, 0x2060, 0x7030, 0x2060, 0x10c0, 0x1180, 0x5000,                                                                                   // Символ: 41 0x29 lenght: 9 offset: 373
0x7000, 0x160c, 0x1318, 0x11b0, 0x10e0, 0x1ffe, 0x10e0, 0x11b0, 0x1318, 0x160c, 0x8000,                                                                   // Символ: 42 0x2a lenght: 11 offset: 382
0x7000, 0x4060, 0x17fe, 0x4060, 0x8000,                                                                                                                   // Символ: 43 0x2b lenght: 5 offset: 393
0xf000, 0x4060, 0x10c0, 0x4000,                                                                                                                           // Символ: 44 0x2c lenght: 4 offset: 398
0xb000, 0x17fe, 0xc000,                                                                                                                                   // Символ: 45 0x2d lenght: 3 offset: 402
0x0, 0x3060, 0x5000,                                                                                                                                      // Символ: 46 0x2e lenght: 3 offset: 405
0x5000, 0x200c, 0x2018, 0x2030, 0x2060, 0x20c0, 0x2180, 0x2300, 0x5000,                                                                                   // Символ: 47 0x2f lenght: 9 offset: 408
// Digits, Цифры
0x4000, 0x11f8, 0x130c, 0x2606, 0x160e, 0x161e, 0x1636, 0x1666, 0x16c6, 0x1786, 0x1706, 0x2606, 0x130c, 0x11f8, 0x5000,                                   // Символ: 48 0x30 lenght: 15 offset: 417
0x4000, 0x1060, 0x10e0, 0x11e0, 0x1360, 0xa060, 0x13fc, 0x5000,                                                                                           // Символ: 49 0x31 lenght: 8 offset: 432
0x4000, 0x11f8, 0x130c, 0x3606, 0x1006, 0x100c, 0x1018, 0x1030, 0x1060, 0x10c0, 0x1180, 0x1300, 0x1600, 0x17fe, 0x5000,                                   // Символ: 50 0x32 lenght: 15 offset: 440
0x4000, 0x11f8, 0x130c, 0x1606, 0x3006, 0x100c, 0x10f8, 0x100c, 0x3006, 0x1606, 0x130c, 0x11f8, 0x5000,                                                   // Символ: 51 0x33 lenght: 13 offset: 455
0x4000, 0x1006, 0x100e, 0x101e, 0x1036, 0x1066, 0x10c6, 0x1186, 0x1306, 0x3606, 0x17fe, 0x3006, 0x5000,                                                   // Символ: 52 0x34 lenght: 13 offset: 468
0x4000, 0x17fe, 0x5600, 0x17f8, 0x100c, 0x4006, 0x1606, 0x130c, 0x11f8, 0x5000,                                                                           // Символ: 53 0x35 lenght: 10 offset: 481
0x4000, 0x11fc, 0x1300, 0x4600, 0x17f8, 0x160c, 0x5606, 0x130c, 0x11f8, 0x5000,                                                                           // Символ: 54 0x36 lenght: 10 offset: 491
0x4000, 0x17fe, 0x2606, 0x1006, 0x200c, 0x2018, 0x2030, 0x5060, 0x5000,                                                                                   // Символ: 55 0x37 lenght: 9 offset: 501
0x4000, 0x11f8, 0x130c, 0x4606, 0x130c, 0x11f8, 0x130c, 0x4606, 0x130c, 0x11f8, 0x5000,                                                                   // Символ: 56 0x38 lenght: 11 offset: 510
0x4000, 0x11f8, 0x130c, 0x5606, 0x1306, 0x11fe, 0x4006, 0x100c, 0x13f8, 0x5000,                                                                           // Символ: 57 0x39 lenght: 10 offset: 521

0x8000, 0x3060, 0x4000, 0x3060, 0x6000,                                                                                                                   // Символ: 58 0x3a lenght: 5 offset: 531
0x8000, 0x3060, 0x4000, 0x4060, 0x10c0, 0x4000,                                                                                                           // Символ: 59 0x3b lenght: 6 offset: 536
0x4000, 0x100c, 0x1018, 0x1030, 0x1060, 0x10c0, 0x1180, 0x1300, 0x1600, 0x1300, 0x1180, 0x10c0, 0x1060, 0x1030, 0x1018, 0x100c, 0x5000,                   // Символ: 60 0x3c lenght: 17 offset: 542
0x9000, 0x17fe, 0x4000, 0x17fe, 0x9000,                                                                                                                   // Символ: 61 0x3d lenght: 5 offset: 559
0x4000, 0x1600, 0x1300, 0x1180, 0x10c0, 0x1060, 0x1030, 0x1018, 0x100c, 0x1018, 0x1030, 0x1060, 0x10c0, 0x1180, 0x1300, 0x1600, 0x5000,                   // Символ: 62 0x3e lenght: 17 offset: 564
0x4000, 0x11f8, 0x130c, 0x3606, 0x100c, 0x1018, 0x1030, 0x2060, 0x2000, 0x3060, 0x5000,                                                                   // Символ: 63 0x3f lenght: 11 offset: 581
0x4000, 0x13f8, 0x160c, 0x1c06, 0x1c3e, 0x1c66, 0x5cc6, 0x1c66, 0x1c3e, 0x1c00, 0x1600, 0x13fe, 0x5000,                                                   // Символ: 64 0x40 lenght: 13 offset: 592
// Roman Capitals / Латиница, прописные
0x4000, 0x11f8, 0x130c, 0x6606, 0x17fe, 0x6606, 0x5000,                                                                                                   // Символ: 65 0x41 lenght: 7 offset: 605
0x4000, 0x17f8, 0x160c, 0x3606, 0x160c, 0x17f8, 0x160c, 0x5606, 0x160c, 0x17f8, 0x5000,                                                                   // Символ: 66 0x42 lenght: 11 offset: 612
0x4000, 0x11f8, 0x130c, 0x2606, 0x7600, 0x2606, 0x130c, 0x11f8, 0x5000,                                                                                   // Символ: 67 0x43 lenght: 9 offset: 623
0x4000, 0x17f8, 0x160c, 0xb606, 0x160c, 0x17f8, 0x5000,                                                                                                   // Символ: 68 0x44 lenght: 7 offset: 632
0x4000, 0x17fe, 0x6600, 0x17f8, 0x6600, 0x17fe, 0x5000,                                                                                                   // Символ: 69 0x45 lenght: 7 offset: 639
0x4000, 0x17fe, 0x6600, 0x17f8, 0x7600, 0x5000,                                                                                                           // Символ: 70 0x46 lenght: 6 offset: 646
0x4000, 0x11f8, 0x130c, 0x2606, 0x3600, 0x163e, 0x5606, 0x130c, 0x11f8, 0x5000,                                                                           // Символ: 71 0x47 lenght: 10 offset: 652
0x4000, 0x7606, 0x17fe, 0x7606, 0x5000,                                                                                                                   // Символ: 72 0x48 lenght: 5 offset: 662
0x4000, 0x11f8, 0xd060, 0x11f8, 0x5000,                                                                                                                   // Символ: 73 0x49 lenght: 5 offset: 667
0x4000, 0x103f, 0x900c, 0x360c, 0x1318, 0x11f0, 0x5000,                                                                                                   // Символ: 74 0x4a lenght: 7 offset: 672
0x4000, 0x1606, 0x160c, 0x1618, 0x1630, 0x1660, 0x16c0, 0x1780, 0x1700, 0x1780, 0x16c0, 0x1660, 0x1630, 0x1618, 0x160c, 0x1606, 0x5000,                   // Символ: 75 0x4b lenght: 17 offset: 679
0x4000, 0xe600, 0x17fe, 0x5000,                                                                                                                           // Символ: 76 0x4c lenght: 4 offset: 696
0x4000, 0x1802, 0x1c06, 0x1e0e, 0x1f1e, 0x1db6, 0x1ce6, 0x1c46, 0x8c06, 0x5000,                                                                           // Символ: 77 0x4d lenght: 10 offset: 700
0x4000, 0x4606, 0x1706, 0x1786, 0x16c6, 0x1666, 0x1636, 0x161e, 0x160e, 0x4606, 0x5000,                                                                   // Символ: 78 0x4e lenght: 11 offset: 710
0x4000, 0x11f8, 0x130c, 0xb606, 0x130c, 0x11f8, 0x5000,                                                                                                   // Символ: 79 0x4f lenght: 7 offset: 721
0x4000, 0x17f8, 0x160c, 0x4606, 0x160c, 0x17f8, 0x7600, 0x5000,                                                                                           // Символ: 80 0x50 lenght: 8 offset: 728
0x4000, 0x11f8, 0x130c, 0xa606, 0x1666, 0x133c, 0x11f8, 0x100c, 0x1006, 0x3000,                                                                           // Символ: 81 0x51 lenght: 10 offset: 736
0x4000, 0x17f8, 0x160c, 0x4606, 0x160c, 0x17f8, 0x1780, 0x16c0, 0x1660, 0x1630, 0x1618, 0x160c, 0x1606, 0x5000,                                           // Символ: 82 0x52 lenght: 14 offset: 746
0x4000, 0x11f8, 0x130c, 0x1606, 0x3600, 0x1300, 0x11f8, 0x100c, 0x3006, 0x1606, 0x130c, 0x11f8, 0x5000,                                                   // Символ: 83 0x53 lenght: 13 offset: 760
0x4000, 0x17fe, 0xe060, 0x5000,                                                                                                                           // Символ: 84 0x54 lenght: 4 offset: 773
0x4000, 0xd606, 0x130c, 0x11f8, 0x5000,                                                                                                                   // Символ: 85 0x55 lenght: 5 offset: 777
0x4000, 0x4606, 0x430c, 0x3198, 0x20f0, 0x2060, 0x5000,                                                                                                   // Символ: 86 0x56 lenght: 7 offset: 782
0x4000, 0x8c06, 0x1c46, 0x1ce6, 0x1db6, 0x1f1e, 0x1e0e, 0x1c06, 0x1802, 0x5000,                                                                           // Символ: 87 0x57 lenght: 10 offset: 789
0x4000, 0x2606, 0x230c, 0x2198, 0x10f0, 0x1060, 0x10f0, 0x2198, 0x230c, 0x2606, 0x5000,                                                                   // Символ: 88 0x58 lenght: 11 offset: 799
0x4000, 0x2606, 0x230c, 0x2198, 0x20f0, 0x7060, 0x5000,                                                                                                   // Символ: 89 0x59 lenght: 7 offset: 810
0x4000, 0x17fe, 0x3006, 0x100c, 0x1018, 0x1030, 0x1060, 0x10c0, 0x1180, 0x1300, 0x3600, 0x17fe, 0x5000,                                                   // Символ: 90 0x5a lenght: 13 offset: 817

0x4000, 0x11f0, 0xd180, 0x11f0, 0x5000,                                                                                                                   // Символ: 91 0x5b lenght: 5 offset: 830
0x5000, 0x2300, 0x2180, 0x20c0, 0x2060, 0x2030, 0x2018, 0x200c, 0x5000,                                                                                   // Символ: 92 0x5c lenght: 9 offset: 835
0x4000, 0x11f0, 0xd030, 0x11f0, 0x5000,                                                                                                                   // Символ: 93 0x5d lenght: 5 offset: 844
0x2000, 0x1060, 0x10f0, 0x1198, 0x130c, 0x1606, 0x0, 0x1000,                                                                                              // Символ: 94 0x5e lenght: 8 offset: 849
0x0, 0x4000, 0x17fe, 0x3000,                                                                                                                              // Символ: 95 0x5f lenght: 4 offset: 857
0x1000, 0x1180, 0x10c0, 0x1060, 0x0, 0x4000,                                                                                                              // Символ: 96 0x60 lenght: 6 offset: 861
// Roman Smalls / Латиница, строчные
0x8000, 0x13f8, 0x100c, 0x2006, 0x11fe, 0x1306, 0x3606, 0x1306, 0x11fe, 0x5000,                                                                           // Символ: 97 0x61 lenght: 10 offset: 867
0x4000, 0x4600, 0x17f8, 0x160c, 0x7606, 0x160c, 0x17f8, 0x5000,                                                                                           // Символ: 98 0x62 lenght: 8 offset: 877
0x8000, 0x11f8, 0x130c, 0x1606, 0x5600, 0x1606, 0x130c, 0x11f8, 0x5000,                                                                                   // Символ: 99 0x63 lenght: 9 offset: 885
0x4000, 0x4006, 0x11fe, 0x1306, 0x7606, 0x1306, 0x11fe, 0x5000,                                                                                           // Символ: 100 0x64 lenght: 8 offset: 894
0x8000, 0x11f8, 0x130c, 0x3606, 0x17fe, 0x3600, 0x1306, 0x11fc, 0x5000,                                                                                   // Символ: 101 0x65 lenght: 9 offset: 902
0x4000, 0x103e, 0x3060, 0x13fc, 0xa060, 0x5000,                                                                                                           // Символ: 102 0x66 lenght: 6 offset: 911
0x8000, 0x11fe, 0x1306, 0x7606, 0x130e, 0x11fe, 0x2006, 0x100c, 0x13f8, 0x1000,                                                                           // Символ: 103 0x67 lenght: 10 offset: 917
0x4000, 0x4600, 0x17f8, 0x160c, 0x9606, 0x5000,                                                                                                           // Символ: 104 0x68 lenght: 6 offset: 927
0x4000, 0x3060, 0x1000, 0x11e0, 0x9060, 0x11f8, 0x5000,                                                                                                   // Символ: 105 0x69 lenght: 7 offset: 933
0x4000, 0x300c, 0x1000, 0x103c, 0xa00c, 0x230c, 0x1198, 0x10f0, 0x1000,                                                                                   // Символ: 106 0x6a lenght: 9 offset: 940
0x4000, 0x4300, 0x1306, 0x130c, 0x1318, 0x1330, 0x1360, 0x13c0, 0x1360, 0x1330, 0x1318, 0x130c, 0x1306, 0x5000,                                           // Символ: 107 0x6b lenght: 14 offset: 949
0x4000, 0x11e0, 0xd060, 0x11f8, 0x5000,                                                                                                                   // Символ: 108 0x6c lenght: 5 offset: 963
0x8000, 0x17f8, 0x166c, 0x9666, 0x5000,                                                                                                                   // Символ: 109 0x6d lenght: 5 offset: 968
0x8000, 0x17f8, 0x160c, 0x9606, 0x5000,                                                                                                                   // Символ: 110 0x6e lenght: 5 offset: 973
0x8000, 0x11f8, 0x130c, 0x7606, 0x130c, 0x11f8, 0x5000,                                                                                                   // Символ: 111 0x6f lenght: 7 offset: 978
0x8000, 0x17f8, 0x160c, 0x7606, 0x160c, 0x17f8, 0x4600, 0x1000,                                                                                           // Символ: 112 0x70 lenght: 8 offset: 985
0x8000, 0x11fe, 0x1306, 0x7606, 0x1306, 0x11fe, 0x4006, 0x1000,                                                                                           // Символ: 113 0x71 lenght: 8 offset: 993
0x8000, 0x167e, 0x16c0, 0x1780, 0x1700, 0x7600, 0x5000,                                                                                                   // Символ: 114 0x72 lenght: 7 offset: 1001
0x8000, 0x13fc, 0x1606, 0x3600, 0x13fc, 0x3006, 0x1606, 0x13fc, 0x5000,                                                                                   // Символ: 115 0x73 lenght: 9 offset: 1008
0x4000, 0x40c0, 0x17f8, 0x90c0, 0x107c, 0x5000,                                                                                                           // Символ: 116 0x74 lenght: 6 offset: 1017
0x8000, 0x9606, 0x1306, 0x11fe, 0x5000,                                                                                                                   // Символ: 117 0x75 lenght: 5 offset: 1023
0x8000, 0x3606, 0x230c, 0x2198, 0x20f0, 0x2060, 0x5000,                                                                                                   // Символ: 118 0x76 lenght: 7 offset: 1028
0x8000, 0x4606, 0x6666, 0x13fc, 0x5000,                                                                                                                   // Символ: 119 0x77 lenght: 5 offset: 1035
0x8000, 0x2606, 0x130c, 0x1198, 0x10f0, 0x1060, 0x10f0, 0x1198, 0x130c, 0x2606, 0x5000,                                                                   // Символ: 120 0x78 lenght: 11 offset: 1040
0x8000, 0x9606, 0x130e, 0x11fe, 0x2006, 0x100c, 0x13f8, 0x1000,                                                                                           // Символ: 121 0x79 lenght: 8 offset: 1051
0x8000, 0x17fe, 0x1006, 0x100c, 0x1018, 0x1030, 0x1060, 0x10c0, 0x1180, 0x1300, 0x1600, 0x17fe, 0x5000,                                                   // Символ: 122 0x7a lenght: 13 offset: 1059

0x4000, 0x1038, 0x1060, 0x50c0, 0x1380, 0x50c0, 0x1060, 0x1038, 0x5000,                                                                                   // Символ: 123 0x7b lenght: 9 offset: 1072
0x4000, 0xf060, 0x5000,                                                                                                                                   // Символ: 124 0x7c lenght: 3 offset: 1081
0x4000, 0x1380, 0x10c0, 0x5060, 0x1038, 0x5060, 0x10c0, 0x1380, 0x5000,                                                                                   // Символ: 125 0x7d lenght: 9 offset: 1084
0x2000, 0x13c6, 0x2666, 0x163c, 0x0, 0x2000,                                                                                                              // Символ: 126 0x7e lenght: 6 offset: 1093
0x7000, 0x1060, 0x10f0, 0x1198, 0x130c, 0x7606, 0x17fe, 0x5000,                                                                                           // Символ: 127 0x7f lenght: 8 offset: 1099
//	Псевдографика
0xb000, 0x2fff, 0xb000,                                                                                                                                   // Символ: 128 0x80 lenght: 3 offset: 1107
0x60, 0x8060,                                                                                                                                             // Символ: 129 0x81 lenght: 2 offset: 1110
0xb000, 0x207f, 0xb060,                                                                                                                                   // Символ: 130 0x82 lenght: 3 offset: 1112
0xb000, 0x2fe0, 0xb060,                                                                                                                                   // Символ: 131 0x83 lenght: 3 offset: 1115
0xb060, 0x207f, 0xb000,                                                                                                                                   // Символ: 132 0x84 lenght: 3 offset: 1118
0xb060, 0x2fe0, 0xb000,                                                                                                                                   // Символ: 133 0x85 lenght: 3 offset: 1121
0xb060, 0x207f, 0xb060,                                                                                                                                   // Символ: 134 0x86 lenght: 3 offset: 1124
0xb060, 0x2fe0, 0xb060,                                                                                                                                   // Символ: 135 0x87 lenght: 3 offset: 1127
0xb000, 0x2fff, 0xb060,                                                                                                                                   // Символ: 136 0x88 lenght: 3 offset: 1130
0xb060, 0x2fff, 0xb000,                                                                                                                                   // Символ: 137 0x89 lenght: 3 offset: 1133
0xb060, 0x2fff, 0xb060,                                                                                                                                   // Символ: 138 0x8a lenght: 3 offset: 1136
0xcfff, 0xc000,                                                                                                                                           // Символ: 139 0x8b lenght: 2 offset: 1139
0xc000, 0xcfff,                                                                                                                                           // Символ: 140 0x8c lenght: 2 offset: 1141
0xfff, 0x8fff,                                                                                                                                            // Символ: 141 0x8d lenght: 2 offset: 1143
0xfc0, 0x8fc0,                                                                                                                                            // Символ: 142 0x8e lenght: 2 offset: 1145
0x3f, 0x803f,                                                                                                                                             // Символ: 143 0x8f lenght: 2 offset: 1147
0x1aaa, 0x1000, 0x1aaa, 0x1000, 0x1aaa, 0x1000, 0x1aaa, 0x1000, 0x1aaa, 0x1000, 0x1aaa, 0x1000, 0x1aaa, 0x1000, 0x1aaa, 0x1000, 0x1aaa, 0x1000, 0x1aaa, 0x1000, 0x1aaa, 0x1000, 0x1aaa, 0x1000, // Символ: 144 0x90 lenght: 24 offset: 1149
0x1aaa, 0x1555, 0x1aaa, 0x1555, 0x1aaa, 0x1555, 0x1aaa, 0x1555, 0x1aaa, 0x1555, 0x1aaa, 0x1555, 0x1aaa, 0x1555, 0x1aaa, 0x1555, 0x1aaa, 0x1555, 0x1aaa, 0x1555, 0x1aaa, 0x1555, 0x1aaa, 0x1555, // Символ: 145 0x91 lenght: 24 offset: 1173
0x1fff, 0x1aaa, 0x1fff, 0x1aaa, 0x1fff, 0x1aaa, 0x1fff, 0x1aaa, 0x1fff, 0x1aaa, 0x1fff, 0x1aaa, 0x1fff, 0x1aaa, 0x1fff, 0x1aaa, 0x1fff, 0x1aaa, 0x1fff, 0x1aaa, 0x1fff, 0x1aaa, 0x1fff, 0x1aaa, // Символ: 146 0x92 lenght: 24 offset: 1197
0x4000, 0x103c, 0x2066, 0x60, 0x1060,                                                                                                                     // Символ: 147 0x93 lenght: 5 offset: 1221
0x7000, 0xa3fc, 0x7000,                                                                                                                                   // Символ: 148 0x94 lenght: 3 offset: 1226
0x9000, 0x10f0, 0x41f8, 0x10f0, 0x9000,                                                                                                                   // Символ: 149 0x95 lenght: 5 offset: 1229
0x2000, 0x200e, 0x600c, 0x360c, 0x130c, 0x118c, 0x10cc, 0x106c, 0x103c, 0x101c, 0x5000,                                                                   // Символ: 150 0x96 lenght: 11 offset: 1234
0x8000, 0x13c6, 0x1666, 0x163c, 0x1000, 0x13c6, 0x1666, 0x163c, 0x9000,                                                                                   // Символ: 151 0x97 lenght: 9 offset: 1245
0x4000, 0x100c, 0x1018, 0x1030, 0x1060, 0x10c0, 0x1180, 0x1300, 0x1180, 0x10c0, 0x1060, 0x1030, 0x1018, 0x100c, 0x1000, 0x17fe, 0x5000,                   // Символ: 152 0x98 lenght: 17 offset: 1254
0x4000, 0x1300, 0x1180, 0x10c0, 0x1060, 0x1030, 0x1018, 0x100c, 0x1018, 0x1030, 0x1060, 0x10c0, 0x1180, 0x1300, 0x1000, 0x17fe, 0x5000,                   // Символ: 153 0x99 lenght: 17 offset: 1271
0x0, 0x8000,                                                                                                                                              // Символ: 154 0x9a lenght: 2 offset: 1288
0x60, 0x2660, 0x13c0, 0x5000,                                                                                                                             // Символ: 155 0x9b lenght: 4 offset: 1290
0x2000, 0x11f0, 0x4318, 0x11f0, 0x0,                                                                                                                      // Символ: 156 0x9c lenght: 5 offset: 1294
0x2000, 0x11f0, 0x2318, 0x1018, 0x1030, 0x1060, 0x10c0, 0x1180, 0x13f8, 0xd000,                                                                           // Символ: 157 0x9d lenght: 10 offset: 1299
0xa000, 0x3060, 0xb000,                                                                                                                                   // Символ: 158 0x9e lenght: 3 offset: 1309
0x6000, 0x3060, 0x2000, 0x17fe, 0x2000, 0x3060, 0x7000,                                                                                                   // Символ: 159 0x9f lenght: 7 offset: 1312
0x9000, 0x2fff, 0x2000, 0x2fff, 0x9000,                                                                                                                   // Символ: 160 0xa0 lenght: 5 offset: 1319
0x198, 0x8198,                                                                                                                                            // Символ: 161 0xa1 lenght: 2 offset: 1324
0x9000, 0x207f, 0x2060, 0x207f, 0x9060,                                                                                                                   // Символ: 162 0xa2 lenght: 5 offset: 1326
0x4000, 0x3198, 0x1000, 0x11f8, 0x130c, 0x3606, 0x17fe, 0x3600, 0x1306, 0x11fc, 0x5000,                                                                   // Символ: 163 0xa3 lenght: 11 offset: 1331
0xb000, 0x21ff, 0xb198,                                                                                                                                   // Символ: 164 0xa4 lenght: 3 offset: 1342
0x9000, 0x21ff, 0x2180, 0x219f, 0x9198,                                                                                                                   // Символ: 165 0xa5 lenght: 5 offset: 1345
0x9000, 0x2fe0, 0x2060, 0x2fe0, 0x9060,                                                                                                                   // Символ: 166 0xa6 lenght: 5 offset: 1350
0xb000, 0x2ff8, 0xb198,                                                                                                                                   // Символ: 167 0xa7 lenght: 3 offset: 1355
0x9000, 0x2ff8, 0x2018, 0x2f98, 0x9198,                                                                                                                   // Символ: 168 0xa8 lenght: 5 offset: 1358
0x9060, 0x207f, 0x2060, 0x207f, 0x9000,                                                                                                                   // Символ: 169 0xa9 lenght: 5 offset: 1363
0xb198, 0x21ff, 0xb000,                                                                                                                                   // Символ: 170 0xaa lenght: 3 offset: 1368
0x9198, 0x219f, 0x2180, 0x21ff, 0x9000,                                                                                                                   // Символ: 171 0xab lenght: 5 offset: 1371
0x9060, 0x2fe0, 0x2060, 0x2fe0, 0x9000,                                                                                                                   // Символ: 172 0xac lenght: 5 offset: 1376
0xb198, 0x2ff8, 0xb000,                                                                                                                                   // Символ: 173 0xad lenght: 3 offset: 1381
0x9198, 0x2f98, 0x2018, 0x2ff8, 0x9000,                                                                                                                   // Символ: 174 0xae lenght: 5 offset: 1384
0x9060, 0x207f, 0x2060, 0x207f, 0x9060,                                                                                                                   // Символ: 175 0xaf lenght: 5 offset: 1389
0xb198, 0x219f, 0xb198,                                                                                                                                   // Символ: 176 0xb0 lenght: 3 offset: 1394
0x9198, 0x219f, 0x2180, 0x219f, 0x9198,                                                                                                                   // Символ: 177 0xb1 lenght: 5 offset: 1397
0x9060, 0x2fe0, 0x2060, 0x2fe0, 0x9060,                                                                                                                   // Символ: 178 0xb2 lenght: 5 offset: 1402
0x3198, 0x1000, 0x17fe, 0x6600, 0x17f8, 0x6600, 0x17fe, 0x5000,                                                                                           // Символ: 179 0xb3 lenght: 8 offset: 1407
0xb198, 0x2f98, 0xb198,                                                                                                                                   // Символ: 180 0xb4 lenght: 3 offset: 1415
0x9198, 0x2f98, 0x2018, 0x2f98, 0x9198,                                                                                                                   // Символ: 181 0xb5 lenght: 5 offset: 1418
0x9000, 0x2fff, 0x2000, 0x2fff, 0x9060,                                                                                                                   // Символ: 182 0xb6 lenght: 5 offset: 1423
0xb000, 0x2fff, 0xb198,                                                                                                                                   // Символ: 183 0xb7 lenght: 3 offset: 1428
0x9000, 0x2fff, 0x2000, 0x2f9f, 0x9198,                                                                                                                   // Символ: 184 0xb8 lenght: 5 offset: 1431
0x9060, 0x2fff, 0x2000, 0x2fff, 0x9000,                                                                                                                   // Символ: 185 0xb9 lenght: 5 offset: 1436
0xb198, 0x2fff, 0xb000,                                                                                                                                   // Символ: 186 0xba lenght: 3 offset: 1441
0x9198, 0x2f9f, 0x2000, 0x2fff, 0x9000,                                                                                                                   // Символ: 187 0xbb lenght: 5 offset: 1444
0x9060, 0x2fff, 0x2060, 0x2fff, 0x9060,                                                                                                                   // Символ: 188 0xbc lenght: 5 offset: 1449
0xb198, 0x2fff, 0xb198,                                                                                                                                   // Символ: 189 0xbd lenght: 3 offset: 1454
0x9198, 0x2f9f, 0x2000, 0x2f9f, 0x9198,                                                                                                                   // Символ: 190 0xbe lenght: 5 offset: 1457
0x6000, 0x13f8, 0x1404, 0x19f2, 0x2b1a, 0x2b02, 0x2b1a, 0x19f2, 0x1404, 0x13f8, 0x6000,                                                                   // Символ: 191 0xbf lenght: 11 offset: 1462
//  Кириллица, строчные
0x8000, 0x1c7c, 0x4cc6, 0x1fc6, 0x4cc6, 0x1c7c, 0x5000,                                                                                                   // Символ: 192 0xc0 lenght: 7 offset: 1473
0x8000, 0x13f8, 0x100c, 0x2006, 0x11fe, 0x1306, 0x3606, 0x1306, 0x11fe, 0x5000,                                                                           // Символ: 193 0xc1 lenght: 10 offset: 1480
0x4000, 0x11fc, 0x1300, 0x3600, 0x17f8, 0x160c, 0x6606, 0x160c, 0x17f8, 0x5000,                                                                           // Символ: 194 0xc2 lenght: 10 offset: 1490
0x8000, 0x9606, 0x1306, 0x11ff, 0x3003, 0x2000,                                                                                                           // Символ: 195 0xc3 lenght: 6 offset: 1500
0x8000, 0x11fe, 0x1306, 0x7606, 0x130e, 0x11fe, 0x2006, 0x100c, 0x13f8, 0x1000,                                                                           // Символ: 196 0xc4 lenght: 10 offset: 1506
0x8000, 0x11f8, 0x130c, 0x3606, 0x17fe, 0x3600, 0x1306, 0x11fc, 0x5000,                                                                                   // Символ: 197 0xc5 lenght: 9 offset: 1516
0x6000, 0x2060, 0x11f8, 0x136c, 0x7666, 0x136c, 0x11f8, 0x2060, 0x3000,                                                                                   // Символ: 198 0xc6 lenght: 9 offset: 1525
0x8000, 0x17fe, 0xa600, 0x5000,                                                                                                                           // Символ: 199 0xc7 lenght: 4 offset: 1534
0x8000, 0x2606, 0x130c, 0x1198, 0x10f0, 0x1060, 0x10f0, 0x1198, 0x130c, 0x2606, 0x5000,                                                                   // Символ: 200 0xc8 lenght: 11 offset: 1538
0x8000, 0x9606, 0x1306, 0x11fe, 0x5000,                                                                                                                   // Символ: 201 0xc9 lenght: 5 offset: 1549
0x4000, 0x230c, 0x11f8, 0x1000, 0x9606, 0x1306, 0x11fe, 0x5000,                                                                                           // Символ: 202 0xca lenght: 8 offset: 1554
0x8000, 0x1306, 0x130c, 0x1318, 0x1330, 0x1360, 0x13c0, 0x1360, 0x1330, 0x1318, 0x130c, 0x1306, 0x5000,                                                   // Символ: 203 0xcb lenght: 13 offset: 1562
0x8000, 0x10fe, 0x1186, 0x8306, 0x1606, 0x5000,                                                                                                           // Символ: 204 0xcc lenght: 6 offset: 1575
0x8000, 0x1402, 0x1606, 0x170e, 0x179e, 0x16f6, 0x1666, 0x5606, 0x5000,                                                                                   // Символ: 205 0xcd lenght: 9 offset: 1581
0x8000, 0x5606, 0x17fe, 0x5606, 0x5000,                                                                                                                   // Символ: 206 0xce lenght: 5 offset: 1590
0x8000, 0x11f8, 0x130c, 0x7606, 0x130c, 0x11f8, 0x5000,                                                                                                   // Символ: 207 0xcf lenght: 7 offset: 1595
0x8000, 0x17fe, 0xa606, 0x5000,                                                                                                                           // Символ: 208 0xd0 lenght: 4 offset: 1602
0x8000, 0x13fe, 0x4606, 0x13fe, 0x1036, 0x1066, 0x10c6, 0x1186, 0x1306, 0x5000,                                                                           // Символ: 209 0xd1 lenght: 10 offset: 1606
0x8000, 0x17f8, 0x160c, 0x7606, 0x160c, 0x17f8, 0x4600, 0x1000,                                                                                           // Символ: 210 0xd2 lenght: 8 offset: 1616
0x8000, 0x11f8, 0x130c, 0x1606, 0x5600, 0x1606, 0x130c, 0x11f8, 0x5000,                                                                                   // Символ: 211 0xd3 lenght: 9 offset: 1624
0x8000, 0x17fe, 0xa060, 0x5000,                                                                                                                           // Символ: 212 0xd4 lenght: 4 offset: 1633
0x8000, 0x9606, 0x130e, 0x11fe, 0x2006, 0x100c, 0x13f8, 0x1000,                                                                                           // Символ: 213 0xd5 lenght: 8 offset: 1637
0x8000, 0x3666, 0x136c, 0x11f8, 0x10f0, 0x11f8, 0x136c, 0x3666, 0x5000,                                                                                   // Символ: 214 0xd6 lenght: 9 offset: 1645
0x4000, 0x13f0, 0x1618, 0x360c, 0x1618, 0x17f8, 0x160c, 0x5606, 0x160c, 0x17f8, 0x5000,                                                                   // Символ: 215 0xd7 lenght: 11 offset: 1654
0x8000, 0x3600, 0x17f0, 0x1618, 0x460c, 0x1618, 0x17f0, 0x5000,                                                                                           // Символ: 216 0xd8 lenght: 8 offset: 1665
0x8000, 0x3606, 0x17c6, 0x1666, 0x4636, 0x1666, 0x17c6, 0x5000,                                                                                           // Символ: 217 0xd9 lenght: 8 offset: 1673
0x8000, 0x13fc, 0x1606, 0x3006, 0x10fc, 0x3006, 0x1606, 0x13fc, 0x5000,                                                                                   // Символ: 218 0xda lenght: 9 offset: 1681
0x8000, 0x9666, 0x1366, 0x11fe, 0x5000,                                                                                                                   // Символ: 219 0xdb lenght: 5 offset: 1690
0x8000, 0x11f8, 0x130c, 0x1606, 0x2006, 0x10fe, 0x2006, 0x1606, 0x130c, 0x11f8, 0x5000,                                                                   // Символ: 220 0xdc lenght: 11 offset: 1695
0x8000, 0x9666, 0x1366, 0x11ff, 0x3003, 0x2000,                                                                                                           // Символ: 221 0xdd lenght: 6 offset: 1706
0x8000, 0x4606, 0x1306, 0x11fe, 0x5006, 0x5000,                                                                                                           // Символ: 222 0xde lenght: 6 offset: 1712
0x8000, 0x2700, 0x1300, 0x13f8, 0x130c, 0x4306, 0x130c, 0x13f8, 0x5000,                                                                                   // Символ: 223 0xdf lenght: 9 offset: 1718
//  Кириллица, прописные
0x4000, 0x1c7c, 0x6cc6, 0x1fc6, 0x6cc6, 0x1c7c, 0x5000,                                                                                                   // Символ: 224 0xe0 lenght: 7 offset: 1727
0x4000, 0x11f8, 0x130c, 0x6606, 0x17fe, 0x6606, 0x5000,                                                                                                   // Символ: 225 0xe1 lenght: 7 offset: 1734
0x4000, 0x17fc, 0x5600, 0x17f8, 0x160c, 0x5606, 0x160c, 0x17f8, 0x5000,                                                                                   // Символ: 226 0xe2 lenght: 9 offset: 1741
0x4000, 0xd606, 0x1306, 0x11ff, 0x3003, 0x2000,                                                                                                           // Символ: 227 0xe3 lenght: 6 offset: 1750
0x4000, 0x10fc, 0x118c, 0xb30c, 0x170c, 0x1ffe, 0x3c06, 0x2000,                                                                                           // Символ: 228 0xe4 lenght: 8 offset: 1756
0x4000, 0x17fe, 0x6600, 0x17f8, 0x6600, 0x17fe, 0x5000,                                                                                                   // Символ: 229 0xe5 lenght: 7 offset: 1764
0x3000, 0x1060, 0x11f8, 0x136c, 0xb666, 0x136c, 0x11f8, 0x1060, 0x4000,                                                                                   // Символ: 230 0xe6 lenght: 9 offset: 1771
0x4000, 0x17fe, 0xe600, 0x5000,                                                                                                                           // Символ: 231 0xe7 lenght: 4 offset: 1780
0x4000, 0x2606, 0x230c, 0x2198, 0x10f0, 0x1060, 0x10f0, 0x2198, 0x230c, 0x2606, 0x5000,                                                                   // Символ: 232 0xe8 lenght: 11 offset: 1784
0x4000, 0x4606, 0x160e, 0x161e, 0x1636, 0x1666, 0x16c6, 0x1786, 0x1706, 0x4606, 0x5000,                                                                   // Символ: 233 0xe9 lenght: 11 offset: 1795
0x230c, 0x11f8, 0x1000, 0x4606, 0x160e, 0x161e, 0x1636, 0x1666, 0x16c6, 0x1786, 0x1706, 0x4606, 0x5000,                                                   // Символ: 234 0xea lenght: 13 offset: 1806
0x4000, 0x1606, 0x160c, 0x1618, 0x1630, 0x1660, 0x16c0, 0x1780, 0x1700, 0x1780, 0x16c0, 0x1660, 0x1630, 0x1618, 0x160c, 0x1606, 0x5000,                   // Символ: 235 0xeb lenght: 17 offset: 1819
0x4000, 0x10fe, 0x1186, 0xb306, 0x1606, 0x1c06, 0x5000,                                                                                                   // Символ: 236 0xec lenght: 7 offset: 1836
0x4000, 0x1802, 0x1c06, 0x1e0e, 0x1f1e, 0x1db6, 0x1ce6, 0x1c46, 0x8c06, 0x5000,                                                                           // Символ: 237 0xed lenght: 10 offset: 1843
0x4000, 0x7606, 0x17fe, 0x7606, 0x5000,                                                                                                                   // Символ: 238 0xee lenght: 5 offset: 1853
0x4000, 0x11f8, 0x130c, 0xb606, 0x130c, 0x11f8, 0x5000,                                                                                                   // Символ: 239 0xef lenght: 7 offset: 1858
0x4000, 0x17fe, 0xe606, 0x5000,                                                                                                                           // Символ: 240 0xf0 lenght: 4 offset: 1865
0x4000, 0x11fe, 0x1306, 0x4606, 0x1306, 0x11fe, 0x101e, 0x1036, 0x1066, 0x10c6, 0x1186, 0x1306, 0x1606, 0x5000,                                           // Символ: 241 0xf1 lenght: 14 offset: 1869
0x4000, 0x17f8, 0x160c, 0x4606, 0x160c, 0x17f8, 0x7600, 0x5000,                                                                                           // Символ: 242 0xf2 lenght: 8 offset: 1883
0x4000, 0x11f8, 0x130c, 0x2606, 0x7600, 0x2606, 0x130c, 0x11f8, 0x5000,                                                                                   // Символ: 243 0xf3 lenght: 9 offset: 1891
0x4000, 0x17fe, 0xe060, 0x5000,                                                                                                                           // Символ: 244 0xf4 lenght: 4 offset: 1900
0x4000, 0x7606, 0x1306, 0x11fe, 0x4006, 0x100c, 0x13f8, 0x5000,                                                                                           // Символ: 245 0xf5 lenght: 8 offset: 1904
0x4000, 0x5666, 0x136c, 0x11f8, 0x10f0, 0x11f8, 0x136c, 0x5666, 0x5000,                                                                                   // Символ: 246 0xf6 lenght: 9 offset: 1912
0x4000, 0x17f8, 0x160c, 0x3606, 0x160c, 0x17f8, 0x160c, 0x5606, 0x160c, 0x17f8, 0x5000,                                                                   // Символ: 247 0xf7 lenght: 11 offset: 1921
0x4000, 0x5600, 0x17f8, 0x160c, 0x6606, 0x160c, 0x17f8, 0x5000,                                                                                           // Символ: 248 0xf8 lenght: 8 offset: 1932
0x4000, 0x5c06, 0x1fc6, 0x1c66, 0x6c36, 0x1c66, 0x1fc6, 0x5000,                                                                                           // Символ: 249 0xf9 lenght: 8 offset: 1940
0x4000, 0x11f8, 0x130c, 0x1606, 0x3006, 0x100c, 0x10f8, 0x100c, 0x3006, 0x1606, 0x130c, 0x11f8, 0x5000,                                                   // Символ: 250 0xfa lenght: 13 offset: 1948
0x4000, 0xd666, 0x1366, 0x11fe, 0x5000,                                                                                                                   // Символ: 251 0xfb lenght: 5 offset: 1961
0x4000, 0x11f8, 0x130c, 0x1606, 0x4006, 0x10fe, 0x4006, 0x1606, 0x130c, 0x11f8, 0x5000,                                                                   // Символ: 252 0xfc lenght: 11 offset: 1966
0x4000, 0xd666, 0x1366, 0x11ff, 0x3003, 0x2000,                                                                                                           // Символ: 253 0xfd lenght: 6 offset: 1977
0x4000, 0x6606, 0x1306, 0x11fe, 0x7006, 0x5000,                                                                                                           // Символ: 254 0xfe lenght: 6 offset: 1983
0x4000, 0x2e00, 0x3600, 0x17f8, 0x160c, 0x6606, 0x160c, 0x17f8, 0x5000,                                                                                   // Символ: 255 0xff lenght: 9 offset: 1989
};

// offset table
static const uint16_t TERMINUS_K12x24B_ID[256]= {
0x0, 0x5, 0x11, 0x1d, 0x27, 0x34, 0x3f, 0x49, 0x4e, 0x53, 0x5a, 0x61, 0x6e, 0x78, 0x80, 0x88, 
0x97, 0xa4, 0xb1, 0xbe, 0xc3, 0xc9, 0xd6, 0xd9, 0xe7, 0xef, 0xf7, 0x102, 0x10d, 0x111, 0x11c, 0x124, 
0x12c, 0x12e, 0x133, 0x137, 0x13e, 0x14d, 0x15a, 0x168, 0x16c, 0x175, 0x17e, 0x189, 0x18e, 0x192, 0x195, 0x198, 
0x1a1, 0x1b0, 0x1b8, 0x1c7, 0x1d4, 0x1e1, 0x1eb, 0x1f5, 0x1fe, 0x209, 0x213, 0x218, 0x21e, 0x22f, 0x234, 0x245, 
0x250, 0x25d, 0x264, 0x26f, 0x278, 0x27f, 0x286, 0x28c, 0x296, 0x29b, 0x2a0, 0x2a7, 0x2b8, 0x2bc, 0x2c6, 0x2d1, 
0x2d8, 0x2e0, 0x2ea, 0x2f8, 0x305, 0x309, 0x30e, 0x315, 0x31f, 0x32a, 0x331, 0x33e, 0x343, 0x34c, 0x351, 0x359, 
0x35d, 0x363, 0x36d, 0x375, 0x37e, 0x386, 0x38f, 0x395, 0x39f, 0x3a5, 0x3ac, 0x3b5, 0x3c3, 0x3c8, 0x3cd, 0x3d2, 
0x3d9, 0x3e1, 0x3e9, 0x3f0, 0x3f9, 0x3ff, 0x404, 0x40b, 0x410, 0x41b, 0x423, 0x430, 0x439, 0x43c, 0x445, 0x44b, 
0x453, 0x456, 0x458, 0x45b, 0x45e, 0x461, 0x464, 0x467, 0x46a, 0x46d, 0x470, 0x473, 0x475, 0x477, 0x479, 0x47b, 
0x47d, 0x495, 0x4ad, 0x4c5, 0x4ca, 0x4cd, 0x4d2, 0x4dd, 0x4e6, 0x4f7, 0x508, 0x50a, 0x50e, 0x513, 0x51d, 0x520, 
0x527, 0x52c, 0x52e, 0x533, 0x53e, 0x541, 0x546, 0x54b, 0x54e, 0x553, 0x558, 0x55b, 0x560, 0x565, 0x568, 0x56d, 
0x572, 0x575, 0x57a, 0x57f, 0x587, 0x58a, 0x58f, 0x594, 0x597, 0x59c, 0x5a1, 0x5a4, 0x5a9, 0x5ae, 0x5b1, 0x5b6, 
0x5c1, 0x5c8, 0x5d2, 0x5dc, 0x5e2, 0x5ec, 0x5f5, 0x5fe, 0x602, 0x60d, 0x612, 0x61a, 0x627, 0x62d, 0x636, 0x63b, 
0x642, 0x646, 0x650, 0x658, 0x661, 0x665, 0x66d, 0x676, 0x681, 0x689, 0x691, 0x69a, 0x69f, 0x6aa, 0x6b0, 0x6b6, 
0x6bf, 0x6c6, 0x6cd, 0x6d6, 0x6dc, 0x6e4, 0x6eb, 0x6f4, 0x6f8, 0x703, 0x70e, 0x71b, 0x72c, 0x733, 0x73d, 0x742, 
0x749, 0x74d, 0x75b, 0x763, 0x76c, 0x770, 0x778, 0x781, 0x78c, 0x794, 0x79c, 0x7a9, 0x7ae, 0x7b9, 0x7bf, 0x7c5, 
};


#endif  //  __TERMINUS_K12x24B_H__
