#include <stdint.h>
#include "main.h"
#include "st7735.h"
#include "stm32f10x_spi.h"
#include "stm32f10x_gpio.h"

static __IO uint32_t s_timer;
//#define chip_select_disable() gpio_set(GPIOA,GPIO_Pin_4)

uint8_t uart_ready;
uint8_t uart_index;
char uart_buf[UART_BUFFER_LEN];
//int enc_prev_value=0;
uint8_t tim4_counter=TIM4_INIT_VALUE;                // 3 sec

extern volatile bool is_done_DMA;

void DMA1_Channel3_IRQHandler(void) {

    DMA1->IFCR |= DMA_IFCR_CGIF3;               // Clear all interrupt flags
    DMA1_Channel3->CCR &= ~(DMA_CCR1_EN);       // Disable DMA1

    chip_select_disable();

    SPI1->CR1 &= CR1_SPE_Reset;                 // Disable SPI for setup
    SPI1->CR2 &= ~SPI_CR2_TXDMAEN;              // Disable DMA request
    SPI1->CR1 = (SPI_Mode_Master|SPI_DataSize_8b|SPI_NSS_Soft|SPI_BaudRatePrescaler_2); // toggel to 8-bit SPI mode
    SPI1->CR1 |= SPI_CR1_SPE;
    SPI1->CR1 |= CR1_SPE_Set;                   // Enable SPI after setup

    is_done_DMA=true;
}

void  TIM4_IRQHandler(void){
    if (tim4_counter)
        --tim4_counter;

    TIM4->SR &= ~(0x01);
}

void USART1_IRQHandler(void)
{
    if ((USART1->SR & USART_SR_ORE) || uart_ready) {
        USART1->DR;
        uart_index=0;
        return;
    }

    uint8_t ch=USART1->DR;
    if (ch == 0xa || ch == 0xd) {
        uart_ready=1;
        uart_buf[uart_index]=0;
    } else {
        if (uart_index < (UART_BUFFER_LEN-1))
            uart_buf[uart_index++]=ch;
        else{
            uart_ready=1;
            uart_buf[UART_BUFFER_LEN-1]=0;
        }
    }
}

void SysTick_Handler(void)
{
        if (s_timer)
            s_timer--;
}

void systick_delay(__IO uint32_t val) {
    s_timer=val;
    SysTick->LOAD |= SysTick_CTRL_ENABLE;
    while(s_timer) {
         __asm__ volatile ("wfi\n");
    };
    SysTick->LOAD &= ~(SysTick_CTRL_ENABLE_Msk);    // disable SysTick
}

