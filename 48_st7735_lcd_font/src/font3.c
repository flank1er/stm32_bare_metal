#include "font3.h"
#include "ds_digits.h"
#include "pixel.h"
#include "st7735.h"
//#include "uart.h"

#define CHAR_LEN 768                 // 768=(24 x 32)  i.e. width * height
#define numlen 5
void font3_decompress_sym(uint8_t sym, uint8_t font);
void font3_char_to_buf(uint8_t x_pos, uint8_t ch, uint8_t len, uint8_t font, uint16_t fg_color, uint16_t bg_color, uint16_t* buffer);
void font3_char_to_buf_2X(uint8_t x_pos, uint8_t ch, uint8_t len, uint8_t font, uint16_t fg_color, uint16_t bg_color, uint16_t* buffer);
static volatile uint16_t tab_buf[32];
static volatile uint8_t sym_buf[32*3];
extern uint16_t buf[];

void font3_print_num(uint32_t num, uint8_t x, uint8_t y, uint16_t fg, uint16_t bg, uint8_t font, uint8_t scale){
    uint8_t n[numlen];
    uint8_t *s=n+(numlen-1);
    *s=0;           // EOL
    do {
        *(--s)=('0' + num%10);
        num = num/10;
    } while (num>0);
    font3_print_str_from_char((char*)s,x,y,fg,bg,font, scale);
}

void font3_print_str_from_char(char *str, uint8_t x, uint8_t y, uint16_t fg_color, uint16_t bg_color,  uint8_t font, uint8_t scale) {
    uint8_t i=0;
    while (*str) {
        font3_send_char(x + scale*i*DS_DIGITS_22x32_CHAR_WIDTH,y,*str++, font, fg_color,bg_color, scale);
        ++i;
    }
}

void font3_send_char(uint8_t x, uint8_t y, uint8_t ch, uint8_t font, uint16_t fg_color, uint16_t bg_color, uint8_t scale) {

    if (scale == 1)
        font3_char_to_buf(0,ch, 1, font, fg_color,bg_color,buf);
    else
        font3_char_to_buf_2X(0,ch, 1, font, fg_color,bg_color,buf);

    chip_select_enable();
    // 16x32 area
    st7735_send(LCD_C,ST77XX_CASET);
    st7735_send(LCD_D,0);
    st7735_send(LCD_D,x);
    st7735_send(LCD_D,0);
    st7735_send(LCD_D,x+((DS_DIGITS_22x32_CHAR_WIDTH*scale)-1));

    st7735_send(LCD_C,ST77XX_RASET);
    st7735_send(LCD_D,0);
    st7735_send(LCD_D,y);
    st7735_send(LCD_D,0);
    st7735_send(LCD_D,y+((DS_DIGITS_22x32_CHAR_HEIGHT*scale)-1));

    st7735_send(LCD_C,ST77XX_RAMWR);
#ifdef HW_SPI

    gpio_set(GPIOB,DC);
    SPI1->CR1 &= CR1_SPE_Reset;      // disable SPI for setup
    SPI1->CR1  = (SPI_Mode_Master|SPI_DataSize_16b|SPI_NSS_Soft|SPI_BaudRatePrescaler_2);
    SPI1->CR1 |= CR1_SPE_Set;        // enable SPI
    for (uint16_t j=0;j<(CHAR_LEN*scale *scale);j++) {
        uint16_t color;

        color=buf[j];
        while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
        SPI1->DR=color;
        //SPI1->DR=fg_color;
    }

    while (!(SPI1->SR & SPI_I2S_FLAG_TXE) || (SPI1->SR & SPI_I2S_FLAG_BSY));
    chip_select_disable();

    SPI1->CR1 &= CR1_SPE_Reset;      // disable SPI for setup
    SPI1->CR1  = (SPI_Mode_Master|SPI_DataSize_8b|SPI_NSS_Soft|SPI_BaudRatePrescaler_2);
    SPI1->CR1 |= CR1_SPE_Set;        // enable SPI
#else
   chip_select_disable();
#endif

}



void font3_char_to_buf_2X(uint8_t x_pos, uint8_t ch, uint8_t len, uint8_t font, uint16_t fg_color, uint16_t bg_color, uint16_t* buffer) {
    if ((x_pos + 48) > LCD_X)
        return;

    font3_decompress_sym(ch,font);

    for (uint8_t i=0; i < 32; i++) {
        uint16_t index= x_pos + (i * len * DS_DIGITS_22x32_CHAR_WIDTH * 4);

        for (uint8_t k=0; k < 3; k++) {
            uint8_t data=sym_buf[i*3+k];


            for (uint8_t j=0; j<8;j++) {
                uint16_t color = (data & 0x80) ? fg_color : bg_color;
                buffer[index+(DS_DIGITS_22x32_CHAR_WIDTH *2)]=color;
                buffer[index+(DS_DIGITS_22x32_CHAR_WIDTH*2+1)]=color;
                buffer[index++]=color;
                buffer[index++]=color;
                data = data << 1;
            }
        }
    }
}

void font3_char_to_buf(uint8_t x_pos, uint8_t ch, uint8_t len, uint8_t font, uint16_t fg_color, uint16_t bg_color, uint16_t* buffer) {
    if ((x_pos + 24) > LCD_X)
        return;

    font3_decompress_sym(ch,font);


    for (uint8_t i=0; i < 32; i++) {
        uint16_t index= x_pos + (i*len* DS_DIGITS_22x32_CHAR_WIDTH);

        for (uint8_t k=0; k < 3; k++) {
            uint8_t data=sym_buf[i*3+k];


            for (uint8_t j=0; j<8;j++) {

                buffer[index++]=(data & 0x80) ? fg_color : bg_color;
                data = data << 1;
            }
        }
    }
}

void font3_decompress_sym(uint8_t sym,  uint8_t font) {
    sym -= 0x20;
    if (sym > 0x64)
        return;


    uint16_t idx =  (!font) ? DS_DIGITS_22x32_ID[sym] : PIXEL_LCD_ID[sym] ;

    uint8_t i=0;
    while (i<32) {
        uint8_t index = (!font) ? DS_DIGITS_22x32[idx++] : PIXEL_LCD_23x32[idx++];

        uint8_t k = (index >> 4);
        if (k == 0xf) {
            k = (index & 0x0f);
            for (uint8_t j=0; j<k; j++) {
                tab_buf[i++]= (!font) ? DS_DIGITS_22x32[idx++] : PIXEL_LCD_23x32[idx++];
            }

        } else if (k == 0x0 ) {
            k=(index & 0x0f);
            index =(!font) ? DS_DIGITS_22x32[idx++] : PIXEL_LCD_23x32[idx++];
            for (uint8_t j=0; j<k; j++) {
                tab_buf[i++]= index;
            }
        } else {
            index = (index & 0x0f);
            for (uint8_t j=0; j<k; j++) {
                tab_buf[i++]= index;
            }

        }
    }

    for (i=0;i<32;i++) {
        uint16_t j=tab_buf[i];
        if (!font) {
            if (j < DS_DIGITS_22x32_WORDS_LENGTH) {
                j *=3;
                sym_buf[i*3]=DS_DIGITS_DICTONARY[j];
                sym_buf[i*3+1]=DS_DIGITS_DICTONARY[j+1];
                sym_buf[i*3+2]=DS_DIGITS_DICTONARY[j+2];
            }
        } else {
            if (j < PIXEL_LCD_23x32_WORDS_LENGTH) {
                j *=3;
                sym_buf[i*3]=PIXEL_LCD_DICTONARY[j];
                sym_buf[i*3+1]=PIXEL_LCD_DICTONARY[j+1];
                sym_buf[i*3+2]=PIXEL_LCD_DICTONARY[j+2];
            }
        }
    }
}
