#include "i2c.h"

void ds3231_write_register(uint8_t reg, uint8_t value) {
    if (init_i2c(DS3231_I2C_ADDR, reg, NOLAST) == 0) {
        I2C1->DR=value;
        while(!(I2C1->SR1 & I2C_FLAG_BTF));     // wait BFT
        I2C1->CR1 |= I2C_CR1_STOP_Set;          // Program the STOP bit
        while (I2C1->CR1 & I2C_CR1_STOP_Set);   // Wait until STOP bit is cleared by hardware
    }  else
        stop_i2c;
}

uint8_t ds3231_read_register(uint8_t reg) {
    uint8_t ret=0;
    if (init_i2c(DS3231_I2C_ADDR, reg, LAST) == 0) {
        ret=read_byte(DS3231_I2C_ADDR|0x01);
    }  else
        stop_i2c;
    return ret;
}

void i2c_read(uint8_t adr, uint8_t count,uint8_t* data){
    uint8_t ret;
    I2C1->CR1 |= I2C_CR1_START_Set;         // START, =0x0100
    while (!(I2C1->SR1 & I2C_FLAG_SB));     // wait SB, while(!(I2C1->SR1 & 0x0001));
    (void) I2C1->SR1;
    I2C1->DR=adr;
    while (!(I2C1->SR1 & I2C_FLAG_ADDR));   // wait ADDR, while(!(I2C1->SR1 & 0x0002));
    I2C1->CR1 |= I2C_CR1_ACK_Set;           // set ACK, 0x0400
    (void) I2C1->SR1;                       // reset ADDR
    (void) I2C1->SR2;                       // reset ADDR

    for(uint8_t i=1;i<=count;i++, data++) {
        if (i<count) {
           while(!(I2C1->SR1 & I2C_IT_RXNE));      // wait RxNE, while(!(I2C1->SR1 & 0x0040));
            *data=I2C1->DR;
        } else {
            I2C1->CR1 &= I2C_CR1_ACK_Reset;         // set NACK, =0xFBFF
            I2C1->CR1 |= I2C_CR1_STOP_Set;          // set STOP, =0x0200
            while(!(I2C1->SR1 & I2C_IT_RXNE));      // wait RxNE, while(!(I2C1->SR1 & 0x0040));
            *data=I2C1->DR;
            while (I2C1->CR1 & I2C_CR1_STOP_Set);   // white STOP, =0x0200
        }
    }

    I2C1->CR1 |= I2C_CR1_ACK_Set;           // 1.ACK, =0x0400;
    return;

}

void i2c_write(uint8_t adr, uint8_t reg, uint8_t count, uint8_t* data) {
    // "count" param MUST BE more than 0!
    if (init_i2c(adr, reg, NOLAST) == 0) {
        for(uint8_t i=1;i<=count;i++,data++) {
            I2C1->DR=*data;
            if (i == count) {
                while(!(I2C1->SR1 & I2C_FLAG_BTF));     // wait BFT
                I2C1->CR1 |= I2C_CR1_STOP_Set;          // Program the STOP bit
                while (I2C1->CR1 & I2C_CR1_STOP_Set);   // Wait until STOP bit is cleared by hardware
            } else
                while(!(I2C1->SR1 & I2C_FLAG_TXE));     // wait BFT
        }
    }  else
        stop_i2c;
}

uint16_t read_two_byte(uint8_t adr){
    uint16_t ret=0;
    uint8_t vl;
    I2C1->CR1 |= I2C_CR1_START_Set;         // START, =0x0100
    while (!(I2C1->SR1 & I2C_FLAG_SB));     // wait SB, while(!(I2C1->SR1 & 0x0001));
    (void) I2C1->SR1;
    I2C1->DR=adr;
    while (!(I2C1->SR1 & I2C_FLAG_ADDR));    // wait ADDR, while(!(I2C1->SR1 & 0x0002));
    I2C1->CR1 |=I2C_PECPosition_Next;       // POS=1
    __disable_irq();
    (void) I2C1->SR1;                       // reset ADDR
    (void) I2C1->SR2;                       // reset ADDR
    I2C1->CR1 &= I2C_CR1_ACK_Reset;         // NACK, =0xFBFF
    __enable_irq();
    while(!(I2C1->SR1 & I2C_FLAG_BTF));     // wait  BFT
    __disable_irq();
    I2C1->CR1 |= I2C_CR1_STOP_Set; ;        // STOP, =0x0200
    vl=I2C1->DR;
    ret|=(uint16_t)vl;
    __enable_irq();
//    while(!(I2C1->SR1 & I2C_IT_RXNE));        // wait RxNE, while(!(I2C1->SR1 & 0x0040));
    vl=I2C1->DR;
    ret|=(uint16_t)(vl<<8);
    while (I2C1->CR1 & I2C_CR1_STOP_Set);
    I2C1->CR1 &= ~(I2C_PECPosition_Next);   // POS=0
    I2C1->CR1 |= I2C_CR1_ACK_Set;           // ACK=1

    return ret;
}

uint8_t read_byte(uint8_t adr){
    uint8_t ret;
    I2C1->CR1 |= I2C_CR1_START_Set;         // START, =0x0100
    while (!(I2C1->SR1 & I2C_FLAG_SB));     // wait SB, while(!(I2C1->SR1 & 0x0001));
    (void) I2C1->SR1;						// clear SB
    I2C1->DR=adr;
    while (!(I2C1->SR1 & I2C_FLAG_ADDR));   // wait ADDR, while(!(I2C1->SR1 & 0x0002));
    I2C1->CR1 &= I2C_CR1_ACK_Reset;         // 1.NACK, =0xFBFF
    __disable_irq();
    (void) I2C1->SR1;                       // 2.reset ADDR
    (void) I2C1->SR2;                       // 2.reset ADDR
    I2C1->CR1 |= I2C_CR1_STOP_Set;          // 3.STOP, =0x0200
    __enable_irq();
    while(!(I2C1->SR1 & I2C_IT_RXNE));      // wait RxNE, while(!(I2C1->SR1 & 0x0040));
    ret=I2C1->DR;
    while (I2C1->CR1 & I2C_CR1_STOP_Set);   // wait clear STOP

    I2C1->CR1 |= I2C_CR1_ACK_Set;           // ACK for next byte
    return ret;

}

uint8_t init_i2c(uint8_t adr, uint8_t value, uint8_t last) {
    I2C1->CR1 |= I2C_CR1_START_Set;         	//=0x0100, START
    while (!(I2C1->SR1 & I2C_FLAG_SB));     	// wait SB
    (void) I2C1->SR1;
    I2C1->DR=adr;								// send ADDR
    while (!(I2C1->SR1 & I2C_FLAG_ADDR))    	// wait ADDR
    {
        if(I2C1->SR1 & I2C_IT_AF)				// if NACK
            return 1;
    }
    (void) I2C1->SR1;                       	// clear ADDR
    (void) I2C1->SR2;                       	// clear ADDR
    I2C1->DR=value;
    if (last == LAST) {
        while(!(I2C1->SR1 & I2C_FLAG_BTF));     // wait BFT
        I2C1->CR1 |= I2C_CR1_STOP_Set;          // Program the STOP bit
        while (I2C1->CR1 & I2C_CR1_STOP_Set);   // Wait until STOP bit is cleared by hardware
    } else {
        while(!(I2C1->SR1 & I2C_FLAG_TXE));     // wait TXE bit
    }
    return 0;
}


