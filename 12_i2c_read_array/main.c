#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "uart.h"
#include "task.h"
#include "led.h"
#include "spi.h"
#include "i2c.h"

extern void delay(uint32_t ms);
void toggle_led();

int main()
{
    // enable GPIOC port
    RCC->APB2ENR |= RCC_APB2Periph_GPIOA;			// enable PORT_A
    RCC->APB2ENR |= RCC_APB2Periph_GPIOB;			// enable PORT_B
    RCC->APB2ENR |= RCC_APB2Periph_GPIOC;			// enable PORT_C
	RCC->APB2ENR |= RCC_APB2Periph_USART1; 			// enable UART1
	RCC->APB2ENR |= RCC_APB2Periph_SPI1; 			// enable SPI1
	RCC->APB1ENR |= RCC_APB1Periph_I2C1; 			// enable I2C1
    // --- GPIO setup ----
    GPIOC->CRH &= ~(uint32_t)(0xf<<20);
    GPIOC->CRH |=  (uint32_t)(0x2<<20);
	// Setup PA9
	GPIOA->CRH &= ~(uint32_t)(0xf<<4);
	GPIOA->CRH |=  (uint32_t)(0xa<<4);
	// Setup PA4, PA5, PA7
	GPIOA->CRL &= ~(uint32_t)(0xf<<16);				// clear mode for PA4
	GPIOA->CRL &= ~(uint32_t)(0xf<<20);				// clear mode for PA5
	GPIOA->CRL &= ~(uint32_t)(0xf<<28);				// clear mode for PA7
	GPIOA->CRL |= (uint32_t)(0x3<<16);				// for PA4 set  PushPull mode, 50MHz
	GPIOA->CRL |= (uint32_t)(0xb<<20);				// for PA5 set  Alternative mode, 50MHz
	GPIOA->CRL |= (uint32_t)(0xb<<28);				// for PA7 set  Alternative mode, 50MHz
	// Setup PB6, PB7
	GPIOB->CRL &= ~(uint32_t)(0xf<<24);				// clear mode for PB6
	GPIOB->CRL &= ~(uint32_t)(0xf<<28);				// clear mode for PB7
	GPIOB->CRL |= (uint32_t)(0xe<<24);				// for PB6 set  Alternative mode, 2MHz, OpenDrain
	GPIOB->CRL |= (uint32_t)(0xe<<28);				// for PB7 set  Alternative mode, 2MHz, OpenDrain

	// --- UART setup ----
    //USART1->BRR  = 0x1d4c;								// 9600 Baud, when APB2=72MHz
	USART1->BRR = 0x271;								// 115200 Baud, when APB2=72MHz
	USART1->CR1 |= USART_CR1_UE_Set | USART_Mode_Tx;	// enable USART1, enable  TX mode
	// --- SPI1 setup ----
	SPI1->CR1 = CR1_SPE_Set|SPI_Mode_Master|SPI_DataSize_16b|SPI_NSS_Soft|SPI_BaudRatePrescaler_4;;
	// --- I2C setup ----
	disable_i2c;
	I2C1->CR2 &= I2C_CR2_FREQ_Reset;	//=0xffc0
	I2C1->CR2 |= 36;					// set FREQ = APB1= 36MHz
	I2C1->CCR = 180;					// 100 KHz
	I2C1->TRISE = 37;
    enable_i2c;

	// Let's go..
	reg=0; led=0;
	clear_task();
	add_task(show_led,TOP,LOOP, 5);
	add_task(toggle_led,TOP,LOOP,1000);
    if (SysTick_Config(72000)) // set 1ms
    {
        while(1); // error
    }
	__enable_irq();

    uint8_t cal[7];
    for(;;){
        delay(1000);
        usart1_print_string("Control: ");
        usart1_print_hex(ds3231_read_register(DS3231_CONTROL_REG));
        usart1_print_string(" Status: ");
        usart1_print_hex(ds3231_read_register(DS3231_STATUS_REG));
        if (init_i2c(DS3231_I2C_ADDR, 0x0,LAST) == 0) {
            i2c_read(DS3231_I2C_ADDR|0x01, 7,cal);
            usart1_print_string(" Time: ");
            usart1_print_bcd(cal[2]);
            usart1_send_char(':');
            usart1_print_bcd(cal[1]);
            usart1_send_char(':');
            usart1_print_bcd(cal[0]);
            usart1_print_string(" Data: ");
            usart1_print_bcd(cal[3]);
            usart1_send_char(':');
            usart1_print_bcd(cal[4]);
            usart1_send_char(':');
            usart1_print_bcd(cal[5]);
            usart1_send_char(':');
            usart1_print_bcd(cal[6]);
            usart1_send_char('\n');
        }
		GPIOC->ODR ^= GPIO_Pin_13;
	}
}

void toggle_led() {
	led++;
//	usart1_print_number(get_load_cpu());
//	usart1_send_char('\n');
}
