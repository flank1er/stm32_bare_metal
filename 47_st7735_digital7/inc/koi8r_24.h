#ifndef __KOI8R_24_H__
#define __KOI8R_24_H__
#include "main.h"

#define REGULAR   0x0
#define BOLD      0x1
#define DIGITAL_7 0x2

#define STRING_CHAR 0x0
#define STRING_FULL 0x01
#define STRING_DMA  0x02

void KOI8Rx24_send_char(uint8_t x, uint8_t y, uint8_t ch,  uint8_t font, uint16_t fg_color, uint16_t bg_color);
void KOI8Rx24_print_str_from_char(char *str,  uint8_t x, uint8_t y, uint16_t fg_color, uint16_t bg_color, uint8_t font);
void KOI8Rx24_print_num(uint32_t num, uint8_t x, uint8_t y, uint16_t fg, uint16_t bg,  uint8_t font, uint8_t str);
void KOI8Rx24_print_full_str(char* str, uint8_t len, uint8_t font, uint8_t x, uint8_t y, uint16_t fg_color, uint16_t bg_color);
void KOI8Rx24_print_str_DMA(char* str, uint8_t len, uint8_t font, uint8_t x, uint8_t y, uint16_t fg_color, uint16_t bg_color);
#endif  // __KOI8R_24_H__
