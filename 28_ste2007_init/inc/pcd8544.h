#ifndef __PCD8544_H__
#define __PCD8544_H__
#include <stdint.h>
#include <stdbool.h>

/// alias
#define  pcd8544_clear() pcd8544_fill(0x0)

// functions
void pcd8544_init();
void pcd8544_fill_fb(uint8_t value);
void pcd8544_display_fb();
void pcd8544_off();

#endif      // __PCD8544_H__
