#include "cp866_16.h"
#include "st7735.h"
#include "uart.h"

#define numlen 10

volatile uint8_t sym_buf[(FONT_CP866_16_CHAR_WIDTH*FONT_CP866_16_CHAR_HEIGHT)];
extern uint16_t buf[];

void cp866x16_decompress_sym(uint8_t sym);
void cp866_16_char_to_buf(uint8_t x_pos, uint8_t ch, uint8_t len, uint16_t fg_color, uint16_t bg_color, uint16_t* buffer);


void cp866x16_print_num_from_char(uint32_t num, uint8_t x, uint8_t y, uint16_t fg, uint16_t bg){
    uint8_t n[numlen];
    uint8_t *s=n+(numlen-1);
    *s=0;           // EOL
    uint8_t l=0;
    do {
        *(--s)=('0' + num%10);
        num = num/10;
        ++l;
    } while (num>0);
    cp866x16_print_str_from_char((char*)s,x,y,fg,bg);
}


void cp866x16_print_str_from_char(char *str, uint8_t x, uint8_t y, uint16_t fg_color, uint16_t bg_color) {
    uint8_t i=0;
    while (*str) {
        cp866x16_send_char(x + i*8,y,*str++,fg_color,bg_color);
        ++i;
    }
}

void cp866x16_send_char(uint8_t x, uint8_t y, uint8_t ch, uint16_t fg_color, uint16_t bg_color) {

    cp866_16_char_to_buf(x,ch, 1, fg_color,bg_color,buf);

    chip_select_enable();
    // 8x8 area
    st7735_send(LCD_C,ST77XX_CASET);
    st7735_send(LCD_D,0);
    st7735_send(LCD_D,x);
    st7735_send(LCD_D,0);
    st7735_send(LCD_D,x+7);

    st7735_send(LCD_C,ST77XX_RASET);
    st7735_send(LCD_D,0);
    st7735_send(LCD_D,y);
    st7735_send(LCD_D,0);
    st7735_send(LCD_D,y+15);

    st7735_send(LCD_C,ST77XX_RAMWR);
#ifdef HW_SPI

    gpio_set(GPIOB,DC);
    SPI1->CR1 &= CR1_SPE_Reset;      // disable SPI for setup
    SPI1->CR1  = (SPI_Mode_Master|SPI_DataSize_16b|SPI_NSS_Soft|SPI_BaudRatePrescaler_2);
    SPI1->CR1 |= CR1_SPE_Set;        // enable SPI
    for (uint8_t j=0;j<128;j++) {
        uint16_t color;

        color=buf[j + x ];
        while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
        SPI1->DR=color;
    } 

    while (!(SPI1->SR & SPI_I2S_FLAG_TXE) || (SPI1->SR & SPI_I2S_FLAG_BSY));
    chip_select_disable();

    SPI1->CR1 &= CR1_SPE_Reset;      // disable SPI for setup
    SPI1->CR1  = (SPI_Mode_Master|SPI_DataSize_8b|SPI_NSS_Soft|SPI_BaudRatePrescaler_2);
    SPI1->CR1 |= CR1_SPE_Set;        // enable SPI
#else
   chip_select_disable();
#endif

}

void cp866_16_char_to_buf(uint8_t x_pos, uint8_t ch, uint8_t len, uint16_t fg_color, uint16_t bg_color, uint16_t* buffer) {
    if ((x_pos + FONT_CP866_16_CHAR_WIDTH) > LCD_X)
        return;
    cp866x16_decompress_sym(ch);

    for (uint8_t i=0; i < (FONT_CP866_16_CHAR_WIDTH * FONT_CP866_16_CHAR_HEIGHT); i++) {
        uint8_t data=sym_buf[i];

        uint16_t index= x_pos + (i*len*8);
        for (uint8_t j=0; j<8;j++) {

            buffer[index++]=(data & 0x80) ? fg_color : bg_color;
            data = data << 1;
        }
    }
}

void cp866x16_decompress_sym(uint8_t sym) {
    uint16_t idx=CP866_8x16_ID[sym];
    uint8_t i=0;
    while (i<(FONT_CP866_16_CHAR_WIDTH*FONT_CP866_16_CHAR_HEIGHT)) {
        uint8_t s=CP866_8x16[idx];
        if (s >= 0xf0) {
            s &= 0x0f;
            if (s==0) s=16;

            for (uint8_t j=0; j<s;j++) {
                uint8_t value=CP866_8x16[idx+1];
                sym_buf[i+j]=value;
            }
            i += s;
            idx +=2;
        } else {
            sym_buf[i]=s;
            ++idx; ++i;
        }
    }
}
