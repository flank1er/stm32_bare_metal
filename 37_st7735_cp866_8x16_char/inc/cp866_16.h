#ifndef __CP866_16_H__
#define __CP866_16_H__
#include "main.h"
#include "cp866_8x16.h"

void cp866x16_print_num_from_char(uint32_t num, uint8_t x, uint8_t y, uint16_t fg, uint16_t bg);
void cp866x16_print_str_from_char(char *str, uint8_t x, uint8_t y, uint16_t fg_color, uint16_t bg_color);
void cp866x16_send_char(uint8_t x, uint8_t y, uint8_t ch, uint16_t fg_color, uint16_t bg_color);

#endif  // __CP866_16_H__
