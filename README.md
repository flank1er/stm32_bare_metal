# stm32_bare_metal

some examples for stm32f103c8t6 (bluepill)

part  1-12 - http://www.count-zero.ru/2018/stm32_start/
part 13-27 - http://www.count-zero.ru/2022/display/
part 28-32 - http://www.count-zero.ru/2023/ste2007/
part 33-49 - http://www.count-zero.ru/2023/st7735/

License: WTFPL
