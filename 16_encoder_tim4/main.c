#include "main.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_usart.h"
#include "stm32f10x_rcc.h"
#include "uart.h"
extern uint8_t tim4_counter;
extern uint8_t uart_ready;
extern uint8_t uart_index;
extern char uart_buf[UART_BUFFER_LEN];              // UART_BUFFER_LEN=10
void clear_uart_buf();
void print_encoder(char* str, int value);

int main()
{
    // enable GPIOC port
    RCC->APB2ENR |= RCC_APB2Periph_GPIOC;           // enable PORT_C
    RCC->APB2ENR |= RCC_APB2Periph_GPIOA;           // enable PORT_A
    RCC->APB2ENR |= RCC_APB2Periph_USART1;          // enable UART1
    RCC->APB1ENR |= RCC_APB1Periph_TIM2;            // enable TIM2
    RCC->APB1ENR |= RCC_APB1Periph_TIM4;    // enable TIM4
    // --- GPIO setup ----
    GPIOC->CRH &= ~(uint32_t)(0xf<<20);             // Reset for PC13 (LED)
    GPIOC->CRH |=  (uint32_t)(0x2<<20);             // Push-Pull 2-MHz fo PC13 (LED)
	gpio_set(GPIOC,LED);
    GPIOA->CRH &= ~(uint32_t)(0xf<<4);              // enable Alterentive mode
    GPIOA->CRH |=  (uint32_t)(0xa<<4);              // for PA9 = USART1_TX
    GPIOA->CRH &= ~(uint32_t)(0xf<<8);              // enable Alterentive mode
    GPIOA->CRH |=  (uint32_t)(0xa<<8);              // for PA10 = USART1_RX
    // encoder PA0, PA1
    GPIOA->CRL &= ~(uint32_t)(0xff);                // clear
    GPIOA->CRL |=  (uint32_t)(0x88);                // pull-down input mode for PA0, PA1
    GPIOA->ODR |= (uint16_t)(0x3);                  // switch to pull-up mode for PA0,PA1
    // ------- SysTick CONFIG --------------
    if (SysTick_Config(72000)) // set 1ms
    {
        while(1); // error
    }
    // ------ TIM2 Setup -------------------
    TIM2->CCMR1 = TIM_CCMR1_CC1S_0 | TIM_CCMR1_CC2S_0;
    TIM2->CCER  = TIM_CCER_CC1P | TIM_CCER_CC2P;
    TIM2->SMCR  = TIM_SMCR_SMS_0 | TIM_SMCR_SMS_1;
    TIM2->ARR   = 1000;
    TIM2->CR1   = TIM_CR1_CEN;
    // ----- TIM4 Setup -------
    TIM4->CR1 = (0x80);                                 // set ARPE flag
    TIM4->PSC = 36000 - 1;                              // 1000 tick/sec
    TIM4->ARR = 100;                                    // 10 Interrupt/sec (1000/100)
    TIM4->DIER |=  TIM_DIER_UIE;                        // enable interrupt per overflow
    TIM4->SR = 0;
    TIM4->EGR |= (0x01);
    TIM4->CR1 |= TIM_CR1_CEN;                           // enable timer
    // --- UART setup ----
    //USART1->BRR  = 0x1d4c;                        // 9600 Baud, when APB2=72MHz
    USART1->BRR = 0x271;                            // 115200 Baud, when APB2=72MHz
    USART1->CR1 |= (USART_CR1_UE_Set | USART_Mode_Tx | USART_Mode_Rx | USART_CR1_RXNEIE);  // enable USART1, enable  TX/RX mode, enable RX interrupt
    NVIC_SetPriority(USART1_IRQn,1);                // set priority for USART1 IRQ
    NVIC_EnableIRQ(USART1_IRQn);                    // enable USART1 IRQ via NVIC
    NVIC_SetPriority(TIM4_IRQn, 21);                // set priority for TIM4_IRQ
    NVIC_EnableIRQ(TIM4_IRQn);                      // enable TIM4 IRQ
    //////////////////////////////////////
    /// let's go.....
    /////////////////////////////////////
    bool isEncoder=false;
    int enc_prev_value=0;
    uint8_t led_toggle=0;
    bool tick_enable=1;
    uint8_t count=0;
    clear_uart_buf();
	println("ready...");
    for(;;){
        if (uart_ready) {
            if (uart_buf[0] == '?' && uart_buf[1] == 0x0) {
                print("help:\n");
                print("t-  disable print tick\n");
            } else if (uart_buf[0] == 't' && uart_buf[1] == '-') {
                print("turn off tick\n");
                tick_enable=false;
            } else {
                print("invalid command\n");
            }
            clear_uart_buf();
            uart_ready=0;
            uart_index=0;
        }

        int enc_value=TIM2->CNT;
        enc_value >>=2;
        if (enc_value != enc_prev_value) {
            enc_prev_value=enc_value;
            isEncoder=true;
            print_encoder("encoder: ",enc_value);
            tim4_counter=TIM4_INIT_VALUE;
        } else if (isEncoder && !tim4_counter) {
            __disable_irq();
            isEncoder=false;
            TIM2->CNT=0;
            __enable_irq();
            enc_prev_value=0;
            print_encoder("total: ",enc_value);
        }

        if ((++count % 20)  == 0) {
            if (led_toggle)
                gpio_set(GPIOC,LED);
            else
                gpio_reset(GPIOC,LED);
            led_toggle=!led_toggle;
            if (tick_enable)
                print("tick..\n");
            count=0;
        }
        delay_ms(50);
    }
}

void clear_uart_buf() {
    for (uint8_t i=0; i<UART_BUFFER_LEN;i++)
        uart_buf[i]=0;
}

void print_encoder(char* str, int value) {
    if (value >= 125) {
        value -= 250;
    }
    print(str);
    uart_print_int(value);
    uart_send_char('\n');
}
