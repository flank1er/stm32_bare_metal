#include <stdint.h>
#include "main.h"

static __IO uint32_t s_timer;

uint8_t uart_ready;
uint8_t uart_index;
char uart_buf[UART_BUFFER_LEN];
//int enc_prev_value=0;
uint8_t tim4_counter=TIM4_INIT_VALUE;                // 3 sec

void  TIM4_IRQHandler(void){
    if (tim4_counter)
        --tim4_counter;

    TIM4->SR &= ~(0x01);
}

void USART1_IRQHandler(void)
{
    if ((USART1->SR & USART_SR_ORE) || uart_ready) {
        USART1->DR;
        uart_index=0;
        return;
    }

    uint8_t ch=USART1->DR;
    if (ch == 0xa || ch == 0xd) {
        uart_ready=1;
        uart_buf[uart_index]=0;
    } else {
        if (uart_index < (UART_BUFFER_LEN-1))
            uart_buf[uart_index++]=ch;
        else{
            uart_ready=1;
            uart_buf[UART_BUFFER_LEN-1]=0;
        }
    }
}

void SysTick_Handler(void)
{
        if (s_timer)
            s_timer--;
}

void systick_delay(__IO uint32_t val) {
    s_timer=val;
    SysTick->LOAD |= SysTick_CTRL_ENABLE;
    while(s_timer) {
         __asm__ volatile ("wfi\n");
    };
    SysTick->LOAD &= ~(SysTick_CTRL_ENABLE_Msk);    // disable SysTick
}

