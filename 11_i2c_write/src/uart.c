#include "uart.h"

#define len 10

const uint8_t hex[]="0123456789ABCDEF";

void usart1_print_number(uint32_t num){
    uint8_t n[len];
    char *s=n+(len-1);
    *s=0;           // EOL
    do {
        *(--s)=(uint32_t)(num%10 + 0x30);
        num=num/10;
    } while (num>0);

    usart1_print_string(s);
}

void usart1_send_char(uint32_t ch) {

	USART1->DR=ch;
	while(!(USART1->SR & USART_FLAG_TXE));
}

void usart1_print_string(char *str) {
    while (*str)
    {
        usart1_send_char((uint32_t)*str++);
    }
}

void usart1_print_hex(uint32_t num){
    uint8_t n[len];
    char *s=n+(len-1);
    *s=0;           // EOL
    do {
        *(--s)=hex[num%16];
        num=num/16;
    } while (num>0);
    *(--s)='x'; *(--s)='0';
    usart1_print_string(s);
}

void usart1_print_bcd(uint8_t num) {
    USART1->DR=(num>>4) + 0x30;
    while(!(USART1->SR & USART_FLAG_TXE));
    USART1->DR=(num & 0x0f) + 0x30;
    while(!(USART1->SR & USART_FLAG_TXE));
}

