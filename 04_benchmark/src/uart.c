#include "uart.h"
#define len 8

void usart1_print_number(uint32_t num){
    uint8_t n[len];
    char *s=n+(len-1);
    *s=0;           // EOL
    do {
        *(--s)=(uint32_t)(num%10 + 0x30);
        num=num/10;
    } while (num>0);

    usart1_print_string(s);
}

void usart1_send_char(uint32_t ch) {

	USART1->DR=ch;
	while(!(USART1->SR & USART_FLAG_TXE));
}

void usart1_print_string(char *str) {
    while (*str)
    {
        usart1_send_char((uint32_t)*str++);
    }
}
