#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "uart.h"

extern void delay(uint32_t ms);
//static __IO uint32_t s_timer;
volatile uint32_t s_timer;

uint32_t div_count;
uint32_t result;

void SysTick_Handler(void)
{
		if (s_timer)
			s_timer--;
}

void delay_ms(__IO uint32_t val) {
    // ------- SysTick CONFIG --------------
    if (SysTick_Config(72000)) // set 1ms
    {
        while(1); // error
    }

	s_timer=val;

    while(s_timer) {
        result=s_timer/(uint32_t)314;
        div_count++;
//		asm("wfi");
	};

	SysTick->LOAD &= ~(SysTick_CTRL_ENABLE_Msk);	// disable SysTick
}

int main()
{
    // enable GPIOC port
    RCC->APB2ENR |= RCC_APB2Periph_GPIOC;			// enable PORT_C
    RCC->APB2ENR |= RCC_APB2Periph_GPIOA;			// enable PORT_A
	RCC->APB2ENR |= RCC_APB2Periph_USART1; 			// enable UART1
    // --- GPIO setup ----
    GPIOC->CRH &= ~(uint32_t)(0xf<<20);
    GPIOC->CRH |=  (uint32_t)(0x2<<20);
	GPIOA->CRH &= ~(uint32_t)(0xf<<4);
	GPIOA->CRH |=  (uint32_t)(0xa<<4);
	// --- UART setup ----

    //USART1->BRR  = 0x1d4c;								// 9600 Baud, when APB2=72MHz
	USART1->BRR = 0x271;								// 115200 Baud, when APB2=72MHz
	USART1->CR1 |= USART_CR1_UE_Set | USART_Mode_Tx;	// enable USART1, enable  TX mode
	// Let's go..
	__enable_irq();
    for(;;){
		div_count=0;
		GPIOC->ODR ^= GPIO_Pin_13;
		delay_ms(50);
		usart1_print_string("count: ");
		usart1_print_number(div_count);
		usart1_send_char('\n');
		delay(950);
    }

}
