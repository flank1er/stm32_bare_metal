#include "koi8r_32.h"
#include "ter-k32n.h"
#include "ter-k32b.h"
#include "st7735.h"
#include "uart.h"

#define numlen 10
#define CHAR_LEN 512                 // 512=(16 x 32)  i.e. width * height

static volatile uint16_t sym32_buf[(TERMINUS_K16x32N_CHAR_WIDTH*TERMINUS_K16x32N_CHAR_HEIGHT)];        // 64 bytes
extern uint16_t buf[];

void KOI8Rx32_char_to_buf_2X(uint8_t x_pos, uint8_t ch, uint8_t len, uint8_t font, uint16_t fg_color, uint16_t bg_color, uint16_t* buffer);
void KOI8Rx32_char_to_buf(uint8_t x_pos, uint8_t ch, uint8_t len, uint8_t font, uint16_t fg_color, uint16_t bg_color, uint16_t* buffer);
void KOI8Rx32_decompress_sym(uint8_t sym,uint8_t font);


void KOI8Rx32_print_num(uint32_t num, uint8_t x, uint8_t y, uint16_t fg, uint16_t bg, uint8_t font, uint8_t scale){
    uint8_t n[numlen];
    uint8_t *s=n+(numlen-1);
    *s=0;           // EOL
    do {
        *(--s)=('0' + num%10);
        num = num/10;
    } while (num>0);
    KOI8Rx32_print_str_from_char((char*)s,x,y,fg,bg,font, scale);
}

void KOI8Rx32_print_str_from_char(char *str, uint8_t x, uint8_t y, uint16_t fg_color, uint16_t bg_color,  uint8_t font, uint8_t scale) {
    uint8_t i=0;
    while (*str) {
        KOI8Rx32_send_char(x + scale*i*TERMINUS_K16x32N_CHAR_WIDTH,y,*str++, font, fg_color,bg_color, scale);
        ++i;
    }
}


void KOI8Rx32_send_char(uint8_t x, uint8_t y, uint8_t ch, uint8_t font, uint16_t fg_color, uint16_t bg_color, uint8_t scale) {

    if (scale == 1)
        KOI8Rx32_char_to_buf(0,ch, 1, font, fg_color,bg_color,buf);
    else
        KOI8Rx32_char_to_buf_2X(0,ch, 1, font, fg_color,bg_color,buf);

    chip_select_enable();
    // 16x32 area
    st7735_send(LCD_C,ST77XX_CASET);
    st7735_send(LCD_D,0);
    st7735_send(LCD_D,x);
    st7735_send(LCD_D,0);
    st7735_send(LCD_D,x+((TERMINUS_K16x32N_CHAR_WIDTH*scale)-1));

    st7735_send(LCD_C,ST77XX_RASET);
    st7735_send(LCD_D,0);
    st7735_send(LCD_D,y);
    st7735_send(LCD_D,0);
    st7735_send(LCD_D,y+((TERMINUS_K16x32N_CHAR_WIDTH*scale*2)-1));

    st7735_send(LCD_C,ST77XX_RAMWR);
#ifdef HW_SPI

    gpio_set(GPIOB,DC);
    SPI1->CR1 &= CR1_SPE_Reset;      // disable SPI for setup
    SPI1->CR1  = (SPI_Mode_Master|SPI_DataSize_16b|SPI_NSS_Soft|SPI_BaudRatePrescaler_2);
    SPI1->CR1 |= CR1_SPE_Set;        // enable SPI
    for (uint16_t j=0;j<(CHAR_LEN*scale *scale);j++) {
        uint16_t color;

        color=buf[j];
        while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
        SPI1->DR=color;
        //SPI1->DR=fg_color;
    }

    while (!(SPI1->SR & SPI_I2S_FLAG_TXE) || (SPI1->SR & SPI_I2S_FLAG_BSY));
    chip_select_disable();

    SPI1->CR1 &= CR1_SPE_Reset;      // disable SPI for setup
    SPI1->CR1  = (SPI_Mode_Master|SPI_DataSize_8b|SPI_NSS_Soft|SPI_BaudRatePrescaler_2);
    SPI1->CR1 |= CR1_SPE_Set;        // enable SPI
#else
   chip_select_disable();
#endif

}

void KOI8Rx32_char_to_buf(uint8_t x_pos, uint8_t ch, uint8_t len, uint8_t font, uint16_t fg_color, uint16_t bg_color, uint16_t* buffer) {
    if ((x_pos + TERMINUS_K16x32N_CHAR_WIDTH) > LCD_X)
        return;

    KOI8Rx32_decompress_sym(ch, font);

    for (uint8_t i=0; i < (TERMINUS_K16x32N_CHAR_WIDTH*TERMINUS_K16x32N_CHAR_HEIGHT); i++) {
        uint16_t data=sym32_buf[i];

        uint16_t index= x_pos + (i*len*TERMINUS_K16x32N_CHAR_WIDTH);
        for (uint8_t j=0; j<TERMINUS_K16x32N_CHAR_WIDTH;j++) {

            buffer[index++]=(data & (uint16_t)0x8000) ? fg_color : bg_color;
            data = data << 1;
        }
    }
}



void KOI8Rx32_char_to_buf_2X(uint8_t x_pos, uint8_t ch, uint8_t len, uint8_t font, uint16_t fg_color, uint16_t bg_color, uint16_t* buffer) {
    if ((x_pos + (TERMINUS_K16x32N_CHAR_WIDTH*2)) > LCD_X)
        return;

    KOI8Rx32_decompress_sym(ch, font);

    for (uint8_t i=0; i < (TERMINUS_K16x32N_CHAR_WIDTH*TERMINUS_K16x32N_CHAR_HEIGHT); i++) {
        uint16_t data=sym32_buf[i];

        uint16_t index= x_pos + (i*len*TERMINUS_K16x32N_CHAR_WIDTH*4);
        for (uint8_t j=0; j<TERMINUS_K16x32N_CHAR_WIDTH;j++) {

            uint16_t color=(data & (uint16_t)0x8000) ? fg_color : bg_color;
            buffer[index+(TERMINUS_K16x32N_CHAR_WIDTH*2)]=color;
            buffer[index+(TERMINUS_K16x32N_CHAR_WIDTH*2+1)]=color;
            buffer[index++]=color;
            buffer[index++]=color;
            data = data << 1;
        }
    }
}

void KOI8Rx32_decompress_sym(uint8_t sym, uint8_t font) {
    uint16_t idx;

    idx=(!font) ? TERMINUS_K16x32N_ID[sym] : TERMINUS_K16x32B_ID[sym];
    uint8_t i=0;
    while (i<32) {
        uint8_t index = (!font) ? TERMINUS_K16x32N[idx++] : TERMINUS_K16x32B[idx++];
        if ((index & 0x03) == 0x03) {
            index=(index >> 2);
            if (index == 0) index=0x20;
            for (uint8_t j=0; j<index;j++) {
                sym32_buf[i++]=0x0;
            }
        } else if (index & 0x80) {
            index &=0x7f;
            index = index>>2;
            if (index == 0) index=0x20;
            for(uint8_t j=0;j<index;j++){
                uint16_t a1=(!font) ? TERMINUS_K16x32N[idx++] : TERMINUS_K16x32B[idx++];
                uint16_t a2=(!font) ? TERMINUS_K16x32N[idx++] : TERMINUS_K16x32B[idx++];
                a1=((a2<<8) + a1);

                sym32_buf[i++]= a1;
            }
        } else {
            index=(index >> 2);
            if (index == 0) index=0x20;
            uint16_t a1=(!font) ? TERMINUS_K16x32N[idx++] : TERMINUS_K16x32B[idx++];
            uint16_t a2=(!font) ? TERMINUS_K16x32N[idx++] : TERMINUS_K16x32B[idx++];
            a1=((a2<<8) + a1);

            for(uint8_t j=0;j<index;j++){
                sym32_buf[i++]= a1;
            }

        }
    }
}

