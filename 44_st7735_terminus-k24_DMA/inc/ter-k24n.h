#ifndef __TERMINUS_K12x24N_H__
#define __TERMINUS_K12x24N_H__
#include <stdint.h>
// Terminus  Console Font
// https://terminus-font.sourceforge.net/
// License: OFL
// size 12x24 px, regular
// codepage KOI8-R
//
// converted by flanker from PSF2 file format.
// compressed size: 1993 max lenght: 24
// compression algorithm: RLE
// www.count-zero.ru
// date 14 May 2023
//
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!! Horizontal endianness !!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#define TERMINUS_K12x24N_LENGTH           256      // full ASCII+KOI8-R code page
#define TERMINUS_K12x24N_START_CHAR       0
#define TERMINUS_K12x24N_CHAR_WIDTH       12
#define TERMINUS_K12x24N_CHAR_HEIGHT      2
#define TERMINUS_K12x24N_ARRAY_LENGTH     1993     // compressed

static const uint16_t TERMINUS_K12x24N[TERMINUS_K12x24N_ARRAY_LENGTH]= {
// первые 32 символа
0x4000, 0x17fc, 0xd404, 0x17fc, 0x5000,                                                                                                                   // Символ: 0 0x0 lenght: 5 offset: 0
0x4000, 0x13f8, 0x1404, 0x2802, 0x29b2, 0x3802, 0x19f2, 0x18e2, 0x2802, 0x1404, 0x13f8, 0x5000,                                                           // Символ: 1 0x1 lenght: 12 offset: 5
0x4000, 0x13f8, 0x17fc, 0x2ffe, 0x2e4e, 0x3ffe, 0x1e0e, 0x1f1e, 0x2ffe, 0x17fc, 0x13f8, 0x5000,                                                           // Символ: 2 0x2 lenght: 12 offset: 17
0x6000, 0x1318, 0x17bc, 0x5ffe, 0x17fc, 0x13f8, 0x11f0, 0x10e0, 0x1040, 0x6000,                                                                           // Символ: 3 0x3 lenght: 10 offset: 29
0x6000, 0x1040, 0x10e0, 0x11f0, 0x13f8, 0x17fc, 0x1ffe, 0x17fc, 0x13f8, 0x11f0, 0x10e0, 0x1040, 0x7000,                                                   // Символ: 4 0x4 lenght: 13 offset: 39
0x4000, 0x10e0, 0x21f0, 0x10e0, 0x2040, 0x175c, 0x3ffe, 0x175c, 0x3040, 0x11f0, 0x5000,                                                                   // Символ: 5 0x5 lenght: 11 offset: 52
0x4000, 0x2040, 0x10e0, 0x11f0, 0x13f8, 0x57fc, 0x1358, 0x3040, 0x11f0, 0x5000,                                                                           // Символ: 6 0x6 lenght: 10 offset: 63
0x9000, 0x10f0, 0x41f8, 0x10f0, 0x9000,                                                                                                                   // Символ: 7 0x7 lenght: 5 offset: 73
0x9fff, 0x1f0f, 0x4e07, 0x1f0f, 0x9fff,                                                                                                                   // Символ: 8 0x8 lenght: 5 offset: 78
0x9000, 0x10f0, 0x4108, 0x10f0, 0x9000,                                                                                                                   // Символ: 9 0x9 lenght: 5 offset: 83
0x9fff, 0x1f0f, 0x4ef7, 0x1f0f, 0x9fff,                                                                                                                   // Символ: 10 0xa lenght: 5 offset: 88
0x4000, 0x107e, 0x1006, 0x100a, 0x1012, 0x1022, 0x1042, 0x11f0, 0x1208, 0x5404, 0x1208, 0x11f0, 0x5000,                                                   // Символ: 11 0xb lenght: 13 offset: 93
0x4000, 0x11f0, 0x1208, 0x5404, 0x1208, 0x11f0, 0x2040, 0x17fc, 0x3040, 0x5000,                                                                           // Символ: 12 0xc lenght: 10 offset: 106
0x4000, 0x11fe, 0x3102, 0x11fe, 0x9100, 0x1e00, 0x5000,                                                                                                   // Символ: 13 0xd lenght: 7 offset: 116
0x4000, 0x17fc, 0x3404, 0x17fc, 0x8404, 0x1408, 0x1800, 0x5000,                                                                                           // Символ: 14 0xe lenght: 8 offset: 123
0x4000, 0x2040, 0x1842, 0x1444, 0x1248, 0x1150, 0x10e0, 0x1fbe, 0x10e0, 0x1150, 0x1248, 0x1444, 0x1842, 0x2040, 0x5000,                                   // Символ: 15 0xf lenght: 15 offset: 131
0x6000, 0x1c00, 0x1f00, 0x1fc0, 0x1ff0, 0x1ffc, 0x1fff, 0x1ffc, 0x1ff0, 0x1fc0, 0x1f00, 0x1c00, 0x7000,                                                   // Символ: 16 0x10 lenght: 13 offset: 146
0x6000, 0x1003, 0x100f, 0x103f, 0x10ff, 0x13ff, 0x1fff, 0x13ff, 0x10ff, 0x103f, 0x100f, 0x1003, 0x7000,                                                   // Символ: 17 0x11 lenght: 13 offset: 159
0x4000, 0x1040, 0x10e0, 0x1150, 0x1248, 0x1444, 0x5040, 0x1444, 0x1248, 0x1150, 0x10e0, 0x1040, 0x5000,                                                   // Символ: 18 0x12 lenght: 13 offset: 172
0x4000, 0xa110, 0x2000, 0x3110, 0x5000,                                                                                                                   // Символ: 19 0x13 lenght: 5 offset: 185
0x4000, 0x13fc, 0x6444, 0x13c4, 0x7044, 0x5000,                                                                                                           // Символ: 20 0x14 lenght: 6 offset: 190
0x3000, 0x10f0, 0x1108, 0x2100, 0x10e0, 0x1110, 0x4108, 0x1088, 0x1070, 0x2008, 0x1108, 0x10f0, 0x5000,                                                   // Символ: 21 0x15 lenght: 13 offset: 196
0xd000, 0x67fe, 0x5000,                                                                                                                                   // Символ: 22 0x16 lenght: 3 offset: 209
0x4000, 0x1040, 0x10e0, 0x1150, 0x1248, 0x1444, 0x4040, 0x1444, 0x1248, 0x1150, 0x10e0, 0x1040, 0x17fc, 0x5000,                                           // Символ: 23 0x17 lenght: 14 offset: 212
0x4000, 0x1040, 0x10e0, 0x1150, 0x1248, 0x1444, 0xa040, 0x5000,                                                                                           // Символ: 24 0x18 lenght: 8 offset: 226
0x4000, 0xa040, 0x1444, 0x1248, 0x1150, 0x10e0, 0x1040, 0x5000,                                                                                           // Символ: 25 0x19 lenght: 8 offset: 234
0x7000, 0x1020, 0x1010, 0x1008, 0x1004, 0x1ffe, 0x1004, 0x1008, 0x1010, 0x1020, 0x8000,                                                                   // Символ: 26 0x1a lenght: 11 offset: 242
0x7000, 0x1080, 0x1100, 0x1200, 0x1400, 0x1ffe, 0x1400, 0x1200, 0x1100, 0x1080, 0x8000,                                                                   // Символ: 27 0x1b lenght: 11 offset: 253
0x7000, 0x8400, 0x17fc, 0x8000,                                                                                                                           // Символ: 28 0x1c lenght: 4 offset: 264
0x7000, 0x1090, 0x1108, 0x1204, 0x1402, 0x1fff, 0x1402, 0x1204, 0x1108, 0x1090, 0x8000,                                                                   // Символ: 29 0x1d lenght: 11 offset: 268
0x6000, 0x2040, 0x20e0, 0x21f0, 0x23f8, 0x27fc, 0x2ffe, 0x6000,                                                                                           // Символ: 30 0x1e lenght: 8 offset: 279
0x6000, 0x2ffe, 0x27fc, 0x23f8, 0x21f0, 0x20e0, 0x2040, 0x6000,                                                                                           // Символ: 31 0x1f lenght: 8 offset: 287
// Знаки
0x0, 0x8000,                                                                                                                                              // Символ: 32 0x20 lenght: 2 offset: 295
0x4000, 0xa040, 0x2000, 0x3040, 0x5000,                                                                                                                   // Символ: 33 0x21 lenght: 5 offset: 297
0x2000, 0x5110, 0x0, 0x1000,                                                                                                                              // Символ: 34 0x22 lenght: 4 offset: 302
0x4000, 0x4110, 0x17fc, 0x5110, 0x17fc, 0x4110, 0x5000,                                                                                                   // Символ: 35 0x23 lenght: 7 offset: 306
0x3000, 0x2040, 0x11f0, 0x1248, 0x1444, 0x2440, 0x1240, 0x11f0, 0x1048, 0x2044, 0x1444, 0x1248, 0x11f0, 0x2040, 0x4000,                                   // Символ: 36 0x24 lenght: 15 offset: 313
0x5000, 0x1308, 0x1488, 0x1490, 0x1310, 0x2020, 0x2040, 0x2080, 0x1118, 0x1124, 0x1224, 0x1218, 0x5000,                                                   // Символ: 37 0x25 lenght: 13 offset: 328
0x4000, 0x11e0, 0x4210, 0x1120, 0x10c0, 0x1140, 0x1224, 0x1414, 0x3408, 0x1214, 0x11e4, 0x5000,                                                           // Символ: 38 0x26 lenght: 12 offset: 341
0x2000, 0x5040, 0x0, 0x1000,                                                                                                                              // Символ: 39 0x27 lenght: 4 offset: 353
0x4000, 0x1010, 0x1020, 0x2040, 0x7080, 0x2040, 0x1020, 0x1010, 0x5000,                                                                                   // Символ: 40 0x28 lenght: 9 offset: 357
0x4000, 0x1080, 0x1040, 0x2020, 0x7010, 0x2020, 0x1040, 0x1080, 0x5000,                                                                                   // Символ: 41 0x29 lenght: 9 offset: 366
0x7000, 0x1208, 0x1110, 0x10a0, 0x1040, 0x17fc, 0x1040, 0x10a0, 0x1110, 0x1208, 0x8000,                                                                   // Символ: 42 0x2a lenght: 11 offset: 375
0x7000, 0x4040, 0x17fc, 0x4040, 0x8000,                                                                                                                   // Символ: 43 0x2b lenght: 5 offset: 386
0xf000, 0x4040, 0x1080, 0x4000,                                                                                                                           // Символ: 44 0x2c lenght: 4 offset: 391
0xb000, 0x17fc, 0xc000,                                                                                                                                   // Символ: 45 0x2d lenght: 3 offset: 395
0x0, 0x3040, 0x5000,                                                                                                                                      // Символ: 46 0x2e lenght: 3 offset: 398
0x5000, 0x2008, 0x2010, 0x2020, 0x2040, 0x2080, 0x2100, 0x2200, 0x5000,                                                                                   // Символ: 47 0x2f lenght: 9 offset: 401
// Digits, Цифры
0x4000, 0x11f0, 0x1208, 0x2404, 0x140c, 0x1414, 0x1424, 0x1444, 0x1484, 0x1504, 0x1604, 0x2404, 0x1208, 0x11f0, 0x5000,                                   // Символ: 48 0x30 lenght: 15 offset: 410
0x4000, 0x1040, 0x10c0, 0x1140, 0x1240, 0xa040, 0x13f8, 0x5000,                                                                                           // Символ: 49 0x31 lenght: 8 offset: 425
0x4000, 0x11f0, 0x1208, 0x3404, 0x1004, 0x1008, 0x1010, 0x1020, 0x1040, 0x1080, 0x1100, 0x1200, 0x1400, 0x17fc, 0x5000,                                   // Символ: 50 0x32 lenght: 15 offset: 433
0x4000, 0x11f0, 0x1208, 0x1404, 0x3004, 0x1008, 0x10f0, 0x1008, 0x3004, 0x1404, 0x1208, 0x11f0, 0x5000,                                                   // Символ: 51 0x33 lenght: 13 offset: 448
0x4000, 0x1004, 0x100c, 0x1014, 0x1024, 0x1044, 0x1084, 0x1104, 0x1204, 0x3404, 0x17fc, 0x3004, 0x5000,                                                   // Символ: 52 0x34 lenght: 13 offset: 461
0x4000, 0x17fc, 0x5400, 0x17f0, 0x1008, 0x4004, 0x1404, 0x1208, 0x11f0, 0x5000,                                                                           // Символ: 53 0x35 lenght: 10 offset: 474
0x4000, 0x11f8, 0x1200, 0x4400, 0x17f0, 0x1408, 0x5404, 0x1208, 0x11f0, 0x5000,                                                                           // Символ: 54 0x36 lenght: 10 offset: 484
0x4000, 0x17fc, 0x2404, 0x1004, 0x2008, 0x2010, 0x2020, 0x5040, 0x5000,                                                                                   // Символ: 55 0x37 lenght: 9 offset: 494
0x4000, 0x11f0, 0x1208, 0x4404, 0x1208, 0x11f0, 0x1208, 0x4404, 0x1208, 0x11f0, 0x5000,                                                                   // Символ: 56 0x38 lenght: 11 offset: 503
0x4000, 0x11f0, 0x1208, 0x5404, 0x1204, 0x11fc, 0x4004, 0x1008, 0x13f0, 0x5000,                                                                           // Символ: 57 0x39 lenght: 10 offset: 514

0x8000, 0x3040, 0x4000, 0x3040, 0x6000,                                                                                                                   // Символ: 58 0x3a lenght: 5 offset: 524
0x8000, 0x3040, 0x4000, 0x4040, 0x1080, 0x4000,                                                                                                           // Символ: 59 0x3b lenght: 6 offset: 529
0x4000, 0x1004, 0x1008, 0x1010, 0x1020, 0x1040, 0x1080, 0x1100, 0x1200, 0x1100, 0x1080, 0x1040, 0x1020, 0x1010, 0x1008, 0x1004, 0x5000,                   // Символ: 60 0x3c lenght: 17 offset: 535
0x9000, 0x17fc, 0x4000, 0x17fc, 0x9000,                                                                                                                   // Символ: 61 0x3d lenght: 5 offset: 552
0x4000, 0x1200, 0x1100, 0x1080, 0x1040, 0x1020, 0x1010, 0x1008, 0x1004, 0x1008, 0x1010, 0x1020, 0x1040, 0x1080, 0x1100, 0x1200, 0x5000,                   // Символ: 62 0x3e lenght: 17 offset: 557
0x4000, 0x11f0, 0x1208, 0x3404, 0x1008, 0x1010, 0x1020, 0x2040, 0x2000, 0x3040, 0x5000,                                                                   // Символ: 63 0x3f lenght: 11 offset: 574
0x4000, 0x11f8, 0x1204, 0x1402, 0x143e, 0x1442, 0x5482, 0x1446, 0x143a, 0x1400, 0x1200, 0x11fe, 0x5000,                                                   // Символ: 64 0x40 lenght: 13 offset: 585
// Roman Capitals / Латиница, прописные
0x4000, 0x11f0, 0x1208, 0x6404, 0x17fc, 0x6404, 0x5000,                                                                                                   // Символ: 65 0x41 lenght: 7 offset: 598
0x4000, 0x17f0, 0x1408, 0x3404, 0x1408, 0x17f0, 0x1408, 0x5404, 0x1408, 0x17f0, 0x5000,                                                                   // Символ: 66 0x42 lenght: 11 offset: 605
0x4000, 0x11f0, 0x1208, 0x2404, 0x7400, 0x2404, 0x1208, 0x11f0, 0x5000,                                                                                   // Символ: 67 0x43 lenght: 9 offset: 616
0x4000, 0x17f0, 0x1408, 0xb404, 0x1408, 0x17f0, 0x5000,                                                                                                   // Символ: 68 0x44 lenght: 7 offset: 625
0x4000, 0x17fc, 0x6400, 0x17f0, 0x6400, 0x17fc, 0x5000,                                                                                                   // Символ: 69 0x45 lenght: 7 offset: 632
0x4000, 0x17fc, 0x6400, 0x17f0, 0x7400, 0x5000,                                                                                                           // Символ: 70 0x46 lenght: 6 offset: 639
0x4000, 0x11f0, 0x1208, 0x2404, 0x3400, 0x147c, 0x5404, 0x1208, 0x11f0, 0x5000,                                                                           // Символ: 71 0x47 lenght: 10 offset: 645
0x4000, 0x7404, 0x17fc, 0x7404, 0x5000,                                                                                                                   // Символ: 72 0x48 lenght: 5 offset: 655
0x4000, 0x11f0, 0xd040, 0x11f0, 0x5000,                                                                                                                   // Символ: 73 0x49 lenght: 5 offset: 660
0x4000, 0x103e, 0x9008, 0x3408, 0x1210, 0x11e0, 0x5000,                                                                                                   // Символ: 74 0x4a lenght: 7 offset: 665
0x4000, 0x1404, 0x1408, 0x1410, 0x1420, 0x1440, 0x1480, 0x1500, 0x1600, 0x1500, 0x1480, 0x1440, 0x1420, 0x1410, 0x1408, 0x1404, 0x5000,                   // Символ: 75 0x4b lenght: 17 offset: 672
0x4000, 0xe400, 0x17fc, 0x5000,                                                                                                                           // Символ: 76 0x4c lenght: 4 offset: 689
0x4000, 0x1402, 0x1606, 0x250a, 0x1492, 0x2462, 0x8402, 0x5000,                                                                                           // Символ: 77 0x4d lenght: 8 offset: 693
0x4000, 0x4404, 0x1604, 0x1504, 0x1484, 0x1444, 0x1424, 0x1414, 0x140c, 0x4404, 0x5000,                                                                   // Символ: 78 0x4e lenght: 11 offset: 701
0x4000, 0x11f0, 0x1208, 0xb404, 0x1208, 0x11f0, 0x5000,                                                                                                   // Символ: 79 0x4f lenght: 7 offset: 712
0x4000, 0x17f0, 0x1408, 0x4404, 0x1408, 0x17f0, 0x7400, 0x5000,                                                                                           // Символ: 80 0x50 lenght: 8 offset: 719
0x4000, 0x11f0, 0x1208, 0xa404, 0x1444, 0x1228, 0x11f0, 0x1008, 0x1004, 0x3000,                                                                           // Символ: 81 0x51 lenght: 10 offset: 727
0x4000, 0x17f0, 0x1408, 0x4404, 0x1408, 0x17f0, 0x1500, 0x1480, 0x1440, 0x1420, 0x1410, 0x1408, 0x1404, 0x5000,                                           // Символ: 82 0x52 lenght: 14 offset: 737
0x4000, 0x11f0, 0x1208, 0x1404, 0x3400, 0x1200, 0x11f0, 0x1008, 0x3004, 0x1404, 0x1208, 0x11f0, 0x5000,                                                   // Символ: 83 0x53 lenght: 13 offset: 751
0x4000, 0x17fc, 0xe040, 0x5000,                                                                                                                           // Символ: 84 0x54 lenght: 4 offset: 764
0x4000, 0xd404, 0x1208, 0x11f0, 0x5000,                                                                                                                   // Символ: 85 0x55 lenght: 5 offset: 768
0x4000, 0x4404, 0x3208, 0x3110, 0x30a0, 0x2040, 0x5000,                                                                                                   // Символ: 86 0x56 lenght: 7 offset: 773
0x4000, 0x8402, 0x2462, 0x1492, 0x250a, 0x1606, 0x1402, 0x5000,                                                                                           // Символ: 87 0x57 lenght: 8 offset: 780
0x4000, 0x2404, 0x2208, 0x2110, 0x10a0, 0x1040, 0x10a0, 0x2110, 0x2208, 0x2404, 0x5000,                                                                   // Символ: 88 0x58 lenght: 11 offset: 788
0x4000, 0x2404, 0x2208, 0x2110, 0x20a0, 0x7040, 0x5000,                                                                                                   // Символ: 89 0x59 lenght: 7 offset: 799
0x4000, 0x17fc, 0x3004, 0x1008, 0x1010, 0x1020, 0x1040, 0x1080, 0x1100, 0x1200, 0x3400, 0x17fc, 0x5000,                                                   // Символ: 90 0x5a lenght: 13 offset: 806

0x4000, 0x10f0, 0xd080, 0x10f0, 0x5000,                                                                                                                   // Символ: 91 0x5b lenght: 5 offset: 819
0x5000, 0x2200, 0x2100, 0x2080, 0x2040, 0x2020, 0x2010, 0x2008, 0x5000,                                                                                   // Символ: 92 0x5c lenght: 9 offset: 824
0x4000, 0x10f0, 0xd010, 0x10f0, 0x5000,                                                                                                                   // Символ: 93 0x5d lenght: 5 offset: 833
0x2000, 0x1040, 0x10a0, 0x1110, 0x1208, 0x1404, 0x0, 0x1000,                                                                                              // Символ: 94 0x5e lenght: 8 offset: 838
0x0, 0x4000, 0x17fc, 0x3000,                                                                                                                              // Символ: 95 0x5f lenght: 4 offset: 846
0x1000, 0x1100, 0x1080, 0x1040, 0x0, 0x4000,                                                                                                              // Символ: 96 0x60 lenght: 6 offset: 850
// Roman Smalls / Латиница, строчные
0x8000, 0x13f0, 0x1008, 0x2004, 0x11fc, 0x1204, 0x3404, 0x1204, 0x11fc, 0x5000,                                                                           // Символ: 97 0x61 lenght: 10 offset: 856
0x4000, 0x4400, 0x17f0, 0x1408, 0x7404, 0x1408, 0x17f0, 0x5000,                                                                                           // Символ: 98 0x62 lenght: 8 offset: 866
0x8000, 0x11f0, 0x1208, 0x1404, 0x5400, 0x1404, 0x1208, 0x11f0, 0x5000,                                                                                   // Символ: 99 0x63 lenght: 9 offset: 874
0x4000, 0x4004, 0x11fc, 0x1204, 0x7404, 0x1204, 0x11fc, 0x5000,                                                                                           // Символ: 100 0x64 lenght: 8 offset: 883
0x8000, 0x11f0, 0x1208, 0x3404, 0x17fc, 0x3400, 0x1204, 0x11f8, 0x5000,                                                                                   // Символ: 101 0x65 lenght: 9 offset: 891
0x4000, 0x103c, 0x3040, 0x13f8, 0xa040, 0x5000,                                                                                                           // Символ: 102 0x66 lenght: 6 offset: 900
0x8000, 0x11fc, 0x1204, 0x7404, 0x120c, 0x11f4, 0x2004, 0x1008, 0x13f0, 0x1000,                                                                           // Символ: 103 0x67 lenght: 10 offset: 906
0x4000, 0x4400, 0x17f0, 0x1408, 0x9404, 0x5000,                                                                                                           // Символ: 104 0x68 lenght: 6 offset: 916
0x4000, 0x3040, 0x1000, 0x11c0, 0x9040, 0x11f0, 0x5000,                                                                                                   // Символ: 105 0x69 lenght: 7 offset: 922
0x4000, 0x3008, 0x1000, 0x1038, 0xa008, 0x2208, 0x1110, 0x10e0, 0x1000,                                                                                   // Символ: 106 0x6a lenght: 9 offset: 929
0x4000, 0x4200, 0x1204, 0x1208, 0x1210, 0x1220, 0x1240, 0x1380, 0x1240, 0x1220, 0x1210, 0x1208, 0x1204, 0x5000,                                           // Символ: 107 0x6b lenght: 14 offset: 938
0x4000, 0x11c0, 0xd040, 0x11f0, 0x5000,                                                                                                                   // Символ: 108 0x6c lenght: 5 offset: 952
0x8000, 0x17f0, 0x1448, 0x9444, 0x5000,                                                                                                                   // Символ: 109 0x6d lenght: 5 offset: 957
0x8000, 0x17f0, 0x1408, 0x9404, 0x5000,                                                                                                                   // Символ: 110 0x6e lenght: 5 offset: 962
0x8000, 0x11f0, 0x1208, 0x7404, 0x1208, 0x11f0, 0x5000,                                                                                                   // Символ: 111 0x6f lenght: 7 offset: 967
0x8000, 0x17f0, 0x1408, 0x7404, 0x1408, 0x17f0, 0x4400, 0x1000,                                                                                           // Символ: 112 0x70 lenght: 8 offset: 974
0x8000, 0x11fc, 0x1204, 0x7404, 0x1204, 0x11fc, 0x4004, 0x1000,                                                                                           // Символ: 113 0x71 lenght: 8 offset: 982
0x8000, 0x147c, 0x1480, 0x1500, 0x1600, 0x7400, 0x5000,                                                                                                   // Символ: 114 0x72 lenght: 7 offset: 990
0x8000, 0x13f8, 0x1404, 0x3400, 0x13f8, 0x3004, 0x1404, 0x13f8, 0x5000,                                                                                   // Символ: 115 0x73 lenght: 9 offset: 997
0x4000, 0x4040, 0x13f8, 0x9040, 0x103c, 0x5000,                                                                                                           // Символ: 116 0x74 lenght: 6 offset: 1006
0x8000, 0x9404, 0x1204, 0x11fc, 0x5000,                                                                                                                   // Символ: 117 0x75 lenght: 5 offset: 1012
0x8000, 0x3404, 0x2208, 0x2110, 0x20a0, 0x2040, 0x5000,                                                                                                   // Символ: 118 0x76 lenght: 7 offset: 1017
0x8000, 0x4404, 0x6444, 0x13f8, 0x5000,                                                                                                                   // Символ: 119 0x77 lenght: 5 offset: 1024
0x8000, 0x2404, 0x1208, 0x1110, 0x10a0, 0x1040, 0x10a0, 0x1110, 0x1208, 0x2404, 0x5000,                                                                   // Символ: 120 0x78 lenght: 11 offset: 1029
0x8000, 0x9404, 0x120c, 0x11f4, 0x2004, 0x1008, 0x13f0, 0x1000,                                                                                           // Символ: 121 0x79 lenght: 8 offset: 1040
0x8000, 0x17fc, 0x1004, 0x1008, 0x1010, 0x1020, 0x1040, 0x1080, 0x1100, 0x1200, 0x1400, 0x17fc, 0x5000,                                                   // Символ: 122 0x7a lenght: 13 offset: 1048

0x4000, 0x1018, 0x1020, 0x5040, 0x1180, 0x5040, 0x1020, 0x1018, 0x5000,                                                                                   // Символ: 123 0x7b lenght: 9 offset: 1061
0x4000, 0xf040, 0x5000,                                                                                                                                   // Символ: 124 0x7c lenght: 3 offset: 1070
0x4000, 0x1180, 0x1040, 0x5020, 0x1018, 0x5020, 0x1040, 0x1180, 0x5000,                                                                                   // Символ: 125 0x7d lenght: 9 offset: 1073
0x2000, 0x1384, 0x2444, 0x1438, 0x0, 0x2000,                                                                                                              // Символ: 126 0x7e lenght: 6 offset: 1082
0x7000, 0x1060, 0x1090, 0x1108, 0x1204, 0x7402, 0x17fe, 0x5000,                                                                                           // Символ: 127 0x7f lenght: 8 offset: 1088
//	Псевдографика
0xb000, 0x1fff, 0xc000,                                                                                                                                   // Символ: 128 0x80 lenght: 3 offset: 1096
0x40, 0x8040,                                                                                                                                             // Символ: 129 0x81 lenght: 2 offset: 1099
0xb000, 0x107f, 0xc040,                                                                                                                                   // Символ: 130 0x82 lenght: 3 offset: 1101
0xb000, 0x1fc0, 0xc040,                                                                                                                                   // Символ: 131 0x83 lenght: 3 offset: 1104
0xb040, 0x107f, 0xc000,                                                                                                                                   // Символ: 132 0x84 lenght: 3 offset: 1107
0xb040, 0x1fc0, 0xc000,                                                                                                                                   // Символ: 133 0x85 lenght: 3 offset: 1110
0xb040, 0x107f, 0xc040,                                                                                                                                   // Символ: 134 0x86 lenght: 3 offset: 1113
0xb040, 0x1fc0, 0xc040,                                                                                                                                   // Символ: 135 0x87 lenght: 3 offset: 1116
0xb000, 0x1fff, 0xc040,                                                                                                                                   // Символ: 136 0x88 lenght: 3 offset: 1119
0xb040, 0x1fff, 0xc000,                                                                                                                                   // Символ: 137 0x89 lenght: 3 offset: 1122
0xb040, 0x1fff, 0xc040,                                                                                                                                   // Символ: 138 0x8a lenght: 3 offset: 1125
0xcfff, 0xc000,                                                                                                                                           // Символ: 139 0x8b lenght: 2 offset: 1128
0xc000, 0xcfff,                                                                                                                                           // Символ: 140 0x8c lenght: 2 offset: 1130
0xfff, 0x8fff,                                                                                                                                            // Символ: 141 0x8d lenght: 2 offset: 1132
0xfc0, 0x8fc0,                                                                                                                                            // Символ: 142 0x8e lenght: 2 offset: 1134
0x3f, 0x803f,                                                                                                                                             // Символ: 143 0x8f lenght: 2 offset: 1136
0x1aaa, 0x1000, 0x1aaa, 0x1000, 0x1aaa, 0x1000, 0x1aaa, 0x1000, 0x1aaa, 0x1000, 0x1aaa, 0x1000, 0x1aaa, 0x1000, 0x1aaa, 0x1000, 0x1aaa, 0x1000, 0x1aaa, 0x1000, 0x1aaa, 0x1000, 0x1aaa, 0x1000, // Символ: 144 0x90 lenght: 24 offset: 1138
0x1aaa, 0x1555, 0x1aaa, 0x1555, 0x1aaa, 0x1555, 0x1aaa, 0x1555, 0x1aaa, 0x1555, 0x1aaa, 0x1555, 0x1aaa, 0x1555, 0x1aaa, 0x1555, 0x1aaa, 0x1555, 0x1aaa, 0x1555, 0x1aaa, 0x1555, 0x1aaa, 0x1555, // Символ: 145 0x91 lenght: 24 offset: 1162
0x1fff, 0x1aaa, 0x1fff, 0x1aaa, 0x1fff, 0x1aaa, 0x1fff, 0x1aaa, 0x1fff, 0x1aaa, 0x1fff, 0x1aaa, 0x1fff, 0x1aaa, 0x1fff, 0x1aaa, 0x1fff, 0x1aaa, 0x1fff, 0x1aaa, 0x1fff, 0x1aaa, 0x1fff, 0x1aaa, // Символ: 146 0x92 lenght: 24 offset: 1186
0x4000, 0x1038, 0x2044, 0x40, 0x1040,                                                                                                                     // Символ: 147 0x93 lenght: 5 offset: 1210
0x7000, 0xa3f8, 0x7000,                                                                                                                                   // Символ: 148 0x94 lenght: 3 offset: 1215
0x9000, 0x10f0, 0x41f8, 0x10f0, 0x9000,                                                                                                                   // Символ: 149 0x95 lenght: 5 offset: 1218
0x2000, 0x100e, 0x7008, 0x3408, 0x1208, 0x1108, 0x1088, 0x1048, 0x1028, 0x1018, 0x5000,                                                                   // Символ: 150 0x96 lenght: 11 offset: 1223
0x8000, 0x1384, 0x1444, 0x1438, 0x1000, 0x1384, 0x1444, 0x1438, 0x9000,                                                                                   // Символ: 151 0x97 lenght: 9 offset: 1234
0x4000, 0x1008, 0x1010, 0x1020, 0x1040, 0x1080, 0x1100, 0x1200, 0x1100, 0x1080, 0x1040, 0x1020, 0x1010, 0x1008, 0x1000, 0x17fc, 0x5000,                   // Символ: 152 0x98 lenght: 17 offset: 1243
0x4000, 0x1200, 0x1100, 0x1080, 0x1040, 0x1020, 0x1010, 0x1008, 0x1010, 0x1020, 0x1040, 0x1080, 0x1100, 0x1200, 0x1000, 0x17fc, 0x5000,                   // Символ: 153 0x99 lenght: 17 offset: 1260
0x0, 0x8000,                                                                                                                                              // Символ: 154 0x9a lenght: 2 offset: 1277
0x40, 0x2440, 0x1380, 0x5000,                                                                                                                             // Символ: 155 0x9b lenght: 4 offset: 1279
0x2000, 0x10f0, 0x4108, 0x10f0, 0x0,                                                                                                                      // Символ: 156 0x9c lenght: 5 offset: 1283
0x2000, 0x10f0, 0x2108, 0x1008, 0x1010, 0x1020, 0x1040, 0x1080, 0x11f8, 0xd000,                                                                           // Символ: 157 0x9d lenght: 10 offset: 1288
0xa000, 0x3040, 0xb000,                                                                                                                                   // Символ: 158 0x9e lenght: 3 offset: 1298
0x6000, 0x3040, 0x2000, 0x17fc, 0x2000, 0x3040, 0x7000,                                                                                                   // Символ: 159 0x9f lenght: 7 offset: 1301
0xa000, 0x1fff, 0x2000, 0x1fff, 0xa000,                                                                                                                   // Символ: 160 0xa0 lenght: 5 offset: 1308
0x90, 0x8090,                                                                                                                                             // Символ: 161 0xa1 lenght: 2 offset: 1313
0xa000, 0x107f, 0x2040, 0x107f, 0xa040,                                                                                                                   // Символ: 162 0xa2 lenght: 5 offset: 1315
0x4000, 0x3110, 0x1000, 0x11f0, 0x1208, 0x3404, 0x17fc, 0x3400, 0x1204, 0x11f8, 0x5000,                                                                   // Символ: 163 0xa3 lenght: 11 offset: 1320
0xb000, 0x10ff, 0xc090,                                                                                                                                   // Символ: 164 0xa4 lenght: 3 offset: 1331
0xa000, 0x10ff, 0x2080, 0x109f, 0xa090,                                                                                                                   // Символ: 165 0xa5 lenght: 5 offset: 1334
0xa000, 0x1fc0, 0x2040, 0x1fc0, 0xa040,                                                                                                                   // Символ: 166 0xa6 lenght: 5 offset: 1339
0xb000, 0x1ff0, 0xc090,                                                                                                                                   // Символ: 167 0xa7 lenght: 3 offset: 1344
0xa000, 0x1ff0, 0x2010, 0x1f90, 0xa090,                                                                                                                   // Символ: 168 0xa8 lenght: 5 offset: 1347
0xa040, 0x107f, 0x2040, 0x107f, 0xa000,                                                                                                                   // Символ: 169 0xa9 lenght: 5 offset: 1352
0xb090, 0x10ff, 0xc000,                                                                                                                                   // Символ: 170 0xaa lenght: 3 offset: 1357
0xa090, 0x109f, 0x2080, 0x10ff, 0xa000,                                                                                                                   // Символ: 171 0xab lenght: 5 offset: 1360
0xa040, 0x1fc0, 0x2040, 0x1fc0, 0xa000,                                                                                                                   // Символ: 172 0xac lenght: 5 offset: 1365
0xb090, 0x1ff0, 0xc000,                                                                                                                                   // Символ: 173 0xad lenght: 3 offset: 1370
0xa090, 0x1f90, 0x2010, 0x1ff0, 0xa000,                                                                                                                   // Символ: 174 0xae lenght: 5 offset: 1373
0xa040, 0x107f, 0x2040, 0x107f, 0xa040,                                                                                                                   // Символ: 175 0xaf lenght: 5 offset: 1378
0xb090, 0x109f, 0xc090,                                                                                                                                   // Символ: 176 0xb0 lenght: 3 offset: 1383
0xa090, 0x109f, 0x2080, 0x109f, 0xa090,                                                                                                                   // Символ: 177 0xb1 lenght: 5 offset: 1386
0xa040, 0x1fc0, 0x2040, 0x1fc0, 0xa040,                                                                                                                   // Символ: 178 0xb2 lenght: 5 offset: 1391
0x3110, 0x1000, 0x17fc, 0x6400, 0x17f0, 0x6400, 0x17fc, 0x5000,                                                                                           // Символ: 179 0xb3 lenght: 8 offset: 1396
0xb090, 0x1f90, 0xc090,                                                                                                                                   // Символ: 180 0xb4 lenght: 3 offset: 1404
0xa090, 0x1f90, 0x2010, 0x1f90, 0xa090,                                                                                                                   // Символ: 181 0xb5 lenght: 5 offset: 1407
0xa000, 0x1fff, 0x2000, 0x1fff, 0xa040,                                                                                                                   // Символ: 182 0xb6 lenght: 5 offset: 1412
0xb000, 0x1fff, 0xc090,                                                                                                                                   // Символ: 183 0xb7 lenght: 3 offset: 1417
0xa000, 0x1fff, 0x2000, 0x1f9f, 0xa090,                                                                                                                   // Символ: 184 0xb8 lenght: 5 offset: 1420
0xa040, 0x1fff, 0x2000, 0x1fff, 0xa000,                                                                                                                   // Символ: 185 0xb9 lenght: 5 offset: 1425
0xb090, 0x1fff, 0xc000,                                                                                                                                   // Символ: 186 0xba lenght: 3 offset: 1430
0xa090, 0x1f9f, 0x2000, 0x1fff, 0xa000,                                                                                                                   // Символ: 187 0xbb lenght: 5 offset: 1433
0xa040, 0x1fff, 0x2040, 0x1fff, 0xa040,                                                                                                                   // Символ: 188 0xbc lenght: 5 offset: 1438
0xb090, 0x1fff, 0xc090,                                                                                                                                   // Символ: 189 0xbd lenght: 3 offset: 1443
0xa090, 0x1f9f, 0x2000, 0x1f9f, 0xa090,                                                                                                                   // Символ: 190 0xbe lenght: 5 offset: 1446
0x6000, 0x11f8, 0x1204, 0x14f2, 0x250a, 0x2502, 0x250a, 0x14f2, 0x1204, 0x11f8, 0x6000,                                                                   // Символ: 191 0xbf lenght: 11 offset: 1451
//  Кириллица, строчные
0x8000, 0x143c, 0x4442, 0x17c2, 0x4442, 0x143c, 0x5000,                                                                                                   // Символ: 192 0xc0 lenght: 7 offset: 1462
0x8000, 0x13f0, 0x1008, 0x2004, 0x11fc, 0x1204, 0x3404, 0x1204, 0x11fc, 0x5000,                                                                           // Символ: 193 0xc1 lenght: 10 offset: 1469
0x4000, 0x11f8, 0x1200, 0x3400, 0x17f0, 0x1408, 0x6404, 0x1408, 0x17f0, 0x5000,                                                                           // Символ: 194 0xc2 lenght: 10 offset: 1479
0x8000, 0x9404, 0x1204, 0x11fe, 0x3002, 0x2000,                                                                                                           // Символ: 195 0xc3 lenght: 6 offset: 1489
0x8000, 0x11fc, 0x1204, 0x7404, 0x120c, 0x11f4, 0x2004, 0x1008, 0x13f0, 0x1000,                                                                           // Символ: 196 0xc4 lenght: 10 offset: 1495
0x8000, 0x11f0, 0x1208, 0x3404, 0x17fc, 0x3400, 0x1204, 0x11f8, 0x5000,                                                                                   // Символ: 197 0xc5 lenght: 9 offset: 1505
0x6000, 0x2040, 0x11f0, 0x1248, 0x7444, 0x1248, 0x11f0, 0x2040, 0x3000,                                                                                   // Символ: 198 0xc6 lenght: 9 offset: 1514
0x8000, 0x17fc, 0xa400, 0x5000,                                                                                                                           // Символ: 199 0xc7 lenght: 4 offset: 1523
0x8000, 0x2404, 0x1208, 0x1110, 0x10a0, 0x1040, 0x10a0, 0x1110, 0x1208, 0x2404, 0x5000,                                                                   // Символ: 200 0xc8 lenght: 11 offset: 1527
0x8000, 0x9404, 0x1204, 0x11fc, 0x5000,                                                                                                                   // Символ: 201 0xc9 lenght: 5 offset: 1538
0x4000, 0x2208, 0x11f0, 0x1000, 0x9404, 0x1204, 0x11fc, 0x5000,                                                                                           // Символ: 202 0xca lenght: 8 offset: 1543
0x8000, 0x1204, 0x1208, 0x1210, 0x1220, 0x1240, 0x1380, 0x1240, 0x1220, 0x1210, 0x1208, 0x1204, 0x5000,                                                   // Символ: 203 0xcb lenght: 13 offset: 1551
0x8000, 0x10fc, 0x1104, 0x8204, 0x1404, 0x5000,                                                                                                           // Символ: 204 0xcc lenght: 6 offset: 1564
0x8000, 0x1404, 0x160c, 0x1514, 0x14a4, 0x1444, 0x6404, 0x5000,                                                                                           // Символ: 205 0xcd lenght: 8 offset: 1570
0x8000, 0x5404, 0x17fc, 0x5404, 0x5000,                                                                                                                   // Символ: 206 0xce lenght: 5 offset: 1578
0x8000, 0x11f0, 0x1208, 0x7404, 0x1208, 0x11f0, 0x5000,                                                                                                   // Символ: 207 0xcf lenght: 7 offset: 1583
0x8000, 0x17fc, 0xa404, 0x5000,                                                                                                                           // Символ: 208 0xd0 lenght: 4 offset: 1590
0x8000, 0x13fc, 0x4404, 0x13fc, 0x1024, 0x1044, 0x1084, 0x1104, 0x1204, 0x5000,                                                                           // Символ: 209 0xd1 lenght: 10 offset: 1594
0x8000, 0x17f0, 0x1408, 0x7404, 0x1408, 0x17f0, 0x4400, 0x1000,                                                                                           // Символ: 210 0xd2 lenght: 8 offset: 1604
0x8000, 0x11f0, 0x1208, 0x1404, 0x5400, 0x1404, 0x1208, 0x11f0, 0x5000,                                                                                   // Символ: 211 0xd3 lenght: 9 offset: 1612
0x8000, 0x17fc, 0xa040, 0x5000,                                                                                                                           // Символ: 212 0xd4 lenght: 4 offset: 1621
0x8000, 0x9404, 0x120c, 0x11f4, 0x2004, 0x1008, 0x13f0, 0x1000,                                                                                           // Символ: 213 0xd5 lenght: 8 offset: 1625
0x8000, 0x3444, 0x1248, 0x1150, 0x10e0, 0x1150, 0x1248, 0x3444, 0x5000,                                                                                   // Символ: 214 0xd6 lenght: 9 offset: 1633
0x4000, 0x13e0, 0x1410, 0x3408, 0x1410, 0x17f0, 0x1408, 0x5404, 0x1408, 0x17f0, 0x5000,                                                                   // Символ: 215 0xd7 lenght: 11 offset: 1642
0x8000, 0x3200, 0x13f0, 0x1208, 0x4204, 0x1208, 0x13f0, 0x5000,                                                                                           // Символ: 216 0xd8 lenght: 8 offset: 1653
0x8000, 0x3404, 0x1784, 0x1444, 0x4424, 0x1444, 0x1784, 0x5000,                                                                                           // Символ: 217 0xd9 lenght: 8 offset: 1661
0x8000, 0x13f8, 0x1404, 0x3004, 0x10f8, 0x3004, 0x1404, 0x13f8, 0x5000,                                                                                   // Символ: 218 0xda lenght: 9 offset: 1669
0x8000, 0x9444, 0x1244, 0x11fc, 0x5000,                                                                                                                   // Символ: 219 0xdb lenght: 5 offset: 1678
0x8000, 0x11f0, 0x1208, 0x1404, 0x2004, 0x10fc, 0x2004, 0x1404, 0x1208, 0x11f0, 0x5000,                                                                   // Символ: 220 0xdc lenght: 11 offset: 1683
0x8000, 0x9444, 0x1244, 0x11fe, 0x3002, 0x2000,                                                                                                           // Символ: 221 0xdd lenght: 6 offset: 1694
0x8000, 0x4404, 0x1204, 0x11fc, 0x5004, 0x5000,                                                                                                           // Символ: 222 0xde lenght: 6 offset: 1700
0x8000, 0x1e00, 0x2200, 0x13f0, 0x1208, 0x4204, 0x1208, 0x13f0, 0x5000,                                                                                   // Символ: 223 0xdf lenght: 9 offset: 1706
//  Кириллица, прописные
0x4000, 0x143c, 0x6442, 0x17c2, 0x6442, 0x143c, 0x5000,                                                                                                   // Символ: 224 0xe0 lenght: 7 offset: 1715
0x4000, 0x11f0, 0x1208, 0x6404, 0x17fc, 0x6404, 0x5000,                                                                                                   // Символ: 225 0xe1 lenght: 7 offset: 1722
0x4000, 0x17f8, 0x5400, 0x17f0, 0x1408, 0x5404, 0x1408, 0x17f0, 0x5000,                                                                                   // Символ: 226 0xe2 lenght: 9 offset: 1729
0x4000, 0xd404, 0x1204, 0x11fe, 0x3002, 0x2000,                                                                                                           // Символ: 227 0xe3 lenght: 6 offset: 1738
0x4000, 0x107c, 0x1084, 0xb104, 0x1204, 0x17fe, 0x3402, 0x2000,                                                                                           // Символ: 228 0xe4 lenght: 8 offset: 1744
0x4000, 0x17fc, 0x6400, 0x17f0, 0x6400, 0x17fc, 0x5000,                                                                                                   // Символ: 229 0xe5 lenght: 7 offset: 1752
0x3000, 0x1040, 0x11f0, 0x1248, 0xb444, 0x1248, 0x11f0, 0x1040, 0x4000,                                                                                   // Символ: 230 0xe6 lenght: 9 offset: 1759
0x4000, 0x17fc, 0xe400, 0x5000,                                                                                                                           // Символ: 231 0xe7 lenght: 4 offset: 1768
0x4000, 0x2404, 0x2208, 0x2110, 0x10a0, 0x1040, 0x10a0, 0x2110, 0x2208, 0x2404, 0x5000,                                                                   // Символ: 232 0xe8 lenght: 11 offset: 1772
0x4000, 0x4404, 0x140c, 0x1414, 0x1424, 0x1444, 0x1484, 0x1504, 0x1604, 0x4404, 0x5000,                                                                   // Символ: 233 0xe9 lenght: 11 offset: 1783
0x2208, 0x11f0, 0x1000, 0x4404, 0x140c, 0x1414, 0x1424, 0x1444, 0x1484, 0x1504, 0x1604, 0x4404, 0x5000,                                                   // Символ: 234 0xea lenght: 13 offset: 1794
0x4000, 0x1404, 0x1408, 0x1410, 0x1420, 0x1440, 0x1480, 0x1500, 0x1600, 0x1500, 0x1480, 0x1440, 0x1420, 0x1410, 0x1408, 0x1404, 0x5000,                   // Символ: 235 0xeb lenght: 17 offset: 1807
0x4000, 0x10fc, 0x1104, 0xb204, 0x1404, 0x1804, 0x5000,                                                                                                   // Символ: 236 0xec lenght: 7 offset: 1824
0x4000, 0x1402, 0x1606, 0x250a, 0x1492, 0x2462, 0x8402, 0x5000,                                                                                           // Символ: 237 0xed lenght: 8 offset: 1831
0x4000, 0x7404, 0x17fc, 0x7404, 0x5000,                                                                                                                   // Символ: 238 0xee lenght: 5 offset: 1839
0x4000, 0x11f0, 0x1208, 0xb404, 0x1208, 0x11f0, 0x5000,                                                                                                   // Символ: 239 0xef lenght: 7 offset: 1844
0x4000, 0x17fc, 0xe404, 0x5000,                                                                                                                           // Символ: 240 0xf0 lenght: 4 offset: 1851
0x4000, 0x11fc, 0x1204, 0x4404, 0x1204, 0x11fc, 0x1014, 0x1024, 0x1044, 0x1084, 0x1104, 0x1204, 0x1404, 0x5000,                                           // Символ: 241 0xf1 lenght: 14 offset: 1855
0x4000, 0x17f0, 0x1408, 0x4404, 0x1408, 0x17f0, 0x7400, 0x5000,                                                                                           // Символ: 242 0xf2 lenght: 8 offset: 1869
0x4000, 0x11f0, 0x1208, 0x2404, 0x7400, 0x2404, 0x1208, 0x11f0, 0x5000,                                                                                   // Символ: 243 0xf3 lenght: 9 offset: 1877
0x4000, 0x17fc, 0xe040, 0x5000,                                                                                                                           // Символ: 244 0xf4 lenght: 4 offset: 1886
0x4000, 0x7404, 0x1204, 0x11fc, 0x4004, 0x1008, 0x13f0, 0x5000,                                                                                           // Символ: 245 0xf5 lenght: 8 offset: 1890
0x4000, 0x5444, 0x1248, 0x1150, 0x10e0, 0x1150, 0x1248, 0x5444, 0x5000,                                                                                   // Символ: 246 0xf6 lenght: 9 offset: 1898
0x4000, 0x17f0, 0x1408, 0x3404, 0x1408, 0x17f0, 0x1408, 0x5404, 0x1408, 0x17f0, 0x5000,                                                                   // Символ: 247 0xf7 lenght: 11 offset: 1907
0x4000, 0x5400, 0x17f0, 0x1408, 0x6404, 0x1408, 0x17f0, 0x5000,                                                                                           // Символ: 248 0xf8 lenght: 8 offset: 1918
0x4000, 0x5402, 0x17c2, 0x1422, 0x6412, 0x1422, 0x17c2, 0x5000,                                                                                           // Символ: 249 0xf9 lenght: 8 offset: 1926
0x4000, 0x11f0, 0x1208, 0x1404, 0x3004, 0x1008, 0x10f0, 0x1008, 0x3004, 0x1404, 0x1208, 0x11f0, 0x5000,                                                   // Символ: 250 0xfa lenght: 13 offset: 1934
0x4000, 0xd444, 0x1244, 0x11fc, 0x5000,                                                                                                                   // Символ: 251 0xfb lenght: 5 offset: 1947
0x4000, 0x11f0, 0x1208, 0x1404, 0x4004, 0x10fc, 0x4004, 0x1404, 0x1208, 0x11f0, 0x5000,                                                                   // Символ: 252 0xfc lenght: 11 offset: 1952
0x4000, 0xd444, 0x1244, 0x11fe, 0x3002, 0x2000,                                                                                                           // Символ: 253 0xfd lenght: 6 offset: 1963
0x4000, 0x6404, 0x1204, 0x11fc, 0x7004, 0x5000,                                                                                                           // Символ: 254 0xfe lenght: 6 offset: 1969
0x4000, 0x2c00, 0x3400, 0x17f0, 0x1408, 0x6404, 0x1408, 0x17f0, 0x5000,                                                                                   // Символ: 255 0xff lenght: 9 offset: 1975
};

// offset table
static const uint16_t TERMINUS_K12x24N_ID[256]= {
0x0, 0x5, 0x11, 0x1d, 0x27, 0x34, 0x3f, 0x49, 0x4e, 0x53, 0x58, 0x5d, 0x6a, 0x74, 0x7b, 0x83, 
0x92, 0x9f, 0xac, 0xb9, 0xbe, 0xc4, 0xd1, 0xd4, 0xe2, 0xea, 0xf2, 0xfd, 0x108, 0x10c, 0x117, 0x11f, 
0x127, 0x129, 0x12e, 0x132, 0x139, 0x148, 0x155, 0x161, 0x165, 0x16e, 0x177, 0x182, 0x187, 0x18b, 0x18e, 0x191, 
0x19a, 0x1a9, 0x1b1, 0x1c0, 0x1cd, 0x1da, 0x1e4, 0x1ee, 0x1f7, 0x202, 0x20c, 0x211, 0x217, 0x228, 0x22d, 0x23e, 
0x249, 0x256, 0x25d, 0x268, 0x271, 0x278, 0x27f, 0x285, 0x28f, 0x294, 0x299, 0x2a0, 0x2b1, 0x2b5, 0x2bd, 0x2c8, 
0x2cf, 0x2d7, 0x2e1, 0x2ef, 0x2fc, 0x300, 0x305, 0x30c, 0x314, 0x31f, 0x326, 0x333, 0x338, 0x341, 0x346, 0x34e, 
0x352, 0x358, 0x362, 0x36a, 0x373, 0x37b, 0x384, 0x38a, 0x394, 0x39a, 0x3a1, 0x3aa, 0x3b8, 0x3bd, 0x3c2, 0x3c7, 
0x3ce, 0x3d6, 0x3de, 0x3e5, 0x3ee, 0x3f4, 0x3f9, 0x400, 0x405, 0x410, 0x418, 0x425, 0x42e, 0x431, 0x43a, 0x440, 
0x448, 0x44b, 0x44d, 0x450, 0x453, 0x456, 0x459, 0x45c, 0x45f, 0x462, 0x465, 0x468, 0x46a, 0x46c, 0x46e, 0x470, 
0x472, 0x48a, 0x4a2, 0x4ba, 0x4bf, 0x4c2, 0x4c7, 0x4d2, 0x4db, 0x4ec, 0x4fd, 0x4ff, 0x503, 0x508, 0x512, 0x515, 
0x51c, 0x521, 0x523, 0x528, 0x533, 0x536, 0x53b, 0x540, 0x543, 0x548, 0x54d, 0x550, 0x555, 0x55a, 0x55d, 0x562, 
0x567, 0x56a, 0x56f, 0x574, 0x57c, 0x57f, 0x584, 0x589, 0x58c, 0x591, 0x596, 0x599, 0x59e, 0x5a3, 0x5a6, 0x5ab, 
0x5b6, 0x5bd, 0x5c7, 0x5d1, 0x5d7, 0x5e1, 0x5ea, 0x5f3, 0x5f7, 0x602, 0x607, 0x60f, 0x61c, 0x622, 0x62a, 0x62f, 
0x636, 0x63a, 0x644, 0x64c, 0x655, 0x659, 0x661, 0x66a, 0x675, 0x67d, 0x685, 0x68e, 0x693, 0x69e, 0x6a4, 0x6aa, 
0x6b3, 0x6ba, 0x6c1, 0x6ca, 0x6d0, 0x6d8, 0x6df, 0x6e8, 0x6ec, 0x6f7, 0x702, 0x70f, 0x720, 0x727, 0x72f, 0x734, 
0x73b, 0x73f, 0x74d, 0x755, 0x75e, 0x762, 0x76a, 0x773, 0x77e, 0x786, 0x78e, 0x79b, 0x7a0, 0x7ab, 0x7b1, 0x7b7, 
};

#endif  //  __TERMINUS_K12x24N_H__
