#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"

void  dummy_loop(uint32_t count){

  while(--count);
}

int main()
{
    // enable GPIOC port
    RCC->APB2ENR |= RCC_APB2Periph_GPIOC;
    // --- GPIO setup ----
    GPIOC->CRH &= ~(uint32_t)(0xf<<20);
    GPIOC->CRH |=  (uint32_t)(0x2<<20);

    for(;;){
        GPIOC->BSRR=GPIO_Pin_13;
        dummy_loop(600000);
        GPIOC->BRR=GPIO_Pin_13;
        dummy_loop(600000);
    }

}
