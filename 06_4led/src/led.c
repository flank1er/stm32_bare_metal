 #include "led.h"

const char digit[10] = {
      0b11000000, // 0
      0b11111001, // 1
      0b10100100, // 2
      0b10110000, // 3
      0b10011001, // 4
      0b10010010, // 5
      0b10000010, // 6
      0b11111000, // 7
      0b10000000, // 8
      0b10010000, // 9
};

void spi_transmit(uint8_t data) {
    for (char i=0; i<8; i++)
    {
		if (data & 0x80)
			GPIOA->BSRR=DIO;
		else
			GPIOA->BRR=DIO;

        data=(data<<1);
        GPIOA->BSRR = SCLK;
        GPIOA->BRR  = SCLK;
    }
}

void to_led(uint8_t value, uint8_t reg) {
	GPIOA->BRR = RCLK;
	spi_transmit(digit[value]);
	spi_transmit(reg);
	GPIOA->BSRR  = RCLK;
}

void show_led() {
    switch (reg) {
    case 0:
        to_led((uint8_t)(led%10),1);
        break;
    case 1:
        if (led>=10)
            to_led((uint8_t)((led%100)/10),2);
        break;
    case 2:
        if (led>=100)
            to_led((uint8_t)((led%1000)/100),4);
        break;
    case 3:
        if (led>=1000)
            to_led((uint8_t)(led/1000),8);
        break;
     }
    reg = (reg ==  3) ? 0 : reg+1;
}
