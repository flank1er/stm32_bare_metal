#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "uart.h"
#include "task.h"
#include "led.h"

extern void delay(uint32_t ms);
void toggle_led();

int main()
{
    // enable GPIOC port
    RCC->APB2ENR |= RCC_APB2Periph_GPIOC;			// enable PORT_C
    RCC->APB2ENR |= RCC_APB2Periph_GPIOA;			// enable PORT_A
	RCC->APB2ENR |= RCC_APB2Periph_USART1; 			// enable UART1
    // --- GPIO setup ----
    GPIOC->CRH &= ~(uint32_t)(0xf<<20);
    GPIOC->CRH |=  (uint32_t)(0x2<<20);
	// Setup PA9
	GPIOA->CRH &= ~(uint32_t)(0xf<<4);
	GPIOA->CRH |=  (uint32_t)(0xa<<4);
	// Setup PA4, PA5, PA7
	GPIOA->CRL &= ~(uint32_t)(0xf<<16);				// clear mode for PA4
	GPIOA->CRL &= ~(uint32_t)(0xf<<20);				// clear mode for PA5
	GPIOA->CRL &= ~(uint32_t)(0xf<<28);				// clear mode for PA7
	GPIOA->CRL |= (uint32_t)(0x3<<16);				// for PA4 set  PushPull mode 50MHz
	GPIOA->CRL |= (uint32_t)(0x3<<20);				// for PA5 set  PushPull mode 50MHz
	GPIOA->CRL |= (uint32_t)(0x3<<28);				// for PA7 set  PushPull mode 50MHz
	// --- UART setup ----
    //USART1->BRR  = 0x1d4c;								// 9600 Baud, when APB2=72MHz
	USART1->BRR = 0x271;								// 115200 Baud, when APB2=72MHz
	USART1->CR1 |= USART_CR1_UE_Set | USART_Mode_Tx;	// enable USART1, enable  TX mode
	// Let's go..
	reg=0; led=0;
	clear_task();
	add_task(show_led,TOP,LOOP, 5);
	add_task(toggle_led,TOP,LOOP,1000);
    if (SysTick_Config(72000)) // set 1ms
    {
        while(1); // error
    }
	__enable_irq();
    for(;;){
		asm("wfi");
	}
}

void toggle_led() {
	GPIOC->ODR ^= GPIO_Pin_13;
	led++;
	usart1_print_number(get_load_cpu());
	usart1_send_char('\n');
}
