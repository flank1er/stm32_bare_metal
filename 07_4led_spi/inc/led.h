#ifndef __LED_H__
#define __LED_H__

#include "stm32f10x.h"
#include "stm32f10x_gpio.h"

#define SCLK GPIO_Pin_5
#define RCLK GPIO_Pin_4
#define DIO  GPIO_Pin_7

uint8_t reg;
__IO uint32_t led;

void show_led();

#endif
