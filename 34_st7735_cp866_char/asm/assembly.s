.syntax unified
.cpu cortex-m3
.fpu softvfp
.thumb

.type delay_ms, %function
.global delay_ms
delay_ms:
    push {r1}
l1: mov.w r1,#10277
lp: subs r1,#1
    bne lp
    subs r0,#1
    bne l1
    pop {r1}
    bx lr
