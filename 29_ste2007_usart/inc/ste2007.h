#ifndef __PCD8544_H__
#define __PCD8544_H__
#include <stdint.h>
#include <stdbool.h>

/// alias
#define  ste2007_clear() ste2007_fill(0x0)

// functions
void ste2007_init();
void ste2007_fill_fb(uint8_t value);
void ste2007_display_fb();
void pcd8544_off();

#endif      // __PCD8544_H__
