#include "main.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_spi.h"
#include "ste2007.h"
#include "uart.h"

// Port B
#define RST  GPIO_Pin_4
//#define BL   GPIO_Pin_5
//#define DC   GPIO_Pin_6
// Port A
#define CS   GPIO_Pin_5
#define CLK  GPIO_Pin_4
#define DIN  GPIO_Pin_2

#define LCD_C     0x00
#define LCD_D     0x01

#define LCD_X     84
#define LCD_Y     48
//#define LCD_LEN   (uint16_t)((LCD_X * LCD_Y) / 8)
#define LCD_LEN 864

#define chip_select_enable() gpio_reset(GPIOA,CS)
#define chip_select_disable() gpio_set(GPIOA,CS)

#define FONT_SIZE_1 (uint8_t)0x01
#define FONT_SIZE_2 (uint8_t)0x02
#define FONT_SIZE_3 (uint8_t)0x03

static const uint8_t lookup[16] = {
0x0, 0x8, 0x4, 0xc, 0x2, 0xa, 0x6, 0xe,
0x1, 0x9, 0x5, 0xd, 0x3, 0xb, 0x7, 0xf, };

static uint8_t fb[LCD_LEN];          // screen buffer

bool isPower=false;

void ste2007_send(uint8_t dc, uint8_t data);

void ste2007_init() {
    // hardware reset
    gpio_reset(GPIOB,RST);
    gpio_set(GPIOB,RST);
    // init routine
    chip_select_enable();
	ste2007_send(LCD_C, 0xE2);  // reset
	delay_ms(10);
    ste2007_send(LCD_C, 0xA4);  // power save off
    ste2007_send(LCD_C, 0x2F);  // power control set
    ste2007_send(LCD_C, 0xB0);  // set page address
    ste2007_send(LCD_C, 0x10);  // set col=0 upper 3 bits
    ste2007_send(LCD_C, 0x00);  // set col=0 lower 4 bits
    ste2007_send(LCD_C, 0x40);  // set row 0
    ste2007_send(LCD_C, 0xAF);  // dispaly on
    chip_select_disable();
	delay_ms(50);
//    gpio_reset(GPIOB,BL);           // enable blacklight
    isPower=true;
    ste2007_fill_fb(0xAA);
    ste2007_display_fb();
}

void ste2007_send(uint8_t dc, uint8_t data)
{
#ifdef HW_SPI
    uint8_t a=(lookup[data&0b1111] << 4) | lookup[data>>4];
    uint16_t d=(uint16_t)a;
    d=d<<1;
    if (dc == 0x01)
        d|=(uint16_t)0x1;
    chip_select_enable();
    while(!(USART2->SR & USART_FLAG_TXE)) {
        __asm__ volatile ("nop\n");
    }

    USART2->DR=d;

    while(!(USART2->SR & USART_FLAG_TC)) {
        __asm__ volatile ("nop\n");
    }
    chip_select_disable();
#else
    // 9-th bit
    if (dc == LCD_D)
        gpio_set(GPIOA,DIN);
    else
        gpio_reset(GPIOA,DIN);

    // Clock Signal
    gpio_set(GPIOA,CLK);
    gpio_reset(GPIOA,CLK);
    // send data bits
   for (uint8_t i=0; i<8; i++)
   {
      if (data & 0x80)
          gpio_set(GPIOA,DIN);
      else
          gpio_reset(GPIOA,DIN);

      data=(data<<1);
      // Set Clock Signal
      gpio_set(GPIOA,CLK);
      gpio_reset(GPIOA,CLK);
   }
#endif
}

//  work with buffer
void ste2007_fill_fb(uint8_t value)
{
    for(int i=0;i<LCD_LEN; i++)
        fb[i]=value;
}

void ste2007_display_fb() {
    chip_select_enable();
    for(uint16_t i=0;i<LCD_LEN; i++)
        ste2007_send(LCD_D,fb[i]);

    ste2007_send(LCD_C, 0xB0);  // set page address
    ste2007_send(LCD_C, 0x10);  // set col=0 upper 3 bits
    ste2007_send(LCD_C, 0x00);  // set col=0 lower 4 bits
    ste2007_send(LCD_C, 0x40);  // set row 0
 
    chip_select_disable();
}


void pcd8544_off() {
//    pcd8544_fill(0);
    // hardware reset
    gpio_reset(GPIOB,RST);
    delay_ms(10);
    gpio_set(GPIOB,RST);
    delay_ms(10);
    // blacklight off
//    gpio_set(GPIOB,BL);
    isPower=false;
}
