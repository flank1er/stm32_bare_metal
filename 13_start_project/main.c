#include "main.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_usart.h"
#include "stm32f10x_rcc.h"
#include "uart.h"

int main()
{
    // enable GPIOC port
    RCC->APB2ENR |= RCC_APB2Periph_GPIOC;           // enable PORT_C
    RCC->APB2ENR |= RCC_APB2Periph_GPIOA;           // enable PORT_A
    RCC->APB2ENR |= RCC_APB2Periph_USART1;          // enable UART1
    // --- GPIO setup ----
    GPIOC->CRH &= ~(uint32_t)(0xf<<20);             // Reset for PC13 (LED)
    GPIOC->CRH |=  (uint32_t)(0x2<<20);             // Push-Pull 2-MHz fo PC13 (LED)
	gpio_set(GPIOC,LED);
    GPIOA->CRH &= ~(uint32_t)(0xf<<4);              // enable Alterentive mode
    GPIOA->CRH |=  (uint32_t)(0xa<<4);              // for PA9 = USART1_TX
    // ------- SysTick CONFIG --------------
    if (SysTick_Config(72000)) // set 1ms
    {
        while(1); // error
    }
    // --- UART setup ----
    //USART1->BRR  = 0x1d4c;                                // 9600 Baud, when APB2=72MHz
    USART1->BRR = 0x271;                                // 115200 Baud, when APB2=72MHz
    USART1->CR1 |= (USART_CR1_UE_Set | USART_Mode_Tx);  // enable USART1, enable  TX mode
    //////////////////////////////////////
    /// let's go.....
    /////////////////////////////////////
    uint32_t tick=0;
	println("ready...");
    for(;;){
		gpio_set(GPIOC,LED);
        delay_ms(1000);
		gpio_reset(GPIOC,LED);
        delay_ms(1000);
		print("tick: ");
        println_num(tick++);
    }
}
