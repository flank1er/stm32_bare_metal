#include <stdint.h>
#include "main.h"

static __IO uint32_t s_timer;

void SysTick_Handler(void)
{
        if (s_timer)
            s_timer--;
}

void systick_delay(__IO uint32_t val) {
    s_timer=val;
    SysTick->LOAD |= SysTick_CTRL_ENABLE;
    while(s_timer) {
         __asm__ volatile ("wfi\n");
    };
    SysTick->LOAD &= ~(SysTick_CTRL_ENABLE_Msk);    // disable SysTick
}

