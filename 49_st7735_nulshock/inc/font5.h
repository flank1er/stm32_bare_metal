#ifndef __FONT5_H__
#define __FONT5_H__
#include "main.h"

#define NULSHOCK   0x0
#define DUNERISE    0x1
#define BLADERUNNER 0x2

void font5_print_str_from_char(char *str, uint8_t x, uint8_t y, uint16_t fg_color, uint16_t bg_color,  uint8_t font, uint8_t scale);
void font5_send_char(uint8_t x, uint8_t y, uint8_t ch, uint8_t font, uint16_t fg_color, uint16_t bg_color, uint8_t scale);

#endif      // __FONT5_H__
