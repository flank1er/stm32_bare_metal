#ifndef __FONT3_H__
#define __FONT3_H__
#include "main.h"

#define DS_DIGITAL   0x0
#define PIXEL_LCD    0x1

void font3_send_char(uint8_t x, uint8_t y, uint8_t ch, uint8_t font, uint16_t fg_color, uint16_t bg_color, uint8_t scale);
void font3_print_str_from_char(char *str, uint8_t x, uint8_t y, uint16_t fg_color, uint16_t bg_color,  uint8_t font, uint8_t scale);
void font3_print_num(uint32_t num, uint8_t x, uint8_t y, uint16_t fg, uint16_t bg, uint8_t font, uint8_t scale);

#endif // __FONT3_H__
