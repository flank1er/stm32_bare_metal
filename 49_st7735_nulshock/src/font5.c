#include "font5.h"
#include "nulshock.h"
#include "dunerise.h"
#include "bladerunner.h"
#include "st7735.h"
//#include "uart.h"

#define CHAR_LEN 1280                 // 1280=(40 x 32)  i.e. width * height
#define numlen 5

static volatile uint16_t tab_buf[32];
static volatile uint8_t sym_buf[32*5];
extern uint16_t buf[];

void font5_decompress_sym(uint8_t sym,  uint8_t font);

void font5_char_to_buf(uint8_t x_pos, uint8_t ch, uint8_t len, uint8_t font, uint16_t fg_color, uint16_t bg_color, uint16_t* buffer);
void font5_char_to_buf_2X(uint8_t x_pos, uint8_t ch, uint8_t len, uint8_t font, uint16_t fg_color, uint16_t bg_color, uint16_t* buffer);

void font5_print_str_from_char(char *str, uint8_t x, uint8_t y, uint16_t fg_color, uint16_t bg_color,  uint8_t font, uint8_t scale) {
    uint8_t i=0;
    while (*str) {
        font5_send_char(x + scale*i*(DUNERISE_36x32_CHAR_WIDTH-5),y,*str++, font, fg_color,bg_color, scale);
        ++i;
    }
}

void font5_send_char(uint8_t x, uint8_t y, uint8_t ch, uint8_t font, uint16_t fg_color, uint16_t bg_color, uint8_t scale) {

    if (scale == 1)
        font5_char_to_buf(0,ch, 1, font, fg_color,bg_color,buf);
    else
        font5_char_to_buf_2X(0,ch, 1, font, fg_color,bg_color,buf);

    chip_select_enable();
    // 16x32 area
    st7735_send(LCD_C,ST77XX_CASET);
    st7735_send(LCD_D,0);
    st7735_send(LCD_D,x);
    st7735_send(LCD_D,0);
    st7735_send(LCD_D,x+((NULSHOCK_40x32_CHAR_WIDTH*scale)-1));

    st7735_send(LCD_C,ST77XX_RASET);
    st7735_send(LCD_D,0);
    st7735_send(LCD_D,y);
    st7735_send(LCD_D,0);
    st7735_send(LCD_D,y+((NULSHOCK_40x32_CHAR_HEIGHT*scale)-1));

    st7735_send(LCD_C,ST77XX_RAMWR);
#ifdef HW_SPI

    gpio_set(GPIOB,DC);
    SPI1->CR1 &= CR1_SPE_Reset;      // disable SPI for setup
    SPI1->CR1  = (SPI_Mode_Master|SPI_DataSize_16b|SPI_NSS_Soft|SPI_BaudRatePrescaler_2);
    SPI1->CR1 |= CR1_SPE_Set;        // enable SPI
    for (uint16_t j=0;j<(CHAR_LEN*scale *scale);j++) {
        uint16_t color;

        color=buf[j];
        while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
        SPI1->DR=color;
        //SPI1->DR=fg_color;
    }

    while (!(SPI1->SR & SPI_I2S_FLAG_TXE) || (SPI1->SR & SPI_I2S_FLAG_BSY));
    chip_select_disable();

    SPI1->CR1 &= CR1_SPE_Reset;      // disable SPI for setup
    SPI1->CR1  = (SPI_Mode_Master|SPI_DataSize_8b|SPI_NSS_Soft|SPI_BaudRatePrescaler_2);
    SPI1->CR1 |= CR1_SPE_Set;        // enable SPI
#else
   chip_select_disable();
#endif

}


void font5_char_to_buf_2X(uint8_t x_pos, uint8_t ch, uint8_t len, uint8_t font, uint16_t fg_color, uint16_t bg_color, uint16_t* buffer) {
    if ((x_pos + 80) > LCD_X)
        return;

    font5_decompress_sym(ch,font);

    for (uint8_t i=0; i < 32; i++) {
        uint16_t index= x_pos + (i * len * NULSHOCK_40x32_CHAR_WIDTH * 4);

        for (uint8_t k=0; k < 5; k++) {
            uint8_t data=sym_buf[i*5+k];


            for (uint8_t j=0; j<8;j++) {
                uint16_t color = (data & 0x80) ? fg_color : bg_color;
                buffer[index+(NULSHOCK_40x32_CHAR_WIDTH *2)]=color;
                buffer[index+(NULSHOCK_40x32_CHAR_WIDTH*2+1)]=color;
                buffer[index++]=color;
                buffer[index++]=color;
                data = data << 1;
            }
        }
    }
}

void font5_char_to_buf(uint8_t x_pos, uint8_t ch, uint8_t len, uint8_t font, uint16_t fg_color, uint16_t bg_color, uint16_t* buffer) {
    if ((x_pos + 40) > LCD_X)
        return;

    font5_decompress_sym(ch,font);


    for (uint8_t i=0; i < 32; i++) {
        uint16_t index= x_pos + (i*len* NULSHOCK_40x32_CHAR_WIDTH);

        for (uint8_t k=0; k < 5; k++) {
            uint8_t data=sym_buf[i*5+k];


            for (uint8_t j=0; j<8;j++) {

                buffer[index++]=(data & 0x80) ? fg_color : bg_color;
                data = data << 1;
            }
        }
    }
}


void font5_decompress_sym(uint8_t sym,  uint8_t font) {
    sym -= 0x20;
    if (sym > 0x64)
        return;

    uint16_t idx;
    switch (font) {
    case BLADERUNNER:
        idx = BLADERUNNER_ID[sym];
        break;
    case DUNERISE:
        idx = DUNERISE_ID[sym];
        break;
    case NULSHOCK:
    default:
        idx = NULSHOCK_ID[sym];
        break;
    }

    //uint16_t idx =  (!font) ? DS_DIGITS_22x32_ID[sym] : PIXEL_LCD_ID[sym] ;
    //uint16_t idx = NULSHOCK_ID[sym];
    uint8_t i=0;
    //uart_print_hex(idx);
    while (i<32) {
        //uint8_t index = (!font) ? DS_DIGITS_22x32[idx++] : PIXEL_LCD_23x32[idx++];
        uint8_t index; /* = NULSHOCK_40x32[idx++]; */
        switch (font) {
        case BLADERUNNER:
            index = BLADERUNNER_38x32[idx++];
            break;
        case DUNERISE:
            index = DUNERISE_36x32[idx++];
            break;
        case NULSHOCK:
        default:
            index = NULSHOCK_40x32[idx++];
            break;
        }

        if (index == 0xf0) {
            uint16_t a;/* = NULSHOCK_40x32[idx++];*/
            switch (font) {
            case BLADERUNNER:
                a = BLADERUNNER_38x32[idx++];
                break;
            case DUNERISE:
                a = DUNERISE_36x32[idx++];
                break;
            case NULSHOCK:
            default:
                a = NULSHOCK_40x32[idx++];
                break;
            }
            a = ((uint16_t)0x100 | a);
            tab_buf[i++]=a;
        } else {
            uint8_t k = (index >> 4);
            if (k == (uint8_t)0x0f) {
                k = (index & 0x0f);
                for (uint8_t j=0; j<k; j++) {
                    //tab_buf[i++]= (!font) ? DS_DIGITS_22x32[idx++] : PIXEL_LCD_23x32[idx++];
                    //tab_buf[i++] = NULSHOCK_40x32[idx++];
                    switch (font) {
                    case BLADERUNNER:
                        tab_buf[i++] = BLADERUNNER_38x32[idx++];
                        break;
                    case DUNERISE:
                        tab_buf[i++] = DUNERISE_36x32[idx++];
                        break;
                    case NULSHOCK:
                    default:
                        tab_buf[i++] = NULSHOCK_40x32[idx++];
                        break;
                    }

                }

            } else if (k == 0x0 ) {
                k=(index & 0x0f);
                //index =(!font) ? DS_DIGITS_22x32[idx++] : PIXEL_LCD_23x32[idx++];
                //index = NULSHOCK_40x32[idx++];
                switch (font) {
                case BLADERUNNER:
                    index = BLADERUNNER_38x32[idx++];
                    break;
                case DUNERISE:
                    index = DUNERISE_36x32[idx++];
                    break;
                case NULSHOCK:
                default:
                    index = NULSHOCK_40x32[idx++];
                    break;
                }
                for (uint8_t j=0; j<k; j++) {
                    tab_buf[i++]= index;
                }
            } else {
                index = (index & 0x0f);
                for (uint8_t j=0; j<k; j++) {
                    tab_buf[i++]= index;
                }

            }
        }
    }

    switch (font) {
    case BLADERUNNER:
        for (i=0;i<32;i++) {
            uint16_t j=tab_buf[i];
            if (j < BLADERUNNER_38x32_WORDS_LENGTH) {
                j *=5;
                sym_buf[i*5]  =BLADERUNNER_DICTONARY[j];
                sym_buf[i*5+1]=BLADERUNNER_DICTONARY[j+1];
                sym_buf[i*5+2]=BLADERUNNER_DICTONARY[j+2];
                sym_buf[i*5+3]=BLADERUNNER_DICTONARY[j+3];
                sym_buf[i*5+4]=BLADERUNNER_DICTONARY[j+4];
            }
        }
        break;
    case DUNERISE:
        for (i=0;i<32;i++) {
            uint16_t j=tab_buf[i];
            if (j < DUNERISE_36x32_WORDS_LENGTH) {
                j *=5;
                sym_buf[i*5]  =DUNERISE_DICTONARY[j];
                sym_buf[i*5+1]=DUNERISE_DICTONARY[j+1];
                sym_buf[i*5+2]=DUNERISE_DICTONARY[j+2];
                sym_buf[i*5+3]=DUNERISE_DICTONARY[j+3];
                sym_buf[i*5+4]=DUNERISE_DICTONARY[j+4];
            }
        }
        break;
    case NULSHOCK:
    default:
        for (i=0;i<32;i++) {
            uint16_t j=tab_buf[i];
            if (j < NULSHOCK_40x32_WORDS_LENGTH) {
                j *=5;
                sym_buf[i*5]  =NULSHOCK_DICTONARY[j];
                sym_buf[i*5+1]=NULSHOCK_DICTONARY[j+1];
                sym_buf[i*5+2]=NULSHOCK_DICTONARY[j+2];
                sym_buf[i*5+3]=NULSHOCK_DICTONARY[j+3];
                sym_buf[i*5+4]=NULSHOCK_DICTONARY[j+4];
            }
        }
        break;
    }

}

