#include "koi8r_24.h"
#include "ter-k24n.h"
#include "ter-k24b.h"
#include "st7735.h"
#include "uart.h"

#define numlen 10
#define CHAR_LEN 288                 // 288=(12 x 24 )  i.e. width * height

static volatile uint16_t sym16_buf[(TERMINUS_K12x24N_CHAR_WIDTH*TERMINUS_K12x24N_CHAR_HEIGHT)];
extern uint16_t buf[];
extern uint16_t buf2[];

void KOI8Rx24_decompress_sym(uint8_t sym, uint8_t font);
void KOI8Rx24_char_to_buf(uint8_t x_pos, uint8_t ch, uint8_t len,  uint8_t font, uint16_t fg_color, uint16_t bg_color, uint16_t* buffer);

void KOI8Rx24_print_num(uint32_t num, uint8_t x, uint8_t y, uint16_t fg, uint16_t bg, uint8_t font){
    uint8_t n[numlen];
    uint8_t *s=n+(numlen-1);
    *s=0;           // EOL
    do {
        *(--s)=('0' + num%10);
        num = num/10;
    } while (num>0);
     KOI8Rx24_print_str_from_char((char*)s,x,y,fg,bg, font);
}

void KOI8Rx24_print_str_from_char(char *str, uint8_t x, uint8_t y, uint16_t fg_color, uint16_t bg_color,  uint8_t font) {
    uint8_t i=0;
    while (*str) {
        KOI8Rx24_send_char(x + i*TERMINUS_K12x24N_CHAR_WIDTH,y,*str++, font, fg_color,bg_color);
        ++i;
    }
}

void KOI8Rx24_send_char(uint8_t x, uint8_t y, uint8_t ch, uint8_t font, uint16_t fg_color, uint16_t bg_color) {

    KOI8Rx24_char_to_buf(x,ch, 1, font, fg_color,bg_color,buf);

    chip_select_enable();
    // 8x8 area
    st7735_send(LCD_C,ST77XX_CASET);
    st7735_send(LCD_D,0);
    st7735_send(LCD_D,x);
    st7735_send(LCD_D,0);
    st7735_send(LCD_D,x+11);

    st7735_send(LCD_C,ST77XX_RASET);
    st7735_send(LCD_D,0);
    st7735_send(LCD_D,y);
    st7735_send(LCD_D,0);
    st7735_send(LCD_D,y+23);

    st7735_send(LCD_C,ST77XX_RAMWR);
#ifdef HW_SPI

    gpio_set(GPIOB,DC);
    SPI1->CR1 &= CR1_SPE_Reset;      // disable SPI for setup
    SPI1->CR1  = (SPI_Mode_Master|SPI_DataSize_16b|SPI_NSS_Soft|SPI_BaudRatePrescaler_2);
    SPI1->CR1 |= CR1_SPE_Set;        // enable SPI
    for (uint16_t j=0;j<CHAR_LEN;j++) {
        uint16_t color;

        color=buf[j];
        while (!(SPI1->SR & SPI_I2S_FLAG_TXE));
        SPI1->DR=color;
    }

    while (!(SPI1->SR & SPI_I2S_FLAG_TXE) || (SPI1->SR & SPI_I2S_FLAG_BSY));
    chip_select_disable();

    SPI1->CR1 &= CR1_SPE_Reset;      // disable SPI for setup
    SPI1->CR1  = (SPI_Mode_Master|SPI_DataSize_8b|SPI_NSS_Soft|SPI_BaudRatePrescaler_2);
    SPI1->CR1 |= CR1_SPE_Set;        // enable SPI
#else
   chip_select_disable();
#endif

}

void KOI8Rx24_char_to_buf(uint8_t x_pos, uint8_t ch, uint8_t len, uint8_t font, uint16_t fg_color, uint16_t bg_color, uint16_t* buffer) {
    if ((x_pos + TERMINUS_K12x24N_CHAR_WIDTH) > LCD_X)
        return;

    KOI8Rx24_decompress_sym(ch, font);

    for (uint8_t i=0; i < (TERMINUS_K12x24N_CHAR_WIDTH*TERMINUS_K12x24N_CHAR_HEIGHT); i++) {
        uint16_t data=sym16_buf[i];
        data = data <<4;

        uint16_t index= (i*len*TERMINUS_K12x24N_CHAR_WIDTH);
        for (uint8_t j=0; j<TERMINUS_K12x24N_CHAR_WIDTH;j++) {

            buffer[index++]=(data & (uint16_t)0x8000) ? fg_color : bg_color;
            data = data << 1;
        }
    }
}

void KOI8Rx24_decompress_sym(uint8_t sym, uint8_t font) {
    uint16_t idx;

    idx=(!font) ? TERMINUS_K12x24N_ID[sym] : TERMINUS_K12x24B_ID[sym];
    uint8_t i=0;
    while (i<(TERMINUS_K12x24N_CHAR_WIDTH*TERMINUS_K12x24N_CHAR_HEIGHT)) {
        uint16_t s,j;
        s=(!font) ? TERMINUS_K12x24N[idx] : TERMINUS_K12x24B[idx];
        j=(s >> 12);
        s &= (uint16_t)0x0fff;

        if (j == 0) j=16;

        for (uint8_t k=0;k<j;k++)
            sym16_buf[i++]=s;

        ++idx;

    }
}
