#include "main.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_spi.h"
#include "ste2007.h"
#include "uart.h"
#include "fonts.h"

// Port B
#define RST  GPIO_Pin_4
#define BL   GPIO_Pin_5
//#define DC   GPIO_Pin_6
// Port A
#define CS   GPIO_Pin_5
#define CLK  GPIO_Pin_4
#define DIN  GPIO_Pin_2

#define LCD_C     0x00
#define LCD_D     0x01

#define myabs(n) ((n) < 0 ? -(n) : (n))

#define chip_select_enable() gpio_reset(GPIOA,CS)
#define chip_select_disable() gpio_set(GPIOA,CS)

#define Text2A_LEN 43
#define buff2A_LEN (Text2A_LEN*7)       // =301

static const uint8_t lookup[16] = {
0x0, 0x8, 0x4, 0xc, 0x2, 0xa, 0x6, 0xe,
0x1, 0x9, 0x5, 0xd, 0x3, 0xb, 0x7, 0xf, };
/*
const uint8_t pointer[7]={0x2,0x6,0xc,0x18,0xc,0x6,0x2};
const uint8_t stereo[7]={0x3c,0x66,0xc3,0x81,0xc3,0x66,0x3c};
const uint8_t battery[14]={28,34,65,93,93,65,93, 93,65,93,93,65,127,0};
*/
static const uint8_t radiotext[Text2A_LEN]="ENERGY IZHEVSK : 96,2FM : REKLAMA 93-90-90 ";
static uint8_t buf2A[buff2A_LEN];
// running text
//static uint16_t text_offset=0;
//static bool direct_text=true;

extern uint16_t freq;
extern bool isMirror;
bool isPower=false;

// display buffer
volatile uint8_t fb[LCD_LEN];          // screen buffer
uint16_t pos;

void ste2007_send(uint8_t dc, uint8_t data);

void ste2007_init() {
    // hardware reset
    gpio_reset(GPIOB,RST);
    delay_ms(1);
    gpio_set(GPIOB,RST);
    // init routine
    chip_select_enable();
    ste2007_send(LCD_C, 0xE2);  // reset
    delay_ms(10);
    ste2007_send(LCD_C, 0xA4);  // power save off
    ste2007_send(LCD_C, 0x2F);  // power control set
    ste2007_send(LCD_C, 0xB0);  // set page address
    ste2007_send(LCD_C, 0x10);  // set col=0 upper 3 bits
    ste2007_send(LCD_C, 0x00);  // set col=0 lower 4 bits
    ste2007_send(LCD_C, 0x40);  // set row 0
    ste2007_send(LCD_C, 0xAF);  // dispaly on
    //pcd8544_send(LCD_C, 0x0d);  // inverse mode
    chip_select_disable();
    delay_ms(50);
    gpio_set(GPIOB,BL);           // enable blacklight
    isPower=true;
    ste2007_fill_fb(0x0);
    ste2007_display_fb();

    uint16_t k=0;
    for (uint8_t i=0;i<Text2A_LEN;i++) {
        uint8_t ch=radiotext[i];

        for (uint8_t j=0; j < 5; j++)
        {
            uint8_t c=ch - 0x20;
            c=ASCII_5x7[c][j];
            buf2A[k++]=c;
        }
        buf2A[k++]=0;
        buf2A[k++]=0;
    }

}

void ste2007_send(uint8_t dc, uint8_t data)
{
#ifdef HW_SPI
    uint8_t a=(lookup[data&0b1111] << 4) | lookup[data>>4];
    uint16_t d=(uint16_t)a;
    d=d<<1;
    if (dc == 0x01)
        d|=(uint16_t)0x1;
    chip_select_enable();
    while(!(USART2->SR & USART_FLAG_TXE)) {
        __asm__ volatile ("nop\n");
    }

    USART2->DR=d;

    while(!(USART2->SR & USART_FLAG_TC)) {
        __asm__ volatile ("nop\n");
    }
    chip_select_disable();
#else
    // 9-th bit
    if (dc == LCD_D)
        gpio_set(GPIOA,DIN);
    else
        gpio_reset(GPIOA,DIN);

    // Clock Signal
    gpio_set(GPIOA,CLK);
    gpio_reset(GPIOA,CLK);
    // send data bits
   for (uint8_t i=0; i<8; i++)
   {
      if (data & 0x80)
          gpio_set(GPIOA,DIN);
      else
          gpio_reset(GPIOA,DIN);

      data=(data<<1);
      // Set Clock Signal
      gpio_set(GPIOA,CLK);
      gpio_reset(GPIOA,CLK);
   }
#endif
}

//  work with buffer
void ste2007_fill_fb(uint8_t value)
{
    for(int i=0;i<LCD_LEN; i++)
        fb[i]=value;
    pos=0;
}

void ste2007_display_fb() {
    chip_select_enable();
    for(uint16_t i=0;i<LCD_LEN; i++)
        ste2007_send(LCD_D,fb[i]);

    ste2007_send(LCD_C, 0xB0);  // set page address
    ste2007_send(LCD_C, 0x10);  // set col=0 upper 3 bits
    ste2007_send(LCD_C, 0x00);  // set col=0 lower 4 bits
    ste2007_send(LCD_C, 0x40);  // set row 0

    chip_select_disable();
}


void ste2007_off() {
    ste2007_fill_fb(0x0);
    ste2007_display_fb();
    delay_ms(10);
    // hardware reset
    gpio_reset(GPIOB,RST);
    delay_ms(10);
    gpio_set(GPIOB,RST);
    delay_ms(50);
    // blacklight off
    gpio_reset(GPIOB,BL);
    isPower=false;
}


void ste2007_mirror() {
    for (uint16_t i=0;i<LCD_LEN;i++) {
        uint8_t b=fb[i];
        fb[i]=(lookup[b&0b1111] << 4) | lookup[b>>4];
    }
    for (uint16_t i=0,j=((LCD_LEN-LCD_X)-1);i<((LCD_LEN-LCD_X)>>1);i++,j--) {
        uint8_t b=fb[i];
        fb[i]=fb[(LCD_LEN-LCD_X)-1-i];
        fb[j]=b;
    }
}

void ste2007_set_graph_pos(uint8_t x, uint8_t y) {
    pos=(y*LCD_X) + x;
}


void ste2007_draw_icon(char * img, uint8_t x,uint8_t y, uint8_t num) {
    ste2007_set_graph_pos(x,y);
    for(uint8_t i=0;i<(num*7);i++)
    {
        uint8_t c=*img;
        fb[pos+i]= c;
        img++;
    }
}

/*
void pcd8544_set_point(uint8_t x, uint8_t y) {
    if (x < LCD_X && y < LCD_Y)
    {
        uint16_t index = ((y>>3)*LCD_X)+x;
        fb[index]|=(1<<(y&0x07));
    }
}

//https://ru.wikibooks.org/wiki/%D0%A0%D0%B5%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D0%B8_%D0%B0%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC%D0%BE%D0%B2/%D0%90%D0%BB%D0%B3%D0%BE%D1%80%D0%B8$
void pcd8544_draw_line(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2) {
    const int deltaX = myabs(x2 - x1);
    const int deltaY = myabs(y2 - y1);
    const int signX = x1 < x2 ? 1 : -1;
    const int signY = y1 < y2 ? 1 : -1;

    int error = deltaX - deltaY;

    pcd8544_set_point(x2,y2);
    while(x1 != x2 || y1 != y2)
    {
        pcd8544_set_point(x1,y1);
        const int error2 = error * 2;

        if(error2 > -deltaY)
        {
            error -= deltaY;
            x1 += signX;
        }
        if(error2 < deltaX)
        {
            error += deltaX;
            y1 += signY;
        }
    }
}
void pcd8544_draw_icon_fb(const char * img, uint8_t x,uint8_t y, uint8_t num) {
    pos=x+y*84;
    uint8_t i;
    for(i=0;i<(num*7);i++)
    {
        uint8_t c=img[i];
        if ((pos+i)<504) fb[pos+i]|=c;
    }
    pos+=num;
}



void radio_encoder_interface(int offset) {
    pcd8544_clear();
    /// print frequency
    uint16_t f=(uint16_t)(freq + offset);
    uint8_t f1=(uint8_t)(f/10);
    uint8_t f2=(uint8_t)(f%10);

    if (f1>=100)
        pcd8544_print_uint8_at(f1,FONT_SIZE_3,7,1);
    else
        pcd8544_print_uint8_at(f1,FONT_SIZE_3,22,1);

    pcd8544_print_uint8_at(f2,FONT_SIZE_2,66,2);

    pos=312;
    pcd8544_send_char(0x27,false);

    if (isMirror)
        pcd8544_mirror();

#ifndef USE_DMA
    pcd8544_display_fb();
#else
    pcd8544_DMA_UPD();
#endif
}


void pcd8544_set_pos(uint8_t x, uint8_t y) {
    pos=(y*LCD_X) + (x*7);
}

void running_text() {

    for (uint16_t i=text_offset,j=0;i<(text_offset +LCD_X);i++,j++) {
        uint8_t c=buf2A[i];
        fb[336+j]=c;
    }
    text_offset =(direct_text) ? text_offset +1: text_offset -1;
    if (text_offset >= (buff2A_LEN-LCD_X) || text_offset == 0) {
        direct_text=!direct_text;
    }

    pcd8544_display_fb();
}
*/


