﻿#include "main.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_usart.h"
#include "stm32f10x_spi.h"
#include "stm32f10x_rcc.h"
#include "uart.h"
#include "ste2007.h"
#include "cp866.h"
#include "font5x7.h"
#include "cybercafe.h"
#include "menu.h"

extern bool isPower;
extern uint8_t tim4_counter;
extern uint8_t uart_ready;
extern uint8_t uart_index;
extern char uart_buf[UART_BUFFER_LEN];              // UART_BUFFER_LEN=10
uint16_t freq=947;
bool isMirror=false;
bool isRadio=false;
uint8_t radio_select=0;
bool isMenu=false;
void clear_uart_buf();
void print_encoder(char* str, int value);

/*  display HX1230 connect to stm32f103c8
PA4 (USART2_TX) - CLK
PA2 (USART2_TX) - DIN
PA5       -       CS
PB4       -       RST
PB5       -       BL
*/

int main()
{   
    // enable GPIOC port
    RCC->APB2ENR |= RCC_APB2Periph_GPIOC;           // enable PORT_C
    RCC->APB2ENR |= RCC_APB2Periph_GPIOA;           // enable PORT_A
    RCC->APB2ENR |= RCC_APB2Periph_GPIOB;           // enable PORT_B
    RCC->APB2ENR |= RCC_APB2Periph_USART1;          // enable UART1
    RCC->APB1ENR |= RCC_APB1Periph_TIM2;            // enable TIM2
    RCC->APB1ENR |= RCC_APB1Periph_TIM4;            // enable TIM4
#ifdef HW_SPI
    RCC->APB1ENR |= RCC_APB1Periph_USART2;          // enable USART2
#endif
    // --- GPIO setup ----
    GPIOC->CRH &= ~(uint32_t)(0xf<<20);             // Reset for PC13 (LED)
    GPIOC->CRH |=  (uint32_t)(0x2<<20);             // Push-Pull 2-MHz fo PC13 (LED)
	gpio_set(GPIOC,LED);
    // UART1 PA9,PA10
    GPIOA->CRH &= ~(uint32_t)(0xf<<4);              // enable Alterentive mode
    GPIOA->CRH |=  (uint32_t)(0xa<<4);              // for PA9 = USART1_TX
    GPIOA->CRH &= ~(uint32_t)(0xf<<8);              // enable Alterentive mode
    GPIOA->CRH |=  (uint32_t)(0xa<<8);              // for PA10 = USART1_RX
    // encoder PA0, PA1
    GPIOA->CRL &= ~(uint32_t)(0xff);                // clear
    GPIOA->CRL |=  (uint32_t)(0x88);                // pull-down input mode for PA0, PA1
    GPIOA->ODR |= (uint16_t)(0x3);                  // switch to pull-up mode for PA0,PA1
    // Configure Port B. PB4_RST,
    GPIOB->CRL &= ~(uint32_t)(0xf<<16);             // clear mode for PB4   (RST)
    GPIOB->CRL |= (uint32_t)(0x3<<16);              // for PB4 set  PushPull mode 50MHz (RST)
    GPIOB->CRL &= ~(uint32_t)(0xf<<20);             // clear mode for PB5   (BackLight)
    GPIOB->CRL |= (uint32_t)(0x3<<20);              // for PB5 set  PushPull mode 50MHz (BL)
    // Configure Port A. PA5_CS
    GPIOA->CRL &= ~(uint32_t)(0xf<<20);             // clear mode for PA5   (CS)
    GPIOA->CRL |= (uint32_t)(0x3<<20);              // for PA5 set  PushPull mode 50MHz (RST)
#ifdef HW_SPI
    // USART2 PA2_TX, PA4_CLK
    GPIOA->CRL &= ~(uint32_t)(0xf<<8);              // enable Alterentive mode, 50MHz
    GPIOA->CRL |=  (uint32_t)(0xb<<8);              // for PA2 = USART2_TX
    GPIOA->CRL &= ~(uint32_t)(0xf<<16);              // enable Alterentive mode, 50MHz
    GPIOA->CRL |=  (uint32_t)(0xb<<16);              // for PA4 = USART2_CLK
#else
    // USART2 PA2_TX, PA4_CLK
    GPIOA->CRL &= ~(uint32_t)(0xf<<8);              // enable PushPull mode, 50MHz
    GPIOA->CRL |=  (uint32_t)(0x3<<8);              // for PA2 = USART2_TX
    GPIOA->CRL &= ~(uint32_t)(0xf<<16);              // enable PushPull mode, 50MHz
    GPIOA->CRL |=  (uint32_t)(0x3<<16);              // for PA4 = USART2_CLK
#endif
    // ------- SysTick CONFIG --------------
    if (SysTick_Config(72000)) // set 1ms
    {
        while(1); // error
    }
    // ------ TIM2 Setup -------------------
    TIM2->CCMR1 = TIM_CCMR1_CC1S_0 | TIM_CCMR1_CC2S_0;
    TIM2->CCER  = TIM_CCER_CC1P | TIM_CCER_CC2P;
    TIM2->SMCR  = TIM_SMCR_SMS_0 | TIM_SMCR_SMS_1;
    TIM2->ARR   = 1000;
    TIM2->CR1   = TIM_CR1_CEN;
    // ----- TIM4 Setup -------
    TIM4->CR1 = (0x80);                                 // set ARPE flag
    TIM4->PSC = 36000 - 1;                              // 1000 tick/sec
    TIM4->ARR = 100;                                    // 10 Interrupt/sec (1000/100)
    TIM4->DIER |=  TIM_DIER_UIE;                        // enable interrupt per overflow
    TIM4->SR = 0;
    TIM4->EGR |= (0x01);
    TIM4->CR1 |= TIM_CR1_CEN;                           // enable timer
    // --- UART1 setup ----
    //USART1->BRR  = 0x1d4c;                        // 9600 Baud, when APB2=72MHz
    USART1->BRR = 0x271;                            // 115200 Baud, when APB2=72MHz
    USART1->CR1 |= (USART_CR1_UE_Set | USART_Mode_Tx | USART_Mode_Rx | USART_CR1_RXNEIE);  // enable USART1, enable  TX/RX mode, enable RX interrupt
    // --- UART2 setup ----
#ifdef HW_SPI
    //USART2->BRR = 0x271;  // 115200 baud
    USART2->BRR = 0x12;     // 2MBaud
    USART2->CR2 |= (USART_CR2_CLKEN| USART_CR2_STOP_1 | USART_CR2_LBCL); //USART_CR2_CPHA| USART_CR2_CPOL
    USART2->CR1 |= (USART_CR1_UE_Set |USART_CR1_M| USART_Mode_Tx);
#endif

    //---------- NVIC ----------------------
    NVIC_SetPriority(USART1_IRQn,1);                // set priority for USART1 IRQ
    NVIC_EnableIRQ(USART1_IRQn);                    // enable USART1 IRQ via NVIC
    NVIC_SetPriority(TIM4_IRQn, 21);                // set priority for TIM4_IRQ
    NVIC_EnableIRQ(TIM4_IRQn);                      // enable TIM4 IRQ
    //////////////////////////////////////
    /// let's go.....
    /////////////////////////////////////
    bool isEncoder=false;
    int enc_prev_value=0;
    uint8_t led_toggle=0;
    bool tick_enable=false;
    bool count_enable=true;
    bool  count2=false;
    bool  count3=false;
    uint8_t tick=0;
    uint8_t count=0;
    clear_uart_buf();
    println("ready...");
    ste2007_init();

    for(;;){
        if (uart_ready) {
            if (uart_buf[0] == '?' && uart_buf[1] == 0x0) {
                print("help:\n");
                print("t-  disable print tick\n");
                print("t+  enable print tick\n");
                print("on - turn on display\n");
                print("off  turn off display\n");
                print("test1 -  print on LCD text\n");
                print("test2 -  print on LCD text\n");
                print("test3 -  print on LCD text\n");
                print("test4 -  print on LCD text\n");
                print("test5 -  print on LCD text\n");
                print("test6 -  print on LCD text\n");
                print("test7 -  print on LCD text\n");
                print("test8 -  print on LCD text\n");
                print("test9 -  print on LCD text\n");
                print("fill - to fill LCD of pattern 0xAA\n");
                print("clr - to clear LCD display\n");
                print("radio1 - show FM-radio interface\n");
                print("radio2 - show FM-radio interface\n");
                print("radio3 - show FM-radio interface\n");
                print("menu - show menu interface\n");
                print("count1 - enable increment count on LCD\n");
                print("count2 - enable increment count on LCD\n");
                print("count3 - enable increment count on LCD\n");
            } else if (uart_buf[0] == 't' && uart_buf[1] == '+') {
                print("turn on tick\n");
                tick_enable=true;
            } else if (uart_buf[0] == 't' && uart_buf[1] == '-') {
                print("turn off tick\n");
                tick_enable=false;
            } else if (comp_str(uart_buf,"off") && isPower) {
                ste2007_off();
                print("power off\n");
                count_enable=false;
                isRadio=false;
            } else if (uart_buf[0] == 'o' && uart_buf[1] == 'n' && !isPower) {
                ste2007_init();
                print("power on\n");
                isRadio=false;
                isMenu=false;
            } else if (comp_str(uart_buf,"mirror") && isPower) {
                isMirror=!isMirror;
                if (isMirror)
                    print("mirror mode is ON\n");
                else
                    print("mirror mode is OFF\n");
                ste2007_mirror();
                ste2007_display_fb();
            }  else if (comp_str(uart_buf,"clr") && isPower) {
                ste2007_clear();
                ste2007_fill_fb(0x0);
                ste2007_display_fb();
                print("clearing...\n");
                count_enable=false;
                isRadio=false;
                isMenu=false;
              }  else if (comp_str(uart_buf,"fill") && isPower) {
                ste2007_clear();
                ste2007_fill_fb(0xaa);
                ste2007_display_fb();
                print("filling...\n");
                count_enable=false;
                isRadio=false;
                isMenu=false;
            }  else if (comp_str(uart_buf,"radio1") && isPower) {
                fm_radio_interface(freq);
                count_enable=false;
                isRadio=true;
                radio_select=1;
                isMenu=false;
            } else if (comp_str(uart_buf,"radio2") && isPower) {
                fm_radio_interface2(freq);
                count_enable=false;
                isRadio=true;
                radio_select=2;
                isMenu=false;
            } else if (comp_str(uart_buf,"radio3") && isPower) {
                fm_radio_interface3(freq);
                count_enable=false;
                isRadio=true;
                radio_select=3;
                isMenu=false;
            } else if (comp_str(uart_buf,"count1") && isPower) {
                count_enable=true;
                count2=false;
                count3=false;
                isRadio=false;
                isMenu=false;
                tick=0;
            } else if (comp_str(uart_buf,"count2") && isPower) {
                count_enable=true;
                count2=true;
                count3=false;
                isRadio=false;
                isMenu=false;
                tick=0;
            } else if (comp_str(uart_buf,"count3") && isPower) {
                count_enable=true;
                count2=false;
                count3=true;
                isRadio=false;
                isMenu=false;
                tick=0;
            } else if(comp_str(uart_buf,"test1") && isPower) {
                ste2007_clear();
                lcd_print_5x7("++++++++++++++",0,0);
                lcd_print_5x7("=Hello, World=",0,1);
                lcd_print_5x7("!!!!!!!!!!!!!!",0,2);
                lcd_print_invert_5x7("=testtesttest=",0,3);
                lcd_print_5x7("--------------",0,4);
                lcd_print_5x7("=\x8f\xe0\xa8\xa2\xa5\xe2 \x8c\xa8\xe0!!=",0,5);
                lcd_print_invert_5x7("\x8c\xa8\xe0, \x92\xe0\xe3\xa4, \x8c\xa0\xa9",0,6);
                lcd_print_5x7("=Privet, MiR!=",0,7);
                lcd_print_invert_5x7("++++++++++++++",0,8);
                if (isMirror)
                    ste2007_mirror();
                ste2007_display_fb();
                count_enable=false;
                isRadio=false;
                isMenu=false;
             } else if(comp_str(uart_buf,"test2") && isPower) {
                ste2007_clear();
                lcd_print_8x8("++++++++++++",0,0);
                lcd_print_8x8("Hello, World",0,1);
                lcd_print_8x8("!!!!!!!!!!!!",0,2);
                lcd_print_invert_8x8("testtesttest",0,3);
                lcd_print_8x8("------------",0,4);
                lcd_print_8x8("\x8f\xe0\xa8\xa2\xa5\xe2 \x8c\xa8\xe0!!",0,5);
                lcd_print_invert_8x8("\x8c\xa8\xe0 \x92\xe0\xe3\xa4 \x8c\xa0\xa9",0,6);
                lcd_print_8x8("Privet, MiR!",0,7);
                lcd_print_invert_8x8("++++++++++++",0,8);
                if (isMirror)
                    ste2007_mirror();
                ste2007_display_fb();
                count_enable=false;
                isRadio=false;
                isMenu=false;
             } else if(comp_str(uart_buf,"test3") && isPower) {
                ste2007_clear();
                cp866_print_at_fb("Twenty six", FONT_SIZE_1,0,0);
                cp866_print_at_fb("Joker", FONT_SIZE_2,0,2);
                cp866_print_at_fb("Zero", FONT_SIZE_3,0,5);
                if (isMirror)
                    ste2007_mirror();
                ste2007_display_fb();
                count_enable=false;
                isRadio=false;
                isMenu=false;
            } else if(comp_str(uart_buf,"test4") && isPower) {
               ste2007_clear();
               // Съешь еще этих мягких французских булок
               cp866_print_at_fb("\x91\xea\xa5\xe8\xec\x20\xa5\xe9\xf1\x20\x20\x20\xed\xe2\xa8\xe5\x20\xac\xef\xa3\xaa\xa8\xe5\x20\xe4\xe0\xa0\xad\xe6"
                                 "\xe3\xa7\xe1\xaa\xa8\xe5\x20\xa1\xe3\xab\xae\xaa\x2c\x20\xa4\xa0\x20\x20\x20\xa2\xeb\xaf\xa5\xa9\x20\xe7\xa0\xee.", FONT_SIZE_1,0,1);
               if (isMirror)
                   ste2007_mirror();
               ste2007_display_fb();
               count_enable=false;
               isRadio=false;
               isMenu=false;
            } else if(comp_str(uart_buf,"test5") && isPower) {
               ste2007_clear();
               cp866_print_at_fb("\x92\xe3\xaa\x2d\xe2\xe3\xaa\x20\x8d\xa5\xae\x2e\x2e", FONT_SIZE_1,0,0);
               cp866_print_at_fb("\x80\xab\xa8\xe1\xa0", FONT_SIZE_2,0,2);
               cp866_print_at_fb("\x8c\xa5\xad\xee", FONT_SIZE_3,0,5);
               if (isMirror)
                   ste2007_mirror();
               ste2007_display_fb();
               count_enable=false;
               isRadio=false;
               isMenu=false;
            } else if(comp_str(uart_buf,"test6") && isPower) {
               ste2007_clear();
               font_ascii_5x7_print("Twenty six", FONT_SIZE_1,0,0);
               font_ascii_5x7_print("Joker", FONT_SIZE_2,0,2);
               font_ascii_5x7_print("Zero", FONT_SIZE_3,0,5);
               if (isMirror)
                   ste2007_mirror();
               ste2007_display_fb();
               count_enable=false;
               isRadio=false;
               isMenu=false;
            } else if(comp_str(uart_buf,"test7") && isPower) {
               ste2007_clear();
               font_ascii_5x7_print("\x92\xe3\xaa\x2d\xe2\xe3\xaa\x20\x8d\xa5\xae\x2e\x2e", FONT_SIZE_1,0,0);
               font_ascii_5x7_print("\x80\xab\xa8\xe1\xa0", FONT_SIZE_2,0,2);
               font_ascii_5x7_print("\x8c\xa5\xad\xee", FONT_SIZE_3,0,5);
               if (isMirror)
                   ste2007_mirror();
               ste2007_display_fb();
               count_enable=false;
               isRadio=false;
               isMenu=false;
            } else if(comp_str(uart_buf,"test8") && isPower) {
               ste2007_clear();
               // Съешь еще этих мягких французских булок
               font_ascii_5x7_print("\x91\xea\xa5\xe8\xec\x20\xa5\xe9\xa5\x20\xed\xe2\xa8\xe5\xac\xef\xa3\xaa\xa8\xe5\x20\xe4\xe0\xa0\xad\xe6"
                                 "\xe3-\xa7\xe1\xaa\xa8\xe5\x20\xa1\xe3\xab\xae\xaa\x2c\x20\x20\xa4\xa0\x20\xa2\xeb\xaf\xa5\xa9\x20\xe7\xa0\xee.", FONT_SIZE_1,0,1);
               if (isMirror)
                   ste2007_mirror();
               ste2007_display_fb();
               count_enable=false;
               isRadio=false;
               isMenu=false;
            } else if(comp_str(uart_buf,"test9") && isPower) {
               ste2007_clear();
               //cybercafe_print_huge_uint8(148,16,2);
               // lorem
               cybercafe_print_str("<0123456789>Lorem ipsum dolor sit amet, consecte", 0,0);
               if (isMirror)
                   ste2007_mirror();
               ste2007_display_fb();
               count_enable=false;
               isRadio=false;
               isMenu=false;
            } else if(comp_str(uart_buf,"menu") && isPower) {
                main_menu(4);
                count_enable=isRadio=false;
                isMenu=true;
            } else {
                print("invalid command\n");
            }
            clear_uart_buf();
            uart_ready=0;
            uart_index=0;
        }

        int enc_value=TIM2->CNT;
        enc_value >>=2;
        if (enc_value != enc_prev_value) {
            enc_prev_value=enc_value;
            isEncoder=true;
            print_encoder("encoder: ",enc_value);
            tim4_counter=TIM4_INIT_VALUE;
            //-------- LCD ---------------
            if (enc_value >= 125) {
                enc_value -= 250;
            }
            if (!isRadio && !isMenu) {
                ste2007_clear();
                if (enc_value < 0)
                    lcd_print_hugo('-',0,2);
                cp866_print_uint8_at(get_abs(enc_value),3,22,2);
                if (isMirror)
                    ste2007_mirror();
                ste2007_display_fb();
            } else if (isRadio && radio_select == 1) {
                //radio_encoder_interface(enc_value);
            } else if (isMenu) {
                TIM2->CNT=0;
                if (enc_value)
                    main_menu_enc(enc_value);
            }
        } else if (isEncoder && !tim4_counter) {
            __disable_irq();
            isEncoder=false;

            if (isRadio) {
                int enc_value=TIM2->CNT;
                enc_value >>=2;
                if (enc_value >= 125) {
                    enc_value -= 250;
                }
                freq +=enc_value;
                //_interface(freq);
            } else if (isMenu) {
                int enc_value=TIM2->CNT;
                enc_value >>=2;
                if (enc_value >= 125) {
                    enc_value -= 250;
                }
                main_menu_enc(enc_value);
            }

            TIM2->CNT=0;
            __enable_irq();
            enc_prev_value=0;
            print_encoder("total: ",enc_value);
        }

        if ((++count % 20)  == 0) {
            if (led_toggle)
                gpio_set(GPIOC,LED);
            else
                gpio_reset(GPIOC,LED);
            led_toggle=!led_toggle;

            if (count_enable && !isEncoder) {
                ste2007_clear();
                if (!count2 && !count3) {
                    lcd_print_hugo('>',0,3);
                    cp866_print_uint8_at(tick,FONT_SIZE_3,21,3);
                } else if (!count2 && count3) {
                    ste2007_set_graph_pos(0,3);
                    cybercafe_digits_send(14);
                    //lcd_print_hugo('>',0,3);
                    cybercafe_print_huge_uint8(tick,21,3);
                } else {
                    font_ascii_5x7_send_char_size3('>',0,3);
                    font_ascii_5x7_print_uint8(tick,FONT_SIZE_3,18,3);
                }
                if (isMirror)
                    ste2007_mirror();
                ste2007_display_fb();
                ++tick;
            }

            if (tick_enable)
                print("tick..\n");
            count=0;
        }
        delay_ms(50);
    }
}
/*
        int enc_value=TIM2->CNT;
        enc_value >>=2;
        if (enc_value != enc_prev_value) {
            enc_prev_value=enc_value;
            isEncoder=true;
            print_encoder("encoder: ",enc_value);
            tim4_counter=TIM4_INIT_VALUE;
            //-------- LCD ---------------
            if (enc_value >= 125) {
                enc_value -= 250;
            }
            if (!isRadio && !isMenu) {
                pcd8544_clear();
                if (enc_value < 0)
                    lcd_print_hugo('-',0,2);
                pcd8544_print_uint8_at(get_abs(enc_value),3,18,2);
                if (isMirror)
                    pcd8544_mirror();
#ifndef         USE_DMA
                pcd8544_display_fb();
#else
                pcd8544_DMA_UPD();
#endif
            } else if (isRadio) {
                radio_encoder_interface(enc_value);
            } else if (isMenu) {
                TIM2->CNT=0;
                if (enc_value)
                    main_menu_enc(enc_value);
            }

        } else if (isEncoder && !tim4_counter) {
            __disable_irq();
            isEncoder=false;
            if (isRadio) {
                int enc_value=TIM2->CNT;
                enc_value >>=2;
                if (enc_value >= 125) {
                    enc_value -= 250;
                }
                freq +=enc_value;
                fm_radio_interface(freq);
            }
*/
/*else if (isMenu) {
                int enc_value=TIM2->CNT;
                enc_value >>=2;
                if (enc_value >= 125) {
                    enc_value -= 250;
                }
                main_menu_enc(enc_value);
            }*/
/*
            TIM2->CNT=0;
            __enable_irq();
            enc_prev_value=0;
            if (!isMenu) {
                print_encoder("total: ",enc_value);
            }
        }

        if ((++count % 20)  == 0) {
            if (led_toggle)
                gpio_set(GPIOC,LED);
            else
                gpio_reset(GPIOC,LED);
            led_toggle=!led_toggle;

            if (count_enable && !isEncoder) {
                pcd8544_clear();
                lcd_print_hugo('>',0,2);
                pcd8544_print_uint8_at(tick,3,18,2);
                if (isMirror)
                    pcd8544_mirror();
#ifndef         USE_DMA
                pcd8544_display_fb();
#else
                pcd8544_DMA_UPD();
#endif
                tick++;
            }

            if (tick_enable)
                print("tick..\n");
            count=0;
        }


        if (isRadio && !isEncoder && !(count % 5)) {
            running_text();
       }

        delay_ms(50);
    }
}
*/
void clear_uart_buf() {
    for (uint8_t i=0; i<UART_BUFFER_LEN;i++)
        uart_buf[i]=0;
}

void print_encoder(char* str, int value) {
    if (value >= 125) {
        value -= 250;
    }
    print(str);
    uart_print_int(value);
    uart_send_char('\n');
}

