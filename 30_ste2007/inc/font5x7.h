#ifndef __FONT5x7_H__
#define __FONT5x7_H__
#include "stdbool.h"
#include "fonts.h"

// alias
#define  lcd_print_5x7(x,y,z)   font_ascii_5x7_print_str(x,y,z)
#define  lcd_print_invert_5x7(s,x,y) font_ascii_5x7_print_str_invert(s,x,y)


void font_ascii_5x7_send(uint8_t ch,bool inverse);
void font_ascii_5x7_print_str(char *str, uint8_t x, uint8_t y);
void font_ascii_5x7_print_str_invert(char *str, uint8_t x, uint8_t y);
void font_ascii_5x7_send_char_size3(uint8_t ch, uint8_t x, uint8_t y);
void font_ascii_5x7_send_char_size2(uint8_t ch, uint8_t x, uint8_t y);
void font_ascii_5x7_print(char *str, uint8_t size,  uint8_t x, uint8_t y);
void font_ascii_5x7_print_uint8(uint8_t num, uint8_t size, uint8_t x, uint8_t y);
// crystal
void nums_send(uint8_t ch);
void cristal_print_uint8(uint8_t num, uint8_t x, uint8_t y);

#endif  // __FONT5x7_H__
