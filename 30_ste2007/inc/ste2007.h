#ifndef __STE2007_H__
#define __STE2007_H__
#include <stdint.h>
#include <stdbool.h>

#define LCD_X     96
#define LCD_Y     72
//#define LCD_LEN   (uint16_t)((LCD_X * LCD_Y) / 8)
#define LCD_LEN 864

#define FONT_SIZE_1 (uint8_t)0x01
#define FONT_SIZE_2 (uint8_t)0x02
#define FONT_SIZE_3 (uint8_t)0x03

/// alias
#define  ste2007_clear() ste2007_fill_fb(0x0)

// functions
void ste2007_init();
void ste2007_fill_fb(uint8_t value);
void ste2007_display_fb();
void ste2007_off();
void ste2007_mirror();
void ste2007_draw_icon(char * img, uint8_t x,uint8_t y, uint8_t num);
void ste2007_set_graph_pos(uint8_t x, uint8_t y);
/*
void pcd8544_set_pos(uint8_t x, uint8_t y);
void fm_radio_interface(uint16_t freq);
void radio_encoder_interface(int offset);
void pcd8544_print_string(char *str, uint8_t x, uint8_t y);
void pcd8544_print_string_invert_fb(char *str, uint8_t x, uint8_t y);
void pcd8544_send_char_size2_fb(uint8_t ch, uint8_t x, uint8_t y);
void pcd8544_send_char_size3_fb(uint8_t ch, uint8_t x, uint8_t y);
void pcd8544_print_uint8_at(uint8_t num, uint8_t size, uint8_t x, uint8_t y);
void pcd8544_print_at_fb(char *str, uint8_t size,  uint8_t x, uint8_t y);
void pcd8544_send_char(uint8_t ch,bool inverse);
void pcd8544_mirror();
void running_text();
*/
#endif      // __STE2007_H__
