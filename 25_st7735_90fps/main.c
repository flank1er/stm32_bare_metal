#include "main.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_usart.h"
#include "stm32f10x_spi.h"
#include "stm32f10x_rcc.h"
#include "uart.h"
#include "st7735.h"
extern bool isPower;
extern uint8_t tim4_counter;
extern uint8_t uart_ready;
extern uint8_t uart_index;
extern char uart_buf[UART_BUFFER_LEN];              // UART_BUFFER_LEN=10
void clear_uart_buf();
//void print_encoder(char* str, int value);

int main()
{
    // enable GPIOC port
    RCC->APB2ENR |= RCC_APB2Periph_GPIOC;           // enable PORT_C
    RCC->APB2ENR |= RCC_APB2Periph_GPIOA;           // enable PORT_A
    RCC->APB2ENR |= RCC_APB2Periph_GPIOB;           // enable PORT_B
    RCC->APB2ENR |= RCC_APB2Periph_USART1;          // enable UART1
    RCC->APB1ENR |= RCC_APB1Periph_TIM2;            // enable TIM2
    RCC->APB1ENR |= RCC_APB1Periph_TIM4;            // enable TIM4
#ifdef HW_SPI
    RCC->APB2ENR |= RCC_APB2Periph_SPI1;            // enable SPI1
#endif
#ifdef USE_DMA
    RCC->AHBENR  |= RCC_AHBENR_DMA1EN;              // enable DMA1
#endif
    // --- GPIO setup ----
    GPIOC->CRH &= ~(uint32_t)(0xf<<20);             // Reset for PC13 (LED)
    GPIOC->CRH |=  (uint32_t)(0x2<<20);             // Push-Pull 2-MHz fo PC13 (LED)
	gpio_set(GPIOC,LED);
    GPIOA->CRH &= ~(uint32_t)(0xf<<4);              // enable Alterentive mode
    GPIOA->CRH |=  (uint32_t)(0xa<<4);              // for PA9 = USART1_TX
    GPIOA->CRH &= ~(uint32_t)(0xf<<8);              // enable Alterentive mode
    GPIOA->CRH |=  (uint32_t)(0xa<<8);              // for PA10 = USART1_RX
    // encoder PA0, PA1
    GPIOA->CRL &= ~(uint32_t)(0xff);                // clear
    GPIOA->CRL |=  (uint32_t)(0x88);                // pull-down input mode for PA0, PA1
    GPIOA->ODR |= (uint16_t)(0x3);                  // switch to pull-up mode for PA0,PA1
    // Configure Port B. PB6_DC, PB4_RST, PB5_BackLight
    GPIOB->CRL &= ~(uint32_t)(0xf<<24);    // clear mode for PB6   (DC)
    GPIOB->CRL &= ~(uint32_t)(0xf<<16);    // clear mode for PB4   (RST)
    GPIOB->CRL &= ~(uint32_t)(0xf<<20);    // clear mode for PB5   (BackLight)
    GPIOB->CRL |= (uint32_t)(0x3<<24);     // for PB6 set  PushPull mode 50MHz (DC)
    GPIOB->CRL |= (uint32_t)(0x3<<16);     // for PB4 set  PushPull mode 50MHz (RST)
    GPIOB->CRL |= (uint32_t)(0x7<<20);     // for PB5 set  OpenDrain mode 50MHz (BL)
    // SOFT SPI Setup (PA4_SE, PA5_CLK,  PA7_DIO)
    GPIOA->CRL &= ~(uint32_t)(0xf<<16);    // clear mode for PA4   (SE)
    GPIOA->CRL &= ~(uint32_t)(0xf<<20);    // clear mode for PA5   (SPI_Clock)
    GPIOA->CRL &= ~(uint32_t)(0xf<<28);    // clear mode for PA7   (SPI_DIO)
    GPIOA->CRL |= (uint32_t)(0x3<<16);     // for PA4 set  PushPull mode 50MHz (SE)
#ifdef HW_SPI
    GPIOA->CRL |= (uint32_t)(0xb<<20);              // for PA5 set  Alternative mode, 50MHz
    GPIOA->CRL |= (uint32_t)(0xb<<28);              // for PA7 set  Alternative mode, 50MHz
#else
    GPIOA->CRL |= (uint32_t)(0x3<<20);     // for PA5 set  PushPull mode 50MHz (SPI_Clock)
    GPIOA->CRL |= (uint32_t)(0x3<<28);     // for PA7 set  PushPull mode 50MHz (SPI_DIO)
#endif
    // ------- SysTick CONFIG --------------
    if (SysTick_Config(72000)) // set 1ms
    {
        while(1); // error
    }
    // ------ TIM2 Setup -------------------
    TIM2->CCMR1 = TIM_CCMR1_CC1S_0 | TIM_CCMR1_CC2S_0;
    TIM2->CCER  = TIM_CCER_CC1P | TIM_CCER_CC2P;
    TIM2->SMCR  = TIM_SMCR_SMS_0 | TIM_SMCR_SMS_1;
    TIM2->ARR   = 1000;
    TIM2->CR1   = TIM_CR1_CEN;
    // ----- TIM4 Setup -------
    TIM4->CR1 = (0x80);                                 // set ARPE flag
    TIM4->PSC = 36000 - 1;                              // 1000 tick/sec
    TIM4->ARR = 100;                                    // 10 Interrupt/sec (1000/100)
    TIM4->DIER |=  TIM_DIER_UIE;                        // enable interrupt per overflow
    TIM4->SR = 0;
    TIM4->EGR |= (0x01);
    TIM4->CR1 |= TIM_CR1_CEN;                           // enable timer
    // --- UART setup ----
    //USART1->BRR  = 0x1d4c;                        // 9600 Baud, when APB2=72MHz
    USART1->BRR = 0x271;                            // 115200 Baud, when APB2=72MHz
    USART1->CR1 |= (USART_CR1_UE_Set | USART_Mode_Tx | USART_Mode_Rx | USART_CR1_RXNEIE);  // enable USART1, enable  TX/RX mode, enable RX interrupt
#ifdef HW_SPI
    // --- SPI1 setup ----
    SPI1->CR1  = (SPI_Mode_Master|SPI_DataSize_8b|SPI_NSS_Soft|SPI_BaudRatePrescaler_4);
    //SPI1->CR2 |= SPI_CR2_TXDMAEN;           // enable DMA request
    SPI1->CR1 |= SPI_CR1_SPE;
#endif
    //---------- NVIC ----------------------
    NVIC_SetPriority(USART1_IRQn,1);                // set priority for USART1 IRQ
    NVIC_EnableIRQ(USART1_IRQn);                    // enable USART1 IRQ via NVIC
    NVIC_SetPriority(TIM4_IRQn, 21);                // set priority for TIM4_IRQ
    NVIC_EnableIRQ(TIM4_IRQn);                      // enable TIM4 IRQ
    //////////////////////////////////////
    /// let's go.....
    /////////////////////////////////////
/*
    bool isEncoder=false;
    int enc_prev_value=0;
    uint8_t led_toggle=0;
    bool tick_enable=1;
    uint8_t count=0;
*/
    clear_uart_buf();
	println("ready...");
    st7735_init();
    //pcd8544_fill_fb(0xaa);
    //pcd8544_display_fb();

    for(;;) {
        gpio_set(GPIOC,LED);
        st7735_fill(ST77XX_GREEN);
        delay_ms(3000);
        println("begin");
        st7735_fill(ST77XX_RED);
        println("end");
        gpio_reset(GPIOC,LED);
        delay_ms(3000);
    }
}
/*
    for(;;){
        if (uart_ready) {
            if (uart_buf[0] == '?' && uart_buf[1] == 0x0) {
                print("help:\n");
                print("t-  disable print tick\n");
                print("on - turn on display\n");
                print("t-  turn off display\n");
            } else if (uart_buf[0] == 't' && uart_buf[1] == '-') {
                print("turn off tick\n");
                tick_enable=false;
//            } else if (comp_str(uart_buf,"off") && isPower) {
//                st7735_off();
            } else if (uart_buf[0] == 'o' && uart_buf[1] == 'n' && !isPower) {
                st7735_init();
            } else {
                print("invalid command\n");
            }
            clear_uart_buf();
            uart_ready=0;
            uart_index=0;
        }

        int enc_value=TIM2->CNT;
        enc_value >>=2;
        if (enc_value != enc_prev_value) {
            enc_prev_value=enc_value;
            isEncoder=true;
            print_encoder("encoder: ",enc_value);
            tim4_counter=TIM4_INIT_VALUE;
        } else if (isEncoder && !tim4_counter) {
            __disable_irq();
            isEncoder=false;
            TIM2->CNT=0;
            __enable_irq();
            enc_prev_value=0;
            print_encoder("total: ",enc_value);
        }

        if ((++count % 20)  == 0) {
            if (led_toggle)
                gpio_set(GPIOC,LED);
            else
                gpio_reset(GPIOC,LED);
            led_toggle=!led_toggle;
            if (tick_enable)
                print("tick..\n");
            count=0;
        }
        delay_ms(50);
    }
}



void print_encoder(char* str, int value) {
    if (value >= 125) {
        value -= 250;
    }
    print(str);
    uart_print_int(value);
    uart_send_char('\n');
}
*/
void clear_uart_buf() {
    for (uint8_t i=0; i<UART_BUFFER_LEN;i++)
        uart_buf[i]=0;
}
