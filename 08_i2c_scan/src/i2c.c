#include "i2c.h"

uint8_t init_i2c(uint8_t adr, uint8_t value, uint8_t last) {
    I2C1->CR1 |= I2C_CR1_START_Set;         	//=0x0100, START
    while (!(I2C1->SR1 & I2C_FLAG_SB));     	// wait SB
    (void) I2C1->SR1;
    I2C1->DR=adr;								// send ADDR
    while (!(I2C1->SR1 & I2C_FLAG_ADDR))    	// wait ADDR
    {
        if(I2C1->SR1 & I2C_IT_AF)				// if NACK
            return 1;
    }
    (void) I2C1->SR1;                       	// clear ADDR
    (void) I2C1->SR2;                       	// clear ADDR
    I2C1->DR=value;
    if (last == LAST) {
        while(!(I2C1->SR1 & I2C_FLAG_BTF));     // wait BFT
        I2C1->CR1 |= I2C_CR1_STOP_Set;          // Program the STOP bit
        while (I2C1->CR1 & I2C_CR1_STOP_Set);   // Wait until STOP bit is cleared by hardware
    } else {
        while(!(I2C1->SR1 & I2C_FLAG_TXE));     // wait TXE bit
    }
    return 0;
}


