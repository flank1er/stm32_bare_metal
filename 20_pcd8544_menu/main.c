#include "main.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_usart.h"
#include "stm32f10x_spi.h"
#include "stm32f10x_rcc.h"
#include "uart.h"
#include "pcd8544.h"
#include "menu.h"

extern bool isPower;
extern uint8_t tim4_counter;
extern uint8_t uart_ready;
extern uint8_t uart_index;
extern char uart_buf[UART_BUFFER_LEN];              // UART_BUFFER_LEN=10
uint16_t freq=967;
bool isMirror=false;
bool isRadio=false;
bool isMenu=false;
void clear_uart_buf();
void print_encoder(char* str, int value);

int main()
{
    // enable GPIOC port
    RCC->APB2ENR |= RCC_APB2Periph_GPIOC;           // enable PORT_C
    RCC->APB2ENR |= RCC_APB2Periph_GPIOA;           // enable PORT_A
    RCC->APB2ENR |= RCC_APB2Periph_GPIOB;           // enable PORT_B
    RCC->APB2ENR |= RCC_APB2Periph_USART1;          // enable UART1
    RCC->APB1ENR |= RCC_APB1Periph_TIM2;            // enable TIM2
    RCC->APB1ENR |= RCC_APB1Periph_TIM4;            // enable TIM4
#ifdef HW_SPI
    RCC->APB2ENR |= RCC_APB2Periph_SPI1;            // enable SPI1
#endif
#ifdef USE_DMA
    RCC->AHBENR  |= RCC_AHBENR_DMA1EN;              // enable DMA1
#endif
    // --- GPIO setup ----
    GPIOC->CRH &= ~(uint32_t)(0xf<<20);             // Reset for PC13 (LED)
    GPIOC->CRH |=  (uint32_t)(0x2<<20);             // Push-Pull 2-MHz fo PC13 (LED)
	gpio_set(GPIOC,LED);
    GPIOA->CRH &= ~(uint32_t)(0xf<<4);              // enable Alterentive mode
    GPIOA->CRH |=  (uint32_t)(0xa<<4);              // for PA9 = USART1_TX
    GPIOA->CRH &= ~(uint32_t)(0xf<<8);              // enable Alterentive mode
    GPIOA->CRH |=  (uint32_t)(0xa<<8);              // for PA10 = USART1_RX
    // encoder PA0, PA1
    GPIOA->CRL &= ~(uint32_t)(0xff);                // clear
    GPIOA->CRL |=  (uint32_t)(0x88);                // pull-down input mode for PA0, PA1
    GPIOA->ODR |= (uint16_t)(0x3);                  // switch to pull-up mode for PA0,PA1
    // Configure Port B. PB6_DC, PB4_RST, PB5_BackLight
    GPIOB->CRL &= ~(uint32_t)(0xf<<24);    // clear mode for PB6   (DC)
    GPIOB->CRL &= ~(uint32_t)(0xf<<16);    // clear mode for PB4   (RST)
    GPIOB->CRL &= ~(uint32_t)(0xf<<20);    // clear mode for PB5   (BackLight)
    GPIOB->CRL |= (uint32_t)(0x3<<24);     // for PB6 set  PushPull mode 50MHz (DC)
    GPIOB->CRL |= (uint32_t)(0x3<<16);     // for PB4 set  PushPull mode 50MHz (RST)
    GPIOB->CRL |= (uint32_t)(0x7<<20);     // for PB5 set  OpenDrain mode 50MHz (BL)
    // SOFT SPI Setup (PA4_SE, PA5_CLK,  PA7_DIO)
    GPIOA->CRL &= ~(uint32_t)(0xf<<16);    // clear mode for PA4   (SE)
    GPIOA->CRL &= ~(uint32_t)(0xf<<20);    // clear mode for PA5   (SPI_Clock)
    GPIOA->CRL &= ~(uint32_t)(0xf<<28);    // clear mode for PA7   (SPI_DIO)
    GPIOA->CRL |= (uint32_t)(0x3<<16);     // for PA4 set  PushPull mode 50MHz (SE)
#ifdef HW_SPI
    GPIOA->CRL |= (uint32_t)(0xb<<20);              // for PA5 set  Alternative mode, 50MHz
    GPIOA->CRL |= (uint32_t)(0xb<<28);              // for PA7 set  Alternative mode, 50MHz
#else
    GPIOA->CRL |= (uint32_t)(0x3<<20);     // for PA5 set  PushPull mode 50MHz (SPI_Clock)
    GPIOA->CRL |= (uint32_t)(0x3<<28);     // for PA7 set  PushPull mode 50MHz (SPI_DIO)
#endif
    // ------- SysTick CONFIG --------------
    if (SysTick_Config(72000)) // set 1ms
    {
        while(1); // error
    }
    // ------ TIM2 Setup -------------------
    TIM2->CCMR1 = TIM_CCMR1_CC1S_0 | TIM_CCMR1_CC2S_0;
    TIM2->CCER  = TIM_CCER_CC1P | TIM_CCER_CC2P;
    TIM2->SMCR  = TIM_SMCR_SMS_0 | TIM_SMCR_SMS_1;
    TIM2->ARR   = 1000;
    TIM2->CR1   = TIM_CR1_CEN;
    // ----- TIM4 Setup -------
    TIM4->CR1 = (0x80);                                 // set ARPE flag
    TIM4->PSC = 36000 - 1;                              // 1000 tick/sec
    TIM4->ARR = 100;                                    // 10 Interrupt/sec (1000/100)
    TIM4->DIER |=  TIM_DIER_UIE;                        // enable interrupt per overflow
    TIM4->SR = 0;
    TIM4->EGR |= (0x01);
    TIM4->CR1 |= TIM_CR1_CEN;                           // enable timer
    // --- UART setup ----
    //USART1->BRR  = 0x1d4c;                        // 9600 Baud, when APB2=72MHz
    USART1->BRR = 0x271;                            // 115200 Baud, when APB2=72MHz
    USART1->CR1 |= (USART_CR1_UE_Set | USART_Mode_Tx | USART_Mode_Rx | USART_CR1_RXNEIE);  // enable USART1, enable  TX/RX mode, enable RX interrupt
#ifdef HW_SPI
    // --- SPI1 setup ----
    SPI1->CR1  = (SPI_Mode_Master|SPI_DataSize_8b|SPI_NSS_Soft|SPI_BaudRatePrescaler_4);
#ifdef USE_DMA
    SPI1->CR2 |= SPI_CR2_TXDMAEN;           // enable DMA request
#endif
    SPI1->CR1 |= SPI_CR1_SPE;
#endif

    //---------- NVIC ----------------------
    NVIC_SetPriority(USART1_IRQn,1);                // set priority for USART1 IRQ
    NVIC_EnableIRQ(USART1_IRQn);                    // enable USART1 IRQ via NVIC
    NVIC_SetPriority(TIM4_IRQn, 21);                // set priority for TIM4_IRQ
    NVIC_EnableIRQ(TIM4_IRQn);                      // enable TIM4 IRQ
    //////////////////////////////////////
    /// let's go.....
    /////////////////////////////////////
    bool isEncoder=false;
    int enc_prev_value=0;
    uint8_t led_toggle=0;
    bool tick_enable=1;
    bool count_enable=true;
    uint8_t tick=0;
    uint8_t count=0;
    clear_uart_buf();
	println("ready...");
    pcd8544_init();
    for(;;){
        if (uart_ready) {
            if (uart_buf[0] == '?' && uart_buf[1] == 0x0) {
                print("help:\n");
                print("t-  disable print tick\n");
                print("on - turn on display\n");
                print("off  turn off display\n");
                print("test -  print on LCD text\n");
                print("fill - to fill LCD of pattern 0xAA\n");
                print("radio - show FM-radio interface\n");
                print("menu - show menu interface\n");
                print("count - enable increment count on LCD\n");
                print("mirror - rotate image on 180 deg\n");
            } else if (uart_buf[0] == 't' && uart_buf[1] == '-') {
                print("turn off tick\n");
                tick_enable=false;
            } else if (comp_str(uart_buf,"off") && isPower) {
                pcd8544_off();
                count_enable=false;
                isRadio=false;
            } else if (comp_str(uart_buf,"mirror") && isPower) {
                isMirror=!isMirror;
                if (isMirror)
                    print("mirror mode is ON\n");
                else
                    print("mirror mode is OFF\n");
                pcd8544_mirror();
                pcd8544_display_fb();
            }  else if (comp_str(uart_buf,"fill") && isPower) {
                pcd8544_clear();
                pcd8544_fill_fb(0xaa);
                pcd8544_display_fb();
                count_enable=false;
                isRadio=false;
                isMenu=false;
            }  else if (comp_str(uart_buf,"radio") && isPower) {
                fm_radio_interface(freq);
                count_enable=false;
                isRadio=true;
                isMenu=false;
            } else if (uart_buf[0] == 'o' && uart_buf[1] == 'n' && !isPower) {
                pcd8544_init();
                isRadio=false;
                isMenu=false;
            } else if (comp_str(uart_buf,"count") && isPower) {
                count_enable=true;
                isRadio=false;
                isMenu=false;
                tick=0;
            } else if(comp_str(uart_buf,"test") && isPower) {
                pcd8544_clear();
                lcd_print("Hello, World",0,1);
                lcd_print("++++++++++++",0,0);
                lcd_print("!!!!!!!!!!!!",0,2);
                //lcd_print_invert("testtesttest",0,3);
                lcd_print_invert("testtesttest",0,3);
                lcd_print("------------",0,4);
                lcd_print("Privet, MiR!",0,5);
                if (isMirror)
                    pcd8544_mirror();
                pcd8544_display_fb();
                count_enable=false;
                isRadio=false;
                isMenu=false;
            } else if(comp_str(uart_buf,"menu") && isPower) {
                main_menu(4);
                count_enable=isRadio=false;
                isMenu=true;
            } else {
                print("invalid command\n");
            }
            clear_uart_buf();
            uart_ready=0;
            uart_index=0;
        }

        int enc_value=TIM2->CNT;
        enc_value >>=2;
        if (enc_value != enc_prev_value) {
            enc_prev_value=enc_value;
            isEncoder=true;
            print_encoder("encoder: ",enc_value);
            tim4_counter=TIM4_INIT_VALUE;
            //-------- LCD ---------------
            if (enc_value >= 125) {
                enc_value -= 250;
            }
            if (!isRadio && !isMenu) {
                pcd8544_clear();
                if (enc_value < 0)
                    lcd_print_hugo('-',0,2);
                pcd8544_print_uint8_at(get_abs(enc_value),3,18,2);
                if (isMirror)
                    pcd8544_mirror();
#ifndef         USE_DMA
                pcd8544_display_fb();
#else
                pcd8544_DMA_UPD();
#endif
            } else if (isRadio) {
                radio_encoder_interface(enc_value);
            } else if (isMenu) {
                TIM2->CNT=0;
                if (enc_value)
                    main_menu_enc(enc_value);
            }

        } else if (isEncoder && !tim4_counter) {
            __disable_irq();
            isEncoder=false;
            if (isRadio) {
                int enc_value=TIM2->CNT;
                enc_value >>=2;
                if (enc_value >= 125) {
                    enc_value -= 250;
                }
                freq +=enc_value;
                fm_radio_interface(freq);
            } /*else if (isMenu) {
                int enc_value=TIM2->CNT;
                enc_value >>=2;
                if (enc_value >= 125) {
                    enc_value -= 250;
                }
                main_menu_enc(enc_value);
            }*/
            TIM2->CNT=0;
            __enable_irq();
            enc_prev_value=0;
            if (!isMenu) {
                print_encoder("total: ",enc_value);
            }
        }

        if ((++count % 20)  == 0) {
            if (led_toggle)
                gpio_set(GPIOC,LED);
            else
                gpio_reset(GPIOC,LED);
            led_toggle=!led_toggle;

            if (count_enable && !isEncoder) {
                pcd8544_clear();
                lcd_print_hugo('>',0,2);
                pcd8544_print_uint8_at(tick,3,18,2);
                if (isMirror)
                    pcd8544_mirror();
#ifndef         USE_DMA
                pcd8544_display_fb();
#else
                pcd8544_DMA_UPD();
#endif
                tick++;
            }

            if (tick_enable)
                print("tick..\n");
            count=0;
        }
        delay_ms(50);
    }
}

void clear_uart_buf() {
    for (uint8_t i=0; i<UART_BUFFER_LEN;i++)
        uart_buf[i]=0;
}

void print_encoder(char* str, int value) {
    if (value >= 125) {
        value -= 250;
    }
    print(str);
    uart_print_int(value);
    uart_send_char('\n');
}
