#ifndef __MENU_H__
#define __MENU_H__
#include "main.h"
void main_menu(uint8_t select);
void menu_inverse_line(uint8_t line);
void main_menu_enc(int offset);

#endif      // __MENU_H__
