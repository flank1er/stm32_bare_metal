#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "uart.h"
#include "task.h"

extern void delay(uint32_t ms);
void toggle_led();

int main()
{
    // enable GPIOC port
    RCC->APB2ENR |= RCC_APB2Periph_GPIOC;			// enable PORT_C
    RCC->APB2ENR |= RCC_APB2Periph_GPIOA;			// enable PORT_A
	RCC->APB2ENR |= RCC_APB2Periph_USART1; 			// enable UART1
    // --- GPIO setup ----
    GPIOC->CRH &= ~(uint32_t)(0xf<<20);
    GPIOC->CRH |=  (uint32_t)(0x2<<20);
	GPIOA->CRH &= ~(uint32_t)(0xf<<4);
	GPIOA->CRH |=  (uint32_t)(0xa<<4);
	// --- UART setup ----
    //USART1->BRR  = 0x1d4c;								// 9600 Baud, when APB2=72MHz
	USART1->BRR = 0x271;								// 115200 Baud, when APB2=72MHz
	USART1->CR1 |= USART_CR1_UE_Set | USART_Mode_Tx;	// enable USART1, enable  TX mode
	// Let's go..
	clear_task();
	add_task(toggle_led,TOP, LOOP, 1000);
    if (SysTick_Config(72000)) // set 1ms
    {
        while(1); // error
    }
	__enable_irq();
    for(;;){
		delay(1000);
		usart1_print_string("load cpu: ");
		usart1_print_number(get_load_cpu());
		usart1_send_char('\n');
	}
}

void toggle_led() {
	GPIOC->ODR ^= GPIO_Pin_13;
}
