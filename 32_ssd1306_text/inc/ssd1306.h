#ifndef __SSD1306_H__
#define __SSD1306_H__
#include <stdint.h>
#include "i2c.h"

#define SSD1306_I2C_ADDRESS (uint8_t)0x3C
#define ssd1306_clean() ssd1306_fill(0x0)

#define SSD1306_OK (uint8_t)0x01
#define SSD1306_ERROR (uint8_t)0x0

#define ssd1306_tunr_off i2c_send_cmd(0xAE)
#define ssd1306_print(x,y, z) ssd1306_print_str(x,y, z,false);
#define ssd1306_print8(x,y, z) ssd1306_print_str_8x8(x,y, z,false);
#define ssd1306_clear()  ssd1306_fill(0)

uint8_t ssd1306_init();
void ssd1306_mirror();
void ssd1306_refresh();
void ssd1306_fill(uint8_t value);
uint16_t ssd1306_set_cursor(uint8_t x, uint8_t y);
void ssd1306_print_str(char *str,uint8_t x, uint8_t y,bool inverse);
void ssd1306_print_str_8x8(char *str,uint8_t x, uint8_t y,bool inverse);
// cyberface fonts ////////
void ssd1306_cybercafe_digits_send(uint8_t ch);
void ssd1306_cybercafe_print_huge_uint8(uint8_t num, uint8_t x, uint8_t y);

#endif
