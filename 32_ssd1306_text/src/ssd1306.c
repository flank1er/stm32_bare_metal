#include "main.h"
#include "ssd1306.h"
#include "fonts.h"
#include "cp866.h"
#include "cybercafe.h"

#define ssd1306_buf_size 512
#define ssd1306_init_len 25

extern uint8_t lookup[];
volatile uint8_t ssd1306_buf[ssd1306_buf_size];
uint16_t  position;

const uint8_t  ssd1306_init_str[ssd1306_init_len] =
        {0xAE,0xD5,0x80,0xA8,0x1F,0xD3,0x00,0x00,0x8D,0x14,0x20,0x00,
         0xA1,0xC8,0xDA,0x02,0x81,0x8F,0xD9,0xF1,0xDB,0x40,0xA4,0xA6,0xAF};

uint8_t init_i2c(uint8_t adr, uint8_t value, uint8_t last);
void i2c_send_cmd(uint8_t cmd);
void i2c_send_data(uint8_t data);
void ssd1306_send_char5x7(uint8_t ch,bool inverse);
////////////////////////////////////////////////////
void ssd1306_send_char_8x8(uint8_t ch,bool inverse);

void ssd1306_mirror() {
    for (uint16_t i=0;i<ssd1306_buf_size;i++) {
        uint8_t b=ssd1306_buf[i];
        ssd1306_buf[i]=(lookup[b&0b1111] << 4) | lookup[b>>4];
    }
    for (uint16_t i=0,j=(ssd1306_buf_size-1);i<(ssd1306_buf_size>>1);i++,j--) {
            uint8_t b=ssd1306_buf[i];
            ssd1306_buf[i]=ssd1306_buf[ssd1306_buf_size-1-i];
            ssd1306_buf[j]=b;
        }
}

void ssd1306_print_str(char *str,uint8_t x, uint8_t y,bool inverse)
{
    position=ssd1306_set_cursor(x,y);
    while (*str)
    {
        ssd1306_send_char5x7(*str++,inverse);
    }
}

void ssd1306_print_str_8x8(char *str,uint8_t x, uint8_t y,bool inverse)
{
    position=ssd1306_set_cursor(x,y);
    while (*str)
    {
        ssd1306_send_char_8x8(*str++,inverse);
    }
}


uint16_t ssd1306_set_cursor(uint8_t x, uint8_t y) {
    position=(uint16_t)((y * 128) + x);
    return position;
}

void ssd1306_send_char5x7(uint8_t ch, bool inverse)
{
   if (ch >= 0x20 && ch <= 0xf0 && (position+7) < ssd1306_buf_size)
   {
      for (uint8_t i = 0; i < 5; i++)
      {
         uint8_t c=(ch<0xe0) ? ch - 0x20 : ch - 0x50;
         c=ASCII_5x7[c][i];
         if (!inverse)
            ssd1306_buf[position+i]=c;
         else
            ssd1306_buf[position+i]= ~c;
      }
      if (inverse) {
          ssd1306_buf[position+6]=0xff;
          ssd1306_buf[position+5]=0xff;
      } else {
          ssd1306_buf[position+6]=0x0;
          ssd1306_buf[position+5]=0x0;
      }
      position+=7;
   }
}

void ssd1306_send_char_8x8(uint8_t ch,bool inverse)
{
    if (position <= (ssd1306_buf_size-8))
    {
        for (uint8_t i=0; i < 8; i++)
        {
            uint8_t c=ch;
            c=CP866_8x8[c*8+i];
            if (inverse)
                ssd1306_buf[position+i] =~c;
            else
                ssd1306_buf[position+i] = c;

        }
        position+=8;
    }
}

void ssd1306_fill(uint8_t value) {
    for(uint16_t i=0; i<ssd1306_buf_size; i++)
        ssd1306_buf[i]=value;
    position=0;
}
///  Cybercafe font huge digits //////////////////////////////////
void ssd1306_cybercafe_digits_send(uint8_t ch)
{
    if (ch > 14 || position > 240)
        return;

    for (uint8_t i=0,j=0; i < 48; i+=3,j++)
    {
        uint8_t c=CYBERCAFE_HUGE_DIGITS[ch*48+i];
        ssd1306_buf[position+j] = c;

    }
    position+=128;
    for (uint8_t i=1,j=0; i < 48; i+=3,j++)
    {
        uint8_t c=CYBERCAFE_HUGE_DIGITS[ch*48+i];
        ssd1306_buf[position+j] = c;

    }

    position+=128;
    for (uint8_t i=2,j=0; i < 48; i+=3,j++)
    {
        uint8_t c=CYBERCAFE_HUGE_DIGITS[ch*48+i];
        ssd1306_buf[position+j] = c;

    }

}

void ssd1306_cybercafe_print_huge_uint8(uint8_t num, uint8_t x, uint8_t y){
    uint8_t sym[3];
    int8_t i=2;
    do  {
      if (num == 0 && i<2)
        sym[i]=10; // space
      else
        sym[i]=num%10;

      num=num/10;
      i--;

    } while (i>=0);

    uint8_t j=0;
    for (i=0;i<3;i++)
    {
        if (!(i<2 && sym[i] == 10))
        {

            position=(y*128) + x +j*18;
            ssd1306_cybercafe_digits_send(sym[i]);
            j++;
        }
    }
}

//////////////////////////////////////////////////////////////////

void ssd1306_turn_off(void)
{
    i2c_send_cmd(0xAE); // Set display OFF
}


uint8_t ssd1306_init() {
    uint8_t ret=SSD1306_OK;   
    for(uint8_t i=0; i<ssd1306_init_len; i++) {
        i2c_send_cmd(ssd1306_init_str[i]);
    }

    delay_ms(20);
    ssd1306_clean();
    ssd1306_refresh();
    return ret;
}

void ssd1306_refresh() {
    i2c_send_cmd(0x22); // Page Start Address
    i2c_send_cmd(0x0);
    i2c_send_cmd(0xFF);
    i2c_send_cmd(0x21);  //Column start address
    i2c_send_cmd(0x0);
    i2c_send_cmd(127);

    enable_i2c;
    if (init_i2c((SSD1306_I2C_ADDRESS<<1),0x40,NOLAST) == 0 ) {
       for(uint16_t i=0; i < ssd1306_buf_size; i++) {
           I2C1->DR=ssd1306_buf[i];
           while(!(I2C1->SR1 & I2C_FLAG_BTF));     // wait BFT
       }
       stop_i2c;
    }
    disable_i2c;
}

void i2c_send_cmd(uint8_t cmd) {
    enable_i2c;
    if (init_i2c((SSD1306_I2C_ADDRESS<<1), 0x0, NOLAST) == 0) {
        I2C1->DR=cmd;
        while(!(I2C1->SR1 & I2C_FLAG_BTF));     // wait BFT
        I2C1->CR1 |= I2C_CR1_STOP_Set;          // Program the STOP bit
        while (I2C1->CR1 & I2C_CR1_STOP_Set);   // Wait until STOP bit is cleared by hardware
    }  else
        stop_i2c;
    disable_i2c;
}

void i2c_send_data(uint8_t data) {
    if (init_i2c((SSD1306_I2C_ADDRESS<<1), 0x40, NOLAST) == 0) {
        I2C1->DR=data;
        while(!(I2C1->SR1 & I2C_FLAG_BTF));     // wait BFT
        I2C1->CR1 |= I2C_CR1_STOP_Set;          // Program the STOP bit
        while (I2C1->CR1 & I2C_CR1_STOP_Set);   // Wait until STOP bit is cleared by hardware
    }  else
        stop_i2c;
}



uint8_t init_i2c(uint8_t adr, uint8_t value, uint8_t last) {
    I2C1->CR1 |= I2C_CR1_START_Set;             //=0x0100, START
    while (!(I2C1->SR1 & I2C_FLAG_SB));         // wait SB
    (void) I2C1->SR1;
    I2C1->DR=adr;                               // send ADDR
    while (!(I2C1->SR1 & I2C_FLAG_ADDR))        // wait ADDR
    {
        if(I2C1->SR1 & I2C_IT_AF)               // if NACK
            return 1;
    }
    (void) I2C1->SR1;                           // clear ADDR
    (void) I2C1->SR2;                           // clear ADDR
    I2C1->DR=value;
    if (last == LAST) {
        while(!(I2C1->SR1 & I2C_FLAG_BTF));     // wait BFT
        I2C1->CR1 |= I2C_CR1_STOP_Set;          // Program the STOP bit
        while (I2C1->CR1 & I2C_CR1_STOP_Set);   // Wait until STOP bit is cleared by hardware
    } else {
        while(!(I2C1->SR1 & I2C_FLAG_TXE));     // wait TXE bit
    }
    return 0;
}

///
/*
uint8_t ssd1306_init(void)
{
    uint8_t ret;

    ret=ssd1306_send_command(0xAE); // Set display OFF

    ret&=ssd1306_send_command(0xD5); // Set Display Clock Divide Ratio / OSC Frequency
    ret&=ssd1306_send_command(0x80); // Display Clock Divide Ratio / OSC Frequency

    ret&=ssd1306_send_command(0xA8); // Set Multiplex Ratio
    ret&=ssd1306_send_command(0x1F); // Multiplex Ratio for 128x32 (32-1)

    ret&=ssd1306_send_command(0xD3); // Set Display Offset
    ret&=ssd1306_send_command(0x00); // Display Offset

    ret&=ssd1306_send_command(0x0); // Set Display Start Line

    ret&=ssd1306_send_command(0x8D); // Set Charge Pump
    ret&=ssd1306_send_command(0x14); // Charge Pump (0x10 External, 0x14 Internal DC/DC)

    ret&=ssd1306_send_command(0x20); // Set Memory Mode
    ret&=ssd1306_send_command(0x00); // like ks0108

    //ret&=ssd1306_send_command(0xA0); // Horiontal Mirror
    ret&=ssd1306_send_command(0xA1); // Set Segment Re-Map
    ret&=ssd1306_send_command(0xC8); // Set Com Output Scan Direction

    ret&=ssd1306_send_command(0xDA); // Set COM Hardware Configuration
    ret&=ssd1306_send_command(0x02); // COM Hardware Configuration

    ret&=ssd1306_send_command(0x81); // Set Contrast
    ret&=ssd1306_send_command(0x8F); // Contrast

    ret&=ssd1306_send_command(0xD9); // Set Pre-Charge Period
    ret&=ssd1306_send_command(0xF1); // Set Pre-Charge Period (0x22 External, 0xF1 Internal)

    ret&=ssd1306_send_command(0xDB); // Set VCOMH Deselect Level
    ret&=ssd1306_send_command(0x40); // VCOMH Deselect Level

    ret&=ssd1306_send_command(0xA4); // Set all pixels OFF
    ret&=ssd1306_send_command(0xA6); // Set display not inverted
    //ret&=ssd1306_send_command(0x2E); // Deactivate Scroll
    ret&=ssd1306_send_command(0xAF); // Set display On

    return ret;
}

*/

