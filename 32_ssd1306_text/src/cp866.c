#include "cp866.h"
#include "font5x7.h"
#include "cybercafe.h"
#include "ste2007.h"
#include "uart.h"

extern uint8_t fb[];
extern uint16_t pos;
extern bool isMirror;

const uint8_t stereo[7]={0x3c,0x66,0xc3,0x81,0xc3,0x66,0x3c};
// radio interface variables //////
bool isScanning=false;
bool preset_mode=true;
bool isDecodeRDS=false;
uint8_t ps_stat=12;
uint8_t rssi=78;
////////////////////////////////

void cp866_send_char_size3_OR(uint8_t ch, uint8_t x, uint8_t y);
void cp866_send_char_split(uint8_t ch,bool inverse);
void cp866_send_icon(uint8_t ch);

void cp866_set_pos(uint8_t x, uint8_t y) {
    pos=(y*LCD_X) + (x*FONT_CP866_8_CHAR_WIDTH);
}

void cp866_print_str_inverse_8x8(char *str, uint8_t x, uint8_t y)
{
    pos=y*LCD_X + x;
    while (*str)
    {
      cp866_send_char_8x8(*str++,true);
    }
}


void cp866_print_str_8x8(char *str, uint8_t x, uint8_t y)
{
    pos=y*LCD_X + x;
    while (*str)
    {
      cp866_send_char_8x8(*str++,false);
    }
}


void cp866_send_char_8x8(uint8_t ch,bool inverse)
{
    if (pos <= (LCD_LEN-8))
    {
        for (uint8_t i=0; i < 8; i++)
        {
            uint8_t c=ch;
            c=CP866_8x8[c*8+i];
            if (inverse)
                fb[pos+i] =~c;
            else
                fb[pos+i] = c;

        }
        pos+=8;
    }
}

void cp866_send_icon(uint8_t ch)
{
    if (pos <= (LCD_LEN-8))
    {
        for (uint8_t i=0; i < 8; i++)
        {
            uint8_t c=ch;
            c=my_pics[c*8+i];
            fb[pos+i] = c;
        }
        pos+=8;
    }
}


void cp866_send_char_split(uint8_t ch,bool inverse)
{
    if (pos <= (LCD_LEN-8))
    {
        for (uint8_t i=0; i < 8; i++)
        {
            uint8_t c=ch;
            c=CP866_8x8[c*8+i];
            if (inverse)
                fb[pos+i] =~c;
            else {
                fb[pos+i] = ((c & 0x0f) <<4);
                fb[pos+i+96] = (c>>4);
            }

        }
        pos+=8;
    }
}



///////// SCALE FONT /////////////////////////

void cp866_send_char_size2_fb(uint8_t ch, uint8_t x, uint8_t y) {
    uint8_t s[8]; // source
    uint8_t r[32]; // result
    uint8_t i,j;
   // get littera
    for (i=0; i < 8; i++)
    {
          s[i]=CP866_8x8[(ch*8)+i];;
    }
    // scale
    for(i=0;i<8;i++)
    {
        uint8_t a=0;
        for(j=0;j<4;j++)
        {
            uint8_t b=(s[i]>>j) & 0x01;
            a|=(b<<(j<<1)) | (b<<((j<<1)+1));
        }
        r[(i<<1)]=a;
        r[(i<<1)+1]=a;
    }

    for(i=0;i<8;i++)
    {
        uint8_t a=0;
        for(j=0;j<4;j++)
        {
            uint8_t b=(s[i]>>(j+4)) & 0x01;
            a|=(b<<(j<<1)) | (b<<((j<<1)+1));
        }
        r[(i<<1)+16]=a;
        r[(i<<1)+17]=a;
    }
    // print
    pos=y*LCD_X+x;
    if (pos<(LCD_LEN-14))
    {
        //fb[pos++]=0x00; fb[pos++]=0x00;
        for(i=0;i<16;i++)
            fb[pos++]=r[i];

        //fb[pos++]=0x00; fb[pos++]=0x00;
    };

    pos=(y+1)*LCD_X+x;
    if(pos<(LCD_LEN-14))
    {
        //fb[pos++]=0x00; fb[pos++]=0x00;
        for(i=16;i<32;i++)
            fb[pos++]=r[i];
        //fb[pos++]=0x00; fb[pos++]=0x00;
    }
}

void cp866_send_char_size3_fb(uint8_t ch, uint8_t x, uint8_t y) {
    uint8_t s[8]; // source
    uint8_t r[72]; // result
    uint8_t i;
    // get littera
    for (i=0; i < 8; i++)
    {
          s[i]=CP866_8x8[(ch*8)+i];;
    }
    // scale
    for(i=0;i<8;i++)
    {
        uint8_t b,a;
        b=(s[i] & 0x01);
        a=(b) ? 0x7 : 0;
        b=(s[i]>>1) & 0x01;
        if (b) a|=0x38;
        b=(s[i]>>2) & 0x01;
        a|=(b<<6)|(b<<7);

        r[i*3]=a;
        r[i*3+1]=a;
        r[i*3+2]=a;

        r[i*3+24]=b;
        r[i*3+25]=b;
        r[i*3+26]=b;
    }

    for(i=0;i<8;i++)
    {
        uint8_t b,a;
        b=(s[i]>>3) & 0x01;
        a=(b) ? 0x0e : 0;
        b=(s[i]>>4) & 0x01;
        if (b) a|=0x70;
        b=(s[i]>>5) & 0x01;
        a|=(b<<7);

        r[i*3+24]|=a;
        r[i*3+25]|=a;
        r[i*3+26]|=a;
     }

    for(i=0;i<8;i++)
    {
        uint8_t b,a;
        b=(s[i]>>5) & 0x01;
        a=(b) ? 0x3 : 0;
        b=(s[i]>>6) & 0x01;
        if (b) a|=0x1c;
        b=(s[i]>>7) & 0x01;
        if (b) a|=0xe0;

        r[i*3+48]=a;
        r[i*3+49]=a;
        r[i*3+50]=a;
     }

    // print
    pos=y*LCD_X+x;
    //fb[pos++]=0;fb[pos++]=0; fb[pos++]=0;
    for(i=0;i<24;i++) fb[pos++]=r[i];
    //fb[pos++]=0;fb[pos++]=0; fb[pos++]=0;

    pos=(y+1)*LCD_X+x;
    //fb[pos++]=0;fb[pos++]=0; fb[pos++]=0;
    for(i=24;i<48;i++) fb[pos++]=r[i];
    //fb[pos++]=0;fb[pos++]=0; fb[pos++]=0;

    pos=(y+2)*LCD_X+x;
    //fb[pos++]=0;fb[pos++]=0; fb[pos++]=0;
    for(i=48;i<72;i++) fb[pos++]=r[i];
    //fb[pos++]=0;fb[pos++]=0; fb[pos++]=0;
}

void cp866_send_char_size3_OR(uint8_t ch, uint8_t x, uint8_t y) {
    uint8_t s[8]; // source
    uint8_t r[72]; // result
    uint8_t i;
    // get littera
    for (i=0; i < 8; i++)
    {
          s[i]=CP866_8x8[(ch*8)+i];;
    }
    // scale
    for(i=0;i<8;i++)
    {
        uint8_t b,a;
        b=(s[i] & 0x01);
        a=(b) ? 0x7 : 0;
        b=(s[i]>>1) & 0x01;
        if (b) a|=0x38;
        b=(s[i]>>2) & 0x01;
        a|=(b<<6)|(b<<7);

        r[i*3]=a;
        r[i*3+1]=a;
        r[i*3+2]=a;

        r[i*3+24]=b;
        r[i*3+25]=b;
        r[i*3+26]=b;
    }

    for(i=0;i<8;i++)
    {
        uint8_t b,a;
        b=(s[i]>>3) & 0x01;
        a=(b) ? 0x0e : 0;
        b=(s[i]>>4) & 0x01;
        if (b) a|=0x70;
        b=(s[i]>>5) & 0x01;
        a|=(b<<7);

        r[i*3+24]|=a;
        r[i*3+25]|=a;
        r[i*3+26]|=a;
     }

    for(i=0;i<8;i++)
    {
        uint8_t b,a;
        b=(s[i]>>5) & 0x01;
        a=(b) ? 0x3 : 0;
        b=(s[i]>>6) & 0x01;
        if (b) a|=0x1c;
        b=(s[i]>>7) & 0x01;
        if (b) a|=0xe0;

        r[i*3+48]=a;
        r[i*3+49]=a;
        r[i*3+50]=a;
     }

    // print
    pos=y*LCD_X+x;
    //fb[pos++]=0;fb[pos++]=0; fb[pos++]=0;
    for(i=0;i<24;i++) fb[pos++] |=r[i];
    //fb[pos++]=0;fb[pos++]=0; fb[pos++]=0;

    pos=(y+1)*LCD_X+x;
    //fb[pos++]=0;fb[pos++]=0; fb[pos++]=0;
    for(i=24;i<48;i++) fb[pos++] |=r[i];
    //fb[pos++]=0;fb[pos++]=0; fb[pos++]=0;

    pos=(y+2)*LCD_X+x;
    //fb[pos++]=0;fb[pos++]=0; fb[pos++]=0;
    for(i=48;i<72;i++) fb[pos++] |=r[i];
    //fb[pos++]=0;fb[pos++]=0; fb[pos++]=0;
}
void cp866_print_uint8_at(uint8_t num, uint8_t size, uint8_t x, uint8_t y){
    uint8_t sym[3];
    int8_t i=2;
    do  {
      if (num == 0 && i<2)
        sym[i]=0x20; // space
      else
        sym[i]=0x30+num%10;

      num=num/10;
      i--;

    } while (i>=0);

    uint8_t j=0;
    for (i=0;i<3;i++)
    {
        if (!(i<2 && sym[i] == 0x20))
        {
            switch(size) {
            case 3:
                //ste2007_set_graph_pos(x+j*9*size,y);
                //nums_send(sym[i]);
                cp866_send_char_size3_fb(sym[i],x+j*9*size,y);
                break;
            case 2:
                cp866_send_char_size2_fb(sym[i],x+j*7*size,y);
                break;
            default:
                cp866_set_pos(x+j,y);
                cp866_send_char_8x8(sym[i],false);
                break;
            }
            j++;
        }
    }
}



void cp866_print_split_num(uint8_t num, uint8_t x, uint8_t y){
    uint8_t sym[3];
    int8_t i=2;
    do  {
      if (num == 0 && i<2)
        sym[i]=0x20; // space
      else
        sym[i]=0x30+num%10;

      num=num/10;
      i--;

    } while (i>=0);

    uint8_t j=0;
    for (i=0;i<3;i++)
    {
        if (!(i<2 && sym[i] == 0x20))
        {
            ste2007_set_graph_pos(x+j*8,y);
            cp866_send_char_split(sym[i],false);
            j++;
        }
    }
}

void cp866_print_num(uint8_t num, uint8_t x, uint8_t y){
    uint8_t sym[3];
    int8_t i=2;
    do  {
      if (num == 0 && i<2)
        sym[i]=0x20; // space
      else
        sym[i]=0x30+num%10;

      num=num/10;
      i--;

    } while (i>=0);

    uint8_t j=0;
    for (i=0;i<3;i++)
    {
        if (!(i<2 && sym[i] == 0x20))
        {

            ste2007_set_graph_pos(x+j*8,y);
            cp866_send_char_8x8(sym[i],false);
            j++;
        }
    }
}

void cp866_print_at_fb(char *str, uint8_t size,  uint8_t x, uint8_t y)
{
    uint8_t i=0;
    pos=y*LCD_X+x;     
    switch (size) {
      case 3:
          while (*str)
          {
             cp866_send_char_size3_fb(*str++,x+i*8*size,y);
             ++i;
          }
          break;
      case 2:
          while (*str)
          {
            cp866_send_char_size2_fb(*str++,x+i*9*size,y);
            ++i;
          }
          break;
      default:
          while (*str)
          {
            cp866_send_char_8x8(*str++,false);
          }
          break;
   }
}

///////// FM-RADIO INTERFACE ///////////////////

void fm_radio_interface(uint16_t freq) {
    //fr=freq;
    ste2007_clear();
    // frequent
    uint8_t f1=(uint8_t)(freq/10);
    uint8_t f2=(uint8_t)(freq%10);
    if (f1>=100) {
        cp866_send_char_size3_fb('1',0,2);
        f1 -= 100;
        if (f1<10) {
            cp866_send_char_size3_fb('0',20,2);
            cp866_send_char_size3_fb(f1+0x30,44,2);
        } else {
            cp866_send_char_size3_fb('1',20,2);
            cp866_send_char_size3_fb((f1-10)+0x30,44,2);
            //cp866_print_uint8_at(f1,FONT_SIZE_3,20,2);
        }
    } else {
        cp866_print_uint8_at(f1,FONT_SIZE_3,20,2);
        //cristal_print_uint8(f1,40,3);
    }

    //cristal_print_uint8(f2,70,3);
    cp866_print_uint8_at(f2,FONT_SIZE_2,75,3);
    // dot
    //font_ascii_5x7_print_str(".",68,4);
    //ste2007_set_graph_pos(52,3);
    //nums_send(0xa+0x30);
    cp866_send_char_size3_OR('.',59,2);

    // MHz
    //font_ascii_5x7_print_str("\x8c\x83\xe6",75,2);
    //cp866_print_at_fb("\x8c\x83\xe6",FONT_SIZE_1,69,2);
    //cp866_print_at_fb("MHz",FONT_SIZE_1,69,2);

    // draw scale
    for (uint8_t i=0; i<96; i+=2) {
        fb[768+i]=0xff;
        fb[768+i+1]=0x0;
    }

    for (uint16_t i=freq-24, j=0;j<96; i++,j+=2) {
        //fb[672+j] = (!(i%10)) ? 0xff : 0xf0;

        if (((i)%10) == 0) {
             fb[672+j] =0xff;
             if (j>=8 && j <90) {
                 cp866_print_num((i/10),j-8,6);
             }
            //uart_print_str("freq: ");
            //uart_print_num(i);
            //uart_send_char('\n');
        } else if (((i)%5) == 0) {
            fb[672+j] =0xf8;
        } else {
            fb[672+j] =0xc0;
        }
    }
    // draw cursor
    ste2007_set_graph_pos(45,7);
    cp866_send_char_8x8(0x1f,false);
    //cp866_send_char_8x8()
    //fb[624]=0xff;fb[625]=0xff;fb[626]=0xff;
   // fb[720]=0xff;fb[719]=0xff;fb[721]=0xff;


    if (!isScanning) {
        // draw RSSI level
        uint8_t r1 = rssi & 0x3f;
        r1 = r1 << 2;
        uint8_t r2=6;
        while (r2) {
            if (r1 & 0x80)
                break;
            else
                --r2;
            r1=r1<<1;

        }
        r2=r2<<1;

        uint8_t r=0x80;
        for (uint16_t i=82;i<82+r2;i+=2) {
            r = r>>1;
            r |= 0x80;
            fb[i]=r;
        }

        //cp866_print_uint8_at(rssi,FONT_SIZE_1,8,0);
        //cp866_send_char_8x8(0xa4,false);
        //cp866_send_char_8x8(0x81,false);
        //cp866_print_at_fb("\x8c\x83\xe6",FONT_SIZE_1,74,2);

        // draw volume
        cp866_print_at_fb("\x85\xa2\xe0\xae",FONT_SIZE_1,0,0);
        //cp866_print_at_fb("\x83\xe0",FONT_SIZE_1,47,0);
        //cp866_print_uint8_at((uint8_t)15,FONT_SIZE_1,8,0);
        ste2007_draw_icon((char*)&stereo,35,0,1);
        ste2007_draw_icon((char*)&stereo,41,0,1);
        ste2007_set_graph_pos(58,0);
        cp866_send_icon(0x0);
        cp866_send_icon(0x5);
        //cp866_print_at_fb("RADIO TEXT",FONT_SIZE_1,8,5);
        // radiotext

        if(preset_mode && !isDecodeRDS) {
            cp866_print_split_num(ps_stat, 75,1);
            /*
            if (ps_stat >= 10)
                cp866_print_at_fb("\x2d\xef\x20\xe1\xe2\xa0\xad\xe6\xa8\xef",FONT_SIZE_1,16,1);
            else
                cp866_print_at_fb("\x2d\xef\x20\xe1\xe2\xa0\xad\xe6\xa8\xef",FONT_SIZE_1,8,1);
               */
        }


    } else {
        cp866_print_at_fb("\xe1\xaa\xa0\xad\xa8\xe0\xae\xa2\xa0\xad\xa8\xa5",FONT_SIZE_1,0,4);  // "сканирование"
    }


/*    pcd8544_print_at_fb("MM", FONT_SIZE_1,18,0);
    pcd8544_print_at_fb("60dB", FONT_SIZE_1,36,0);
    //pcd8544_print_at_fb("7th-STATION", FONT_SIZE_1,3,4);
    //pcd8544_print_at_fb("vol-", FONT_SIZE_1,0,5);
    //pcd8544_print_uint8_at(11, FONT_SIZE_1,18,5);


    pcd8544_draw_line(0,46,83,46);
    for(uint8_t j=0;j<14;j++)
        pcd8544_draw_line(j*6,44,j*6,47);
    pcd8544_draw_line(83,44,83,47);

    char * ptr= (char *)(&pointer);
    uint8_t p=(freq-880)/2-(freq-880)/10;
    pcd8544_draw_icon_fb(ptr,p,5,1);

    ptr= (char *)(&stereo);
    pcd8544_draw_icon_fb(ptr,0,0,1);
    pcd8544_draw_icon_fb(ptr,7,0,1);

    ptr= (char *)(&battery);
    pcd8544_draw_icon_fb(ptr,70,0,2);
*/
    if (isMirror)
        ste2007_mirror();

    ste2007_display_fb();
}

void fm_radio_interface2(uint16_t freq) {
    //fr=freq;
    ste2007_clear();
    // frequent
    uint8_t f1=(uint8_t)(freq/10);
    uint8_t f2=(uint8_t)(freq%10);
    if (f1>=100) {
        ste2007_set_graph_pos(10,2);
        nums_send(0x1+0x30);
        //cp866_send_char_size3_fb('1',0,2);
        f1 -= 100;
        if (f1<10) {
            ste2007_set_graph_pos(28,2);
            nums_send(0x30);
            ste2007_set_graph_pos(43,2);
            nums_send(f1+0x30);
            ///cp866_send_char_size3_fb('0',20,2);
            //cp866_send_char_size3_fb(f1+0x30,44,2);
        }
/*        } else {
            cp866_send_char_size3_fb('1',20,2);
            cp866_send_char_size3_fb((f1-10)+0x30,44,2);
            //cp866_print_uint8_at(f1,FONT_SIZE_3,20,2);
        }
*/
    } else {
        //cp866_print_uint8_at(f1,FONT_SIZE_3,20,2);
        cristal_print_uint8(f1,30,2);
    }

    //cristal_print_uint8(f2,50,2);
    //cp866_print_uint8_at(f2,FONT_SIZE_2,75,3);
    // dot
    //font_ascii_5x7_print_str(".",60,3);
    ste2007_set_graph_pos(58,3);
    font_ascii_5x7_send('.',false);
    //nums_send(0xa+0x30);
    //cp866_send_char_size3_OR('.',59,2);
     cristal_print_uint8(f2,60,2);

    // MHz
    //font_ascii_5x7_print_str("\x8c\x83\xe6",75,2);
    //cp866_print_at_fb("\x8c\x83\xe6",FONT_SIZE_1,69,2);
    //cp866_print_at_fb("MHz",FONT_SIZE_1,69,2);

    // draw scale
    for (uint8_t i=0; i<96; i+=2) {
        fb[768+i]=0xff;
        fb[768+i+1]=0x0;
    }

    for (uint16_t i=freq-24, j=0;j<96; i++,j+=2) {
        //fb[672+j] = (!(i%10)) ? 0xff : 0xf0;

        if (((i)%10) == 0) {
             fb[672+j] =0xff;
             if (j>=8 && j <90) {
                 if ((i/10) < 100) {
                    font_ascii_5x7_print_uint8((i/10),FONT_SIZE_1, j-6,6);
                 } else if (!((i/10)&0x01)){
                     font_ascii_5x7_print_uint8((i/10),FONT_SIZE_1, j-10,6);
                 }
                 //font_ascii_5x7_print_uint8((i/10),FONT_SIZE_1, j-8,6);
             }
            //uart_print_str("freq: ");
            //uart_print_num(i);
            //uart_send_char('\n');
        } else if (((i)%5) == 0) {
            fb[672+j] =0xf8;
        } else {
            fb[672+j] =0xc0;
        }
    }
    // draw cursor
    ste2007_set_graph_pos(45,7);
    cp866_send_char_8x8(0x1f,false);
    //cp866_send_char_8x8()
    //fb[624]=0xff;fb[625]=0xff;fb[626]=0xff;
   // fb[720]=0xff;fb[719]=0xff;fb[721]=0xff;


    if (!isScanning) {
        // draw RSSI level
        uint8_t r1 = rssi & 0x3f;
        r1 = r1 << 2;
        uint8_t r2=6;
        while (r2) {
            if (r1 & 0x80)
                break;
            else
                --r2;
            r1=r1<<1;

        }
        r2=r2<<1;

        uint8_t r=0x80;
        for (uint16_t i=82;i<82+r2;i+=2) {
            r = r>>1;
            r |= 0x80;
            fb[i]=r;
        }

        //cp866_print_uint8_at(rssi,FONT_SIZE_1,8,0);
        //cp866_send_char_8x8(0xa4,false);
        //cp866_send_char_8x8(0x81,false);
        //cp866_print_at_fb("\x8c\x83\xe6",FONT_SIZE_1,74,2);

        // draw volume
        font_ascii_5x7_print_str("\x85\xa2\xe0\xae",0,0);
        //cp866_print_at_fb("\x85\xa2\xe0\xae",FONT_SIZE_1,0,0);
        //cp866_print_at_fb("\x83\xe0",FONT_SIZE_1,47,0);
        //cp866_print_uint8_at((uint8_t)15,FONT_SIZE_1,8,0);
        ste2007_draw_icon((char*)&stereo,35,0,1);
        ste2007_draw_icon((char*)&stereo,41,0,1);
        ste2007_set_graph_pos(58,0);
        cp866_send_icon(0x0);
        cp866_send_icon(0x5);
        font_ascii_5x7_print_str("RADIO TEXT",8,5);
        // radiotext

        if(preset_mode && !isDecodeRDS) {
            font_ascii_5x7_print_uint8(ps_stat,FONT_SIZE_1,80,3);
            //cp866_print_split_num(ps_stat, 75,1);
            //font_ascii_5x7_print_uint8(ps_stat,FONT_SIZE_1, 75,3);
            /*
            if (ps_stat >= 10)
                cp866_print_at_fb("\x2d\xef\x20\xe1\xe2\xa0\xad\xe6\xa8\xef",FONT_SIZE_1,16,1);
            else
                cp866_print_at_fb("\x2d\xef\x20\xe1\xe2\xa0\xad\xe6\xa8\xef",FONT_SIZE_1,8,1);
               */
        }


    } else {
        cp866_print_at_fb("\xe1\xaa\xa0\xad\xa8\xe0\xae\xa2\xa0\xad\xa8\xa5",FONT_SIZE_1,0,4);  // "сканирование"
    }


/*    pcd8544_print_at_fb("MM", FONT_SIZE_1,18,0);
    pcd8544_print_at_fb("60dB", FONT_SIZE_1,36,0);
    //pcd8544_print_at_fb("7th-STATION", FONT_SIZE_1,3,4);
    //pcd8544_print_at_fb("vol-", FONT_SIZE_1,0,5);
    //pcd8544_print_uint8_at(11, FONT_SIZE_1,18,5);


    pcd8544_draw_line(0,46,83,46);
    for(uint8_t j=0;j<14;j++)
        pcd8544_draw_line(j*6,44,j*6,47);
    pcd8544_draw_line(83,44,83,47);

    char * ptr= (char *)(&pointer);
    uint8_t p=(freq-880)/2-(freq-880)/10;
    pcd8544_draw_icon_fb(ptr,p,5,1);

    ptr= (char *)(&stereo);
    pcd8544_draw_icon_fb(ptr,0,0,1);
    pcd8544_draw_icon_fb(ptr,7,0,1);

    ptr= (char *)(&battery);
    pcd8544_draw_icon_fb(ptr,70,0,2);
*/
    if (isMirror)
        ste2007_mirror();

    ste2007_display_fb();
}

void fm_radio_interface3(uint16_t freq) {
    //fr=freq;
    ste2007_clear();
    // frequent
    uint8_t f1=(uint8_t)(freq/10);
    uint8_t f2=(uint8_t)(freq%10);
    cybercafe_print_large_uint8(f1,18,2);
/*
    if (f1>=100) {
        cp866_send_char_size3_fb('1',0,2);
        f1 -= 100;
        if (f1<10) {
            cp866_send_char_size3_fb('0',20,2);
            cp866_send_char_size3_fb(f1+0x30,44,2);
        } else {
            cp866_send_char_size3_fb('1',20,2);
            cp866_send_char_size3_fb((f1-10)+0x30,44,2);
            //cp866_print_uint8_at(f1,FONT_SIZE_3,20,2);
        }
    } else {
        cp866_print_uint8_at(f1,FONT_SIZE_3,20,2);
        //cristal_print_uint8(f1,40,3);
    }
*/
    //cristal_print_uint8(f2,70,3);
    cybercafe_print_uint8(f2,63,3);
    //cp866_print_uint8_at(f2,FONT_SIZE_2,75,3);
    // dot
    //font_ascii_5x7_print_str(".",68,4);
    //ste2007_set_graph_pos(52,3);
    //nums_send(0xa+0x30);
    cp866_send_char_size3_OR('.',59,2);

    // MHz
    //font_ascii_5x7_print_str("\x8c\x83\xe6",75,2);
    //cp866_print_at_fb("\x8c\x83\xe6",FONT_SIZE_1,69,2);
    //cp866_print_at_fb("MHz",FONT_SIZE_1,69,2);

    // draw scale
    for (uint8_t i=0; i<96; i+=2) {
        fb[768+i]=0xff;
        fb[768+i+1]=0x0;
    }

    for (uint16_t i=freq-24, j=0;j<96; i++,j+=2) {
        //fb[672+j] = (!(i%10)) ? 0xff : 0xf0;

        if (((i)%10) == 0) {
             fb[672+j] =0xff;
             if (j>=8 && j <90) {
                 //cp866_print_num((i/10),j-8,6);
                 if ((i/10) < 100) {
                    cp866_print_num((i/10),j-8,6);
                 } else if (!((i/10)&0x01)){
                     cp866_print_num((i/10),j-11,6);
                 }
             }
            //uart_print_str("freq: ");
            //uart_print_num(i);
            //uart_send_char('\n');
        } else if (((i)%5) == 0) {
            fb[672+j] =0xf8;
        } else {
            fb[672+j] =0xc0;
        }
    }
    // draw cursor
    ste2007_set_graph_pos(45,7);
    cp866_send_char_8x8(0x1f,false);
    //cp866_send_char_8x8()
    //fb[624]=0xff;fb[625]=0xff;fb[626]=0xff;
   // fb[720]=0xff;fb[719]=0xff;fb[721]=0xff;


    if (!isScanning) {
        // draw RSSI level
        uint8_t r1 = rssi & 0x3f;
        r1 = r1 << 2;
        uint8_t r2=6;
        while (r2) {
            if (r1 & 0x80)
                break;
            else
                --r2;
            r1=r1<<1;

        }
        r2=r2<<1;

        uint8_t r=0x80;
        for (uint16_t i=82;i<82+r2;i+=2) {
            r = r>>1;
            r |= 0x80;
            fb[i]=r;
        }

        //cp866_print_uint8_at(rssi,FONT_SIZE_1,8,0);
        //cp866_send_char_8x8(0xa4,false);
        //cp866_send_char_8x8(0x81,false);
        //cp866_print_at_fb("\x8c\x83\xe6",FONT_SIZE_1,74,2);

        // draw volume
        cp866_print_at_fb("\x85\xa2\xe0\xae",FONT_SIZE_1,0,0);
        //cp866_print_at_fb("\x83\xe0",FONT_SIZE_1,47,0);
        //cp866_print_uint8_at((uint8_t)15,FONT_SIZE_1,8,0);
        ste2007_draw_icon((char*)&stereo,35,0,1);
        ste2007_draw_icon((char*)&stereo,41,0,1);
        ste2007_set_graph_pos(58,0);
        cp866_send_icon(0x0);
        cp866_send_icon(0x5);
        //cp866_print_at_fb("RADIO TEXT",FONT_SIZE_1,8,5);
        // radiotext

        if(preset_mode && !isDecodeRDS) {
            cp866_print_split_num(ps_stat, 75,1);
            /*
            if (ps_stat >= 10)
                cp866_print_at_fb("\x2d\xef\x20\xe1\xe2\xa0\xad\xe6\xa8\xef",FONT_SIZE_1,16,1);
            else
                cp866_print_at_fb("\x2d\xef\x20\xe1\xe2\xa0\xad\xe6\xa8\xef",FONT_SIZE_1,8,1);
               */
        }


    } else {
        cp866_print_at_fb("\xe1\xaa\xa0\xad\xa8\xe0\xae\xa2\xa0\xad\xa8\xa5",FONT_SIZE_1,0,4);  // "сканирование"
    }


/*    pcd8544_print_at_fb("MM", FONT_SIZE_1,18,0);
    pcd8544_print_at_fb("60dB", FONT_SIZE_1,36,0);
    //pcd8544_print_at_fb("7th-STATION", FONT_SIZE_1,3,4);
    //pcd8544_print_at_fb("vol-", FONT_SIZE_1,0,5);
    //pcd8544_print_uint8_at(11, FONT_SIZE_1,18,5);


    pcd8544_draw_line(0,46,83,46);
    for(uint8_t j=0;j<14;j++)
        pcd8544_draw_line(j*6,44,j*6,47);
    pcd8544_draw_line(83,44,83,47);

    char * ptr= (char *)(&pointer);
    uint8_t p=(freq-880)/2-(freq-880)/10;
    pcd8544_draw_icon_fb(ptr,p,5,1);

    ptr= (char *)(&stereo);
    pcd8544_draw_icon_fb(ptr,0,0,1);
    pcd8544_draw_icon_fb(ptr,7,0,1);

    ptr= (char *)(&battery);
    pcd8544_draw_icon_fb(ptr,70,0,2);
*/
    if (isMirror)
        ste2007_mirror();

    ste2007_display_fb();
}
